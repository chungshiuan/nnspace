import argparse, os, shutil
parser = argparse.ArgumentParser()


def get_target_floder(pth):
    total = [os.path.join(pth, fpth) for fpth in os.listdir(pth)]
    return len(total), total 

def get_source_folder(pth):
    all_path =[os.path.join(pth, fpth) for fpth in  os.listdir(pth)]
    return all_path

def aggregrate_src_folder(to, src_files, to_total):
    rd = int(to_total/len(src_files))
    print(rd)
    for i in range(rd):
        for sfile in src_files:
            tofile = sfile.split('/')[-1]
            tofile = tofile.split('.')
            tofile[0] = '{}_{}'.format(tofile[0],i)
            tofile = os.path.join(to, '.'.join(tofile))
            print(tofile)
            shutil.copyfile(sfile, tofile)    
    total = len(os.listdir(to))
    print(total) 
    return total

def aggregrate_tar_folder(to, tar_files):
    for sfile in tar_files:
        tofile = sfile.split('/')[-1]
        tofile = os.path.join(to, tofile)
        print(tofile)
        shutil.copyfile(sfile, tofile)    

def check_path(pth):
    pth=os.path.abspath(pth)
    try:
        if not os.path.exists(pth):
            os.makedirs(pth)
        return pth
    except Exception as e:
        print('check {} fail: {}'.format(pth, e))
        raise Exception('check {} fail'.format(pth))

def gen_ds(src, tar, to):
    to_total, tar_files = get_target_floder(tar)
    src_files = get_source_folder(src)
    to = check_path(to)
    aggregrate_src_folder(to, src_files, to_total)
    aggregrate_tar_folder(to, tar_files)
    print(len(os.listdir(to)))
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--src", type=str, help="source", required=True)
    parser.add_argument("--tar", type=str, help="tar", required=True)
    parser.add_argument("--to", type=str, help="to", required=True)
    args = parser.parse_args()
    gen_ds(args.src, args.tar, args.to)
