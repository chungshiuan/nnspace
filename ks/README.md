**keras installation**
- [ELL解决：libstdc++.so.6: version `CXXABI_1.3.9' not found (required by bin/opencv_test_core)?](http://blog.csdn.net/l297969586/article/details/76590055)
- [(fix)报错信息ImportError: /lib64/libstdc++.so.6: version `CXXABI_1.3.9' not found (required by)](http://blog.csdn.net/ccbrid/article/details/78979878)

**example**
- [Keras VGG Retrained CNN](https://www.kaggle.com/vincento/keras-vgg-retrained-cnn)
- [Dogs vs Cats: Keras Solution](https://www.kaggle.com/sarvajna/dogs-vs-cats-keras-solution)
- [CatdogNet - Keras Convnet Starter](https://www.kaggle.com/jeffd23/catdognet-keras-convnet-starter)
- [Full Classification Example with ConvNet](https://www.kaggle.com/sentdex/full-classification-example-with-convnet)