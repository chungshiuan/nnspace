import os, __init__
import pandas as pd
import numpy as np
from base_dataset import ToDataset
from torch.utils.data import Dataset, DataLoader
import PIL.Image
from scipy.stats import itemfreq
from zipfile import ZipFile
from io import BytesIO
from const import *
from torchvision import transforms
import torch
from plot import plotFigures

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image, landmarks = sample['image'], sample['labels']
        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image = image.transpose((2, 0, 1))
        return {'image': torch.from_numpy(image),
                'labels': landmarks}

data_transform = transforms.Compose([
        #transforms.CenterCrop(WH_SIZE),
        ToTensor()
])


class Dataset(ToDataset):

    def __init__(self, folder = 'KaggleDog', transform = None):
        super(Dataset, self).__init__(folder)
        train_path = os.path.join(self.root_dir, 'train.zip')
        self.train_u_path = os.path.join(self.root_dir, 'train')
        self.archive_u_train = os.listdir(self.train_u_path)
        self.archive_train = ZipFile(train_path, 'r')
        label_csv = os.path.join(self.root_dir, 'labels.csv.zip')
        self.label_frame = pd.read_csv(label_csv, compression='zip', header=0, sep=',', quotechar='"')
        self.transform = transform
        self.labels, self.selected_frame = self._get_records()

    def get_labels(self):
        return self.labels.tolist()

    def _get_records(self):
        labels_freq = itemfreq(self.label_frame['breed'])
        labels_freq = labels_freq[labels_freq[:,1].argsort()[::-1]]
        selected_freq = labels_freq[labels_freq[0:BREED_SIZE,0].argsort()][:,0]
        selected_frame = self.label_frame[self.label_frame['breed'].isin(selected_freq)]
        return selected_freq, selected_frame.reset_index(drop=True)

    def __len__(self):
        return len(self.selected_frame)

    def __getitem__(self, idx):
        img_name = '{}.jpg'.format(self.selected_frame.id[idx])
        img_path = [item for item in self.archive_u_train if img_name in item][0]
        image = np.array(PIL.Image.open(os.path.join(self.train_u_path, img_path)).resize((WH_SIZE, WH_SIZE))).astype('float32')
        labels = self.get_labels().index( self.selected_frame.breed[idx])
        sample = {'image': image,
                  'labels': labels}
        if self.transform:
            sample = self.transform(sample)
        return sample

    def __getitem_bk__(self, idx):
        img_name = '{}.jpg'.format(self.selected_frame.id[idx])
        img_path = [item for item in self.archive_train.namelist() if img_name in item][0]
        file_bio = BytesIO(self.archive_train.read(img_path))
        image = np.array(PIL.Image.open(file_bio).resize((WH_SIZE, WH_SIZE))).astype('float32')
        labels = self.get_labels().index( self.selected_frame.breed[idx])
        sample = {'image': image,
                  'labels': labels}
        if self.transform:
            sample = self.transform(sample)
        return sample

def get_dataloader(data_transform=data_transform): 
    ds = Dataset(transform = data_transform)
    dataloader = DataLoader(ds, batch_size=int(os.environ.get('BATCH_SIZE')),
                            shuffle=True, num_workers=4)

    return dataloader

if __name__=='__main__':
    ds = Dataset(transform=None)
    print('=======>',len(ds))
    dataloader = DataLoader(ds, batch_size=int(os.environ.get('BATCH_SIZE')),
                         shuffle=True, num_workers=4)

    for step, sample_batch in enumerate(dataloader):
        print('==== ',step,' ====')
        #plotFigures(sample_batch['image'])
        print(sample_batch['image'])
        #plotFigures(sample_batch['image'])

