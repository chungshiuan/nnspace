import torch
import numpy as np
from torch.autograd import Variable
from torch.nn import functional as F
import os 
from dataset import get_dataloader
dataloader = get_dataloader()
num_epochs=500

learning_rate = 0.001
batch_size=int(os.environ.get('BATCH_SIZE'))

class DogBreedNet(torch.nn.Module):
    def __init__(self, D_in, D_out):
        super(DogBreedNet, self).__init__()
        self.conv = torch.nn.Sequential()
        self.conv.add_module("conv_1", torch.nn.Conv2d(D_in[0], 32, kernel_size=5))
        self.conv.add_module("maxpool_1", torch.nn.MaxPool2d(kernel_size=2))
        self.conv.add_module("relu_1", torch.nn.ReLU())
        self.conv.add_module("conv_2", torch.nn.Conv2d(32, 64, kernel_size=4))
        self.conv.add_module("maxpool_2", torch.nn.MaxPool2d(kernel_size=2))
        self.conv.add_module("relu_2", torch.nn.ReLU())
        self.conv.add_module("conv_3", torch.nn.Conv2d(64, 128, kernel_size=3))
        self.conv.add_module("maxpool_3", torch.nn.MaxPool2d(kernel_size=2))
        self.conv.add_module("relu_3", torch.nn.ReLU())
        self.conv.add_module("dropout_3", torch.nn.Dropout())
        
        self.flat_in_size = self._get_flat_fts(D_in)
        self.fc = torch.nn.Sequential()
        self.fc.add_module("fc1", torch.nn.Linear(self.flat_in_size, 128))
        self.fc.add_module("relu_fc1", torch.nn.ReLU())
        self.fc.add_module("dropout_fc1", torch.nn.Dropout())
        self.fc.add_module("fc2", torch.nn.Linear(128, D_out))

    def _get_flat_fts(self, in_size):
        f = self.conv(Variable(torch.ones(1,*in_size)))
        return int(np.prod(f.size()[1:]))

    def forward(self, x):
        fts = self.conv(x)
        flat_fts = fts.view(-1, self.flat_in_size)
        out = self.fc(flat_fts)
        return out

cnn = DogBreedNet((3,128,128),2)


# Loss and Optimizer
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(cnn.parameters(), lr=learning_rate)

# Train the Model
for epoch in range(num_epochs):
    for i, sample in enumerate(dataloader):
        images = Variable(sample['image'])
        labels = Variable(sample['labels'])
        
        # Forward + Backward + Optimize
        optimizer.zero_grad()
        outputs = cnn(images)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        print('=={}=='.format(i)) 
        total=0
        correct=0
        if (i+1) % 10 == 0:
            print (outputs)
            print (labels)
            soft_out = F.softmax(outputs)
            _, max = torch.max(soft_out, 1)
            print('!!!!!!',max)
            print('======',labels)
            print(dir(max == labels))
            total += labels.size(0)
            correct += (max == labels).sum()
            print('----',type(total))
            print('----',dir(correct))
            print('$$$$$$$$$',total)
            print ('Epoch [%d/%d], Iter [%d/%d] Loss: %.4f accuracy %.4f' 
                   %(epoch+1, num_epochs, i+1, len(sample['labels'])//batch_size, loss.data[0], float(correct)/total))
