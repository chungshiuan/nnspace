import os, __init__
from torch.utils.data import Dataset 
from config import initEnv

initEnv()

class ToDataset(Dataset):
    def __init__(self, folder):
        self.root_dir = os.path.join((os.environ.get('DATASET_ROOT')), folder)


if __name__ == '__main__':
   to_dataset = ToDataset('gg')

