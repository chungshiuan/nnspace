import os, __init__
import pandas as pd
import numpy as np
from base_dataset import ToDataset
from torch.utils.data import Dataset, DataLoader
import PIL.Image

class Dataset(ToDataset):

    def __init__(self, folder = 'ToFaces/faces', transform = None):
        print('????',folder)
        super(Dataset, self).__init__(folder)
        self.landmark_frame = pd.read_csv(os.path.join(self.root_dir, 'face_landmarks.csv'))
        print(self.landmark_frame.iloc[1,1])
        self.transform = transform

    def __len__(self):
        return len(self.landmark_frame)

    def __getitem__(self, idx):
        #print(self.landmark_frame.iloc[idx,0])
        img_name = os.path.join(self.root_dir, 
                                self.landmark_frame.iloc[idx,0])
        image = np.array(PIL.Image.open(img_name).resize((60,60)))
        landmarks = self.landmark_frame.iloc[idx,1:].as_matrix().astype('float').reshape(-1,2)
        #print(np.array(image))
        #print(landmarks)
        sample = {'image': image,
                  'landmarks': landmarks}
        if self.transform:
           sample = self.transform(sample)
        return sample

 
if __name__=='__main__':
    ds = Dataset()
    print('=======>',len(ds))
    dataloader = DataLoader(ds, batch_size=int(os.environ.get('BATCH_SIZE')),
                            shuffle=True, num_workers=4)
    for step, sample_batch in enumerate(dataloader):
        print('==== ',step,' ====')
        print(len(sample_batch))
        
