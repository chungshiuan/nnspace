get flatten dimension
- [Inferring shape via flatten operator](https://discuss.pytorch.org/t/inferring-shape-via-flatten-operator/138)
- [Flatten layer of PyTorch](http://forums.fast.ai/t/flatten-layer-of-pytorch/4639)

cnn example
- [Getting started with PyTorch for Deep Learning (Part 3: Neural Network basics)](https://codetolight.wordpress.com/2017/11/29/getting-started-with-pytorch-for-deep-learning-part-3-neural-network-basics/)
- [A PyTorch tutorial – deep learning in Python](http://adventuresinmachinelearning.com/pytorch-tutorial-deep-learning/)
- [mnist pytorch simple (non-cnn) example](https://www.kaggle.com/databang/mnist-pytorch-simple-non-cnn-example)
- [toy demo - PyTorch + MNIST](https://xmfbit.github.io/2017/03/04/pytorch-mnist-example/)

cross entropy
- [PyTorch中的Loss Fucntion](http://sshuair.com/2017/10/21/pytorch-loss/)
- [條件機率](http://math1.ck.tp.edu.tw/%E9%99%B3%E5%98%AF%E8%99%8E/%E5%B0%8F%E8%99%8E/99%E8%AA%B2%E7%B6%B1/%E7%AC%AC%E4%BA%8C%E5%86%8A/%E9%87%8D%E9%BB%9E/99%E8%AA%B2%E7%B6%B1%E6%95%99%E5%AD%B8%E9%87%8D%E9%BB%9E%E6%95%B4%E7%90%862-3-3%E6%A9%9F%E7%8E%87-%E6%A2%9D%E4%BB%B6%E6%A9%9F%E7%8E%87%E8%88%87%E8%B2%9D%E6%B0%8F%E5%AE%9A%E7%90%86.pdf)
- [Softmax vs. Softmax-Loss: Numerical Stability](http://freemind.pluskid.org/machine-learning/softmax-vs-softmax-loss-numerical-stability/)
- [Negative log-likelihood function](http://www.cnblogs.com/ZJUT-jiangnan/p/5489047.html)
- [Understanding softmax and the negative log-likelihood](https://ljvmiranda921.github.io/notebook/2017/08/13/softmax-and-the-negative-log-likelihood/)
- [tensor 轉型態](http://blog.csdn.net/nnuyi/article/details/78474563)
- [Training a classifier](http://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html)
- [A Friendly Introduction to Cross-Entropy Loss](https://rdipietro.github.io/friendly-intro-to-cross-entropy-loss/#cross-entropy)
- [KL Divergence](https://www.youtube.com/watch?v=aSuuByj8T6Y)
- [Shan-Hung Wu](https://www.youtube.com/channel/UCBtDVX6tpl1SCPsT5SQEnYQ)
- [为什么交叉熵（cross-entropy）可以用于计算代价？](https://www.zhihu.com/question/65288314)