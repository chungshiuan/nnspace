import os
from fnmatch import fnmatch
from tensorflow.python.client import device_lib
import numpy as np

def get_pattern_files(path, pattern='training'):
    ret = []
    for item in os.listdir(path):
        if fnmatch(item, '*'+pattern+'*'):
            ret.append(os.path.join(path, item))
    return ret

def check_path(path):
    path=os.path.abspath(path)
    try:
        if not os.path.exists(path):
            os.makedirs(path)    
        return path
    except Exception as e:
        print('check {} fail: {}'.format(path, e))
        return None


def get_available_gpus(d_type='GPU'):
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == d_type]

def pixel_range(img):
    vmin, vmax = np.min(img), np.max(img)
    if vmin * vmax >= 0:
        return (vmin, vmax)
    else:
        if -vmin > vmax:
            vmax = -vmin
        else:
            vmin = -vmax
    return (vmin, vmax)

if __name__ == '__main__':
   print(get_available_gpus())
   print(get_available_gpus('CPU'))
