import sys, os, time, __init__
import tensorflow as tf
import numpy as np
from dataset_factory import CelebADS
from ae.ae_lib import AELib

BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))

if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = AELib(CelebADS(reshape_size = [-1, 128, 128, 3]), image_shape=[128, 128, 3])    
    getattr(net, exe_type)()
