import sys, os, time, __init__
from gan.dcgan import DCGANXEtpy_2Loss
from dataset_factory import CelebADS
from utils import check_path

#MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')


if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = DCGANXEtpy_2Loss(dataset=CelebADS(epoch=30000, reshape_size=[-1, 128, 128, 3]), disc_image_shape=[128, 128, 3])    

    getattr(net, exe_type)()
