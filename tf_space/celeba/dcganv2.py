import sys, os, time, __init__
from gan.dcgan import DCGANv2
from dataset_factory import CelebADS
from utils import check_path

MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
size3 = [3, 3]
size5 = [5, 5]
size2 = [2, 2]

if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = DCGANv2(dataset=CelebADS(epoch=3000, reshape_size=[-1, 128, 128, 3]), 
                dis_k_size=size5,
                dis_k_stride=[2, 2], 
                dis_p_size=size2,
                dis_p_stride=[1, 1],
                dis_namespace='dis',
                dis_out_activate='sigmoid',
                dis_layers=4,
                gen_k_size=size3,
                gen_k_stride=[2, 2],
                gen_namespace='gen',
                gen_out_activate='tanh',
                gen_layers=4,
                image_size=[128, 128],
                image_channel=3,
                filepath=__file__)    

    getattr(net, exe_type)()
