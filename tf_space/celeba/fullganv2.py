import sys, os, time, __init__
from gan.fullgan import FULLGANv2 as GANClass
from dataset_factory import CelebADS

from utils import check_path

MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
size3 = [3, 3]
size5 = [5, 5]
size2 = [2, 2]

if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = GANClass(dataset=CelebADS(epoch=3000, reshape_size=[-1, 128, 128, 3], resize=[128,128]), 
                dis_k_size=None,
                dis_k_stride=None, 
                dis_p_size=None,
                dis_p_stride=None,
                dis_namespace='dis',
                dis_out_activate='sigmoid',
                dis_layers=4,
                gen_k_size=None,
                gen_k_stride=None,
                gen_namespace='gen',
                gen_out_activate='tanh',
                gen_layers=4,
                image_size=[128, 128],
                image_channel=3,
                filepath=__file__)    


    getattr(net, exe_type)()
