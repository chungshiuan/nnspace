import os, sys, argparse, cv2
import __init__
import pandas as pd
from scipy.stats import itemfreq
import tensorflow as tf
from PIL import Image
from base_dataset import FileData
import numpy as np
from plot import plotFigures
import utils

def imgRead(img_path, resize=[]):
    image = Image.open(img_path.decode('utf-8'))
    if len(resize)>0:
       resize = tuple(resize)
       image = image.resize(resize)
    #image = np.clip(np.array(image)/256.0, 0.0, 1.0)
    image = np.array(image)
    #print('parsed image: {}'.format(image))
    return image.astype(np.float32)

class Data(FileData):
    def __init__(self, folder='KaggleCelebA', select_category='church_outdoor_train_lmdb', resize=[], batch_size=10, image_norm_type=None):
        super(Data, self).__init__(folder=folder, resize=resize, batch_size=batch_size,  image_norm_type=image_norm_type)
        self._select_category = select_category.split(',')
        self._check_root() 
        self._image_path = os.path.join(self.root, 'img_align_celeba')

    def get_records(self, epoch=2):
        self.files = [os.path.join(self._image_path, key) for key in os.listdir(self._image_path)]
        self.labels = os.listdir(self._image_path)
        self.codes = self.labels
        return super(Data, self).get_records(epoch)  

    def parser(self, *record):
        print('== start to parse record ==',record)
        if len(self.resize) != 2: 
           self.resize = [128,128]
        image = tf.py_func(
            imgRead,
            [record[0], tf.constant(self.resize)],
            tf.float32
        )
        image = self.get_image_norm(image)
        return image, record[1], record[2]

    def get_labels(self, all = False):
        print('not implemented get_labels')
        pass   

    def list_category(self):
        print('not implemented list_category')
        pass


if __name__ == '__main__':
    dog = Data(batch_size=10,resize=[256,256], image_norm_type='0to1')
    if sys.argv[1] == 'fetch':
        dog.fetch()  
    elif sys.argv[1] == 'image':
        iterator =  dog.get_records(1)
        image_batch, label_batch, codes = iterator.get_next()
        with tf.Session() as sess:
            sess.run((tf.global_variables_initializer(),tf.local_variables_initializer()))
            sess.run(iterator.initializer)
            ep = 0
            while True:
                try:
                    labels, images, t_labels = sess.run([label_batch, image_batch, codes])
                    print('--------------------------------', ep)
                    print(len(labels))
                    print(images)
                    plotFigures(images, labels=t_labels, realname=labels)
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print(e)
                    break
                ep += 1
    elif sys.argv[1] == 'ls':
        print('category: {}'.format(dog.list_category()))
    else:
        print(len(dog.get_labels(True)))
