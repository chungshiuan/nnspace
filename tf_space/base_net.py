import tensorflow as tf
import numpy as np
import __init__, os, time, traceback
from plot import plotFigures,genImage
#from const import *
from utils import check_path
import shutil


NOISE_DIM = 100
IMAGE_DIM = 784
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
LOGS_PATH = os.path.join('./tmp_2l_relu',str(int(time.time())))

class BaseNet(object):

    def __init__(self, dataset, filepath=None):
        self.run_train_tensors={}
        self.run_predict_tensors={}
        self.init = None
        self.saver = None
        self.iterator = None
        self.dataset = dataset
        if not filepath == None:
            self.filepath = filepath.replace('.py', '')
            self.filepath = self.filepath + '__space'
            self.create_space_folder()
        else:
            self.filepath = None

    def create_space_folder(self):
        if self.filepath==None:
            return
        if os.path.exists(self.filepath):
            shutil.rmtree(self.filepath)
        os.makedirs(self.filepath) 

    def _get_noise(self, n_type='uni', shape=[BATCH_SIZE, NOISE_DIM]):
        if n_type == 'uni':
            return np.random.uniform(-1., 1., size=shape)
        return np.random.normal(0, 1, size=shape)

    def _post_jobs(self):
        self.init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.saver =  tf.train.Saver()
        self.writer = tf.summary.FileWriter(LOGS_PATH, graph=tf.get_default_graph())
        self.summary = tf.summary.merge_all()
        self.run_train_tensors['_summary_op' ] = self.summary

    def net(self):
        raise NotImplementedError

    def train(self):
        with tf.Session() as sess:
            sess.run(self.init)
            loop = 0
            while True:
                try:
                    noise = self._get_noise('nor')
                    batch_x,_,_ = self.dataset.next_batch(BATCH_SIZE)
                    gen_input = tf.get_default_graph().get_tensor_by_name('gen_input:0') 
                    disc_input = tf.get_default_graph().get_tensor_by_name('disc_input:0') 
                    feed_dict = {gen_input: noise, 
                                 disc_input: batch_x}
                    result = sess.run(list(self.run_train_tensors.values()), feed_dict=feed_dict)
                    self.writer.add_summary(result[-1], loop)
                    if loop % 20 == 0 or loop == 0:
                        print('=========== {}'.format(loop))
                        for i, i_value in enumerate(result):
                            i_key = list(self.run_train_tensors.keys())[i]
                            if not i_key.startswith('_'):
                                print('{} : {}'.format(i_key, i_value))  
                        self.saver.save(sess, MODEL_FOLDER)
                    loop += 1
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print(e)
                    break

    def predict(self):
        with tf.Session() as sess:
            saver = tf.train.import_meta_graph(MODEL_FOLDER+'.meta')
            saver.restore(sess, MODEL_FOLDER)
            noise = self._get_noise('nor')
            batch_x,_,_ = self.dataset.next_batch(BATCH_SIZE)
            gen_input = tf.get_default_graph().get_tensor_by_name('gen_input:0')
            disc_input = tf.get_default_graph().get_tensor_by_name('disc_input:0') 
            gen_out = tf.get_default_graph().get_tensor_by_name('g-out:0') 

            feed_dict = {gen_input: noise, 
                         disc_input: batch_x}
 
            result = sess.run(gen_out, feed_dict=feed_dict)
            images = result.reshape((BATCH_SIZE, WH_SIZE, WH_SIZE))
            images = -1*(images-1) 
            plotFigures(images)

