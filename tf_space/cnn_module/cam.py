import tensorflow as tf
from tensorflow.python.ops import nn_ops, gen_nn_ops

class CAM(object):
    def __init__(self, activation, weights, biases=[]):
        self.activation = activation
        self.weights = weights

    def get_ex(self, logist, image_size):
        print(image_size)
        cam = tf.reduce_sum(self.activation * self.weights[:,logist], axis=3, keep_dims=True) 
        print('!!!!!',cam, image_size)
        return tf.image.resize_bilinear(cam, image_size, align_corners=True)

