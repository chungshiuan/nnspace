import tensorflow as tf
from tensorflow.python.ops import nn_ops, gen_nn_ops

class LRP_Alpha_Beta(object):
    def __init__(self, activations, weights, biases, alpha=0.5):
        self.alpha=alpha
        self.activations = activations
        self.weights = weights
        self.biases = biases
        self.conv_strides = [1,1,1,1]
        self.pool_strides = [1,2,2,1]
        self.pool_ksize = [1,2,2,1]
        print('----------activations=====================================')
        print(self.activations)
        print(self.activations[0][1])
        print(self.activations[0][1].shape)
        print(self.activations[0][0][0])
        print(self.activations[0][0][1])
        print(self.activations[0][1][0])
        print(self.activations[0][1][1])
        print(self.activations[0][2][0])
        print(self.activations[0][2][1])

        print('---------activations=====================================')

    def get_lrp(self, logit):
        RS=[]
        print('>>>>>>>>>>>>>ac, len:{},{}'.format(len(self.activations), self.activations))
        print('>>>>>>>>>>>>>we, len:{},{}'.format(len(self.weights), self.weights))
        widx = 0
        for idx, activation in enumerate(self.activations):
            print('activation name:{}'.format(activation.name.lower()))
            if idx == 0:
                print('========================')
                print('idx:{} , act:{}, act+1:{}'.format(idx, activation, self.activations[idx+1]))
                print('idx:{}, widx:{}, w: {}'.format(idx, widx, self.weights[widx]))
                RS.append(activation[:,logit,None])
                #RS.append(self.backprop_dense(self.activations[idx + 1], self.weights[widx][:,logit,None], None, RS[-1]))
                #widx += 1
            elif 'dense' in activation.name.lower():
                print('========================')
                print('idx:{} , act:{}, act+1:{}'.format(idx, activation, self.activations[idx+1]))
                print('idx:{}, widx:{}, w: {}'.format(idx, widx,self.weights[widx]))
                RS.append(self.backprop_dense(self.activations[idx + 1], self.weights[widx], None, RS[-1]))
                widx += 1
            elif 'reshape' in activation.name.lower():
                print('========================')
                print('idx:{} , act:{}, act+1:{}'.format(idx, activation, self.activations[idx+1]))
                shape = self.activations[idx + 1].get_shape().as_list()
                shape[0] = -1
                RS.append(tf.reshape(RS[-1], shape))
            elif 'conv' in activation.name.lower():
                print('========================')
                print('idx:{} , act:{}, act+1:{}'.format(idx, activation, self.activations[idx+1]))
                print('idx:{}, widx:{}, w: {}'.format(idx, widx,self.weights[widx]))
                RS.append(self.backprop_conv(self.activations[idx + 1], self.weights[widx], None, RS[-1], self.conv_strides))
                widx += 1 
            elif 'pool' in activation.name.lower():
                print('========================')
                print('idx:{} , act:{}, act+1:{}'.format(idx, activation, self.activations[idx+1]))
                RS.append(self.backprop_pool(self.activations[idx + 1], RS[-1], self.pool_ksize, self.pool_strides))
            else:
                raise Exception('Unknown operation: {}'.format(activation))
            print('len rs:{}'.format(len(RS)))
            print('lastest rs:{}'.format(RS[-1]))
            print(idx, ' | ', len(self.activations))
            if idx == len(self.activations) - 2 :
                break
        return RS[-1]*255

    def backprop_conv(self, activation, kernel, bias, relevance, strides, padding='SAME'):
        W_p = tf.maximum(0., kernel)
        z_p = nn_ops.conv2d(activation, W_p, strides, padding)
        if bias: 
            z_p = zp + tf.maximum(0., bias) 
        s_p = relevance / z_p
        c_p = nn_ops.conv2d_backprop_input(tf.shape(activation), W_p, s_p, strides, padding)

        W_n = tf.minimum(0., kernel)
        z_n = nn_ops.conv2d(activation, W_n, strides, padding)
        if bias:
            z_n =  z_n + tf.minimum(0., bias)
        s_n = relevance / z_n
        c_n = nn_ops.conv2d_backprop_input(tf.shape(activation), W_n, s_n, strides, padding)
        return activation * (self.alpha * c_p + (1 - self.alpha) * c_n)


    def backprop_pool(self, activation, relevance, ksize, strides, padding='SAME'):
        z = nn_ops.max_pool(activation, ksize, strides, padding) + 1e-10
        s = relevance / z
        c = gen_nn_ops.max_pool_grad(activation, z, s, ksize, strides, padding)
        return activation * c

    def backprop_dense(self, activation, kernel, bias, relevance):
        W_p = tf.maximum(0., kernel)
        z_p = tf.matmul(activation, W_p) 
        if bias:
            z_p = z_p + tf.maximum(0., bias)
        s_p = relevance / z_p
        c_p = tf.matmul(s_p, tf.transpose(W_p))

        W_n = tf.minimum(0., kernel) 
        z_n = tf.matmul(activation, W_n) 
        if bias:
            z_n = z_n + tf.minimum(0., bias)
        s_n = relevance / z_n
        c_n = tf.matmul(s_n, tf.transpose(W_n))
        return activation * (self.alpha * c_p + (1 - self.alpha) * c_n)
