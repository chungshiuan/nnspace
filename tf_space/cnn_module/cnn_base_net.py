import tensorflow as tf
import numpy as np
from base_net import BaseNet
import os 
class BaseCNN(BaseNet):
    def __init__(self, dataset):
        super(BaseCNN, self).__init__(dataset)

    def train(self):
        with tf.Session() as sess:
            sess.run(self.init)
            loop = 0
            ex = 0
            while True:
                pass
LEARNING_RATE=1e-3
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))

class CNN(BaseCNN):
    def __init__(self, dataset, image_shape, model_path='model'):
        super(CNN, self).__init__(dataset)
        self.image_shape = image_shape
        self.net_out = None
        self.model_path = model_path

    def net(self, scope='CNN', cnn_layers=2):
        activation = []
        with tf.variable_scope(scope):
            input_place = tf.placeholder(tf.float32, shape=[None, *self.image_shape], name='input')
            n_out = input_place
            activation.append(n_out)
            filter = 32
            for i in range(cnn_layers):
                n_out = tf.layers.conv2d(inputs=n_out, filters=filter, kernel_size=[3,3], use_bias=False, padding="SAME",activation=tf.nn.relu, name='conv{}'.format(i))
                activation.append(n_out)
                n_out = tf.layers.max_pooling2d(inputs=n_out, pool_size=[2,2], padding='SAME', strides=2, name='pool{}'.format(i))
                activation.append(n_out)
                filter *= 2
            n_out = tf.reshape(n_out, [-1, n_out.get_shape()[1:4].num_elements()])
            activation.append(n_out)
            n_out = tf.layers.dense(inputs=n_out, units=625, activation=tf.nn.relu, use_bias=False, name='dense1')
            activation.append(n_out)
            n_out = tf.layers.dense(inputs=n_out, units=len(self.dataset.get_labels()), use_bias=False, name='dense2')
            activation.append(n_out)
            n_out = tf.nn.softmax(n_out, name='softmax')
            activation.append(n_out)
            self.net_out = n_out
        return activation

    def optimization(self):
        labels = tf.placeholder(tf.string, shape=[None, ], name='labels')
        #labels = tf.Print(labels, [labels])
        #print(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='CNN'))
        train_labels = tf.map_fn(lambda l: tf.where(tf.equal(self.dataset.get_labels(), l))[0,0:1][0], labels, dtype=tf.int64)
        one_hot_labels = tf.one_hot(train_labels, depth=len(self.dataset.get_labels()), dtype=tf.float32, name="one_hot_labels")
        #diff = tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=self.net_out)
        diff = -tf.reduce_sum( one_hot_labels*tf.log(self.net_out), reduction_indices=[1])
        loss = tf.reduce_mean(diff)
        train = tf.train.AdamOptimizer(LEARNING_RATE).minimize(loss, name='train')
        #print(train)
        predict = tf.nn.softmax(self.net_out)
        predict = tf.argmax(predict, 1)
        ans = tf.argmax(one_hot_labels, 1)
        correct_prediction = tf.equal(predict, ans)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')
    
    def train(self):
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        with tf.Session() as sess:
            sess.run(init)  
            saver = tf.train.Saver() 
            loop = 0
            input_data = tf.get_default_graph().get_tensor_by_name('CNN/input:0') 
            labels = tf.get_default_graph().get_tensor_by_name('labels:0') 
            train = tf.get_default_graph().get_operation_by_name('train')
            accuracy = tf.get_default_graph().get_tensor_by_name('accuracy:0')
            while True:
                try:
                    batch_in, batch_label, _ = self.dataset.next_batch(BATCH_SIZE)
                    #print('>>>>>>>>>>>>', batch_label)
                    feed_dict = { input_data: batch_in,
                                  labels: batch_label}
                    result = sess.run([train, accuracy], feed_dict=feed_dict) 
                    if loop % 1 == 0 or loop == 0:
                        print('{}: accuracy: {}'.format(loop, result[1]))
                    if loop % 100 == 0:
                        saver.save(sess, self.model_path)
                    loop+=1
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print(e)
                    break



class CNNCAM(CNN):
    def __init__(self, dataset, image_shape, model_path='model'):
        super(CNNCAM, self).__init__(dataset, image_shape, model_path)

    def net(self, scope='CNN', cnn_layers=3):
        activation = []
        with tf.variable_scope(scope):
            input_place = tf.placeholder(tf.float32, shape=[None, *self.image_shape], name='input')
            n_out = input_place
            activation.append(n_out)
            filter = 32
            for i in range(cnn_layers):
                n_out = tf.layers.conv2d(inputs=n_out, filters=filter, kernel_size=[3,3], use_bias=False, padding="SAME",activation=tf.nn.relu, name='conv{}'.format(i))
                activation.append(n_out)
                if i == (cnn_layers - 1):
                    print("??????", n_out.get_shape())
                    n_out = tf.layers.average_pooling2d(inputs=n_out, pool_size=[*n_out.get_shape()[1:3]], strides=1)
                else:
                    n_out = tf.layers.max_pooling2d(inputs=n_out, pool_size=[2,2], padding='SAME', strides=2, name='pool{}'.format(i))
                activation.append(n_out)
                filter *= 2
            print('!!!!!!', n_out.get_shape())
            n_out = tf.reshape(n_out, [-1, n_out.get_shape()[1:4].num_elements()])
            activation.append(n_out)
            #n_out = tf.layers.dense(inputs=n_out, units=625, activation=tf.nn.relu, use_bias=False, name='dense1')
            #activation.append(n_out)
            n_out = tf.layers.dense(inputs=n_out, units=len(self.dataset.get_labels()), use_bias=False, name='dense2')
            activation.append(n_out)
            n_out = tf.nn.softmax(n_out, name='softmax')
            activation.append(n_out)
            self.net_out = n_out
        return activation

