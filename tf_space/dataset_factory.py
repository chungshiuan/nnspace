import os,sys
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from mnist.tfr_dataset import Data as MnistData
from dogbreed.dataset import Data as DogBreedData
from dogcat.dataset import Data as DogCatData
from lsun.dataset import Data as LSUNData
from celeba.dataset import Data as CelebAData
from screw.dataset import Data as ScrewData

from plot import plotFigures,genImage

BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))

class BaseDSItf(object):
    def __init__(self, batch_size = BATCH_SIZE, reshape_size=[], resize=[]):
        self.batch_size = batch_size
        self.resize = resize

        self.reshape_size = reshape_size
    def next_batch(self, batch_size = None):
        raise NotImplementedError

class MnistLibDS(BaseDSItf):
    def __init__(self, batch_size = BATCH_SIZE, reshape_size=[], resize=[], image_norm_type=None):
        super(MnistLibDS, self).__init__(batch_size, reshape_size, resize, image_norm_type)
        self.mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)
        print('LibDS Batch size: {}'.format(self.batch_size))

    def next_batch(self, batch_size = None):
        images, labels = self.mnist.train.next_batch(self.batch_size)
        if len(self.reshape_size) > 0:
            images = images.reshape(self.reshape_size)
        labels = np.argmax(labels,axis=1)
        return images, labels, []

class BaseTfrDS(BaseDSItf):
    def __init__(self, batch_size = BATCH_SIZE, reshape_size=[], epoch=3000, resize=[]):
        super(BaseTfrDS, self).__init__(batch_size, reshape_size, resize)
        self.data =  None

    def _init_gen(self):
        with tf.Session() as ds_sess:       
            ds_sess.run(tf.local_variables_initializer())

            ds_sess.run(self.iterator.initializer)
            while True:
                output_ds = ds_sess.run(self.output_ds)
                images = output_ds[0]
                labels = output_ds[1]
                code = output_ds[2] #only for dogbreed, dogcat
                if len(self.reshape_size) > 0:
                    #print('reshape_size: {} > 0 ,start to reshape'.format(self.reshape_size))
                    images = np.reshape(images, self.reshape_size)
                yield images, labels, code

    def next_batch(self, batch_size = None):
        return next(self.gen)
    def get_labels(self):
        return self.data.get_labels()

class TfrDS(BaseTfrDS):
    def __init__(self, batch_size = BATCH_SIZE, reshape_size=[], Data=MnistData, epoch=3000, resize=[], image_norm_type=None):
        super(TfrDS, self).__init__(batch_size, reshape_size, epoch, resize)
        self.data =  Data(batch_size=self.batch_size, resize=self.resize, image_norm_type=image_norm_type)
        self.iterator = self.data.get_records(epoch=epoch)
        self.output_ds = self.iterator.get_next()
        self.gen = self._init_gen()
      
class TfrWithBatchSizeDS(BaseTfrDS):
    def __init__(self, batch_size = BATCH_SIZE, reshape_size=[], Data=MnistData, epoch=3000, resize=[], image_norm_type=None):
        super(TfrWithBatchSizeDS, self).__init__(batch_size, reshape_size, epoch, resize)
        self.data =  Data(batch_size=self.batch_size, resize=self.resize, image_norm_type=image_norm_type)
        self.iterator = self.data.get_records(epoch=epoch)
        self.output_ds = self.iterator.get_next()
        self.gen = self._init_gen()
  
class MnistTfrDS(TfrWithBatchSizeDS):
    def __init__(self, batch_size=BATCH_SIZE, reshape_size=[], Data=MnistData,epoch=3000, resize=[], image_norm_type=None):
        super(MnistTfrDS, self).__init__(batch_size, reshape_size, Data, epoch=epoch, resize=resize, image_norm_type=image_norm_type)

class DogBreedTfrDS(TfrDS):
    def __init__(self, batch_size=BATCH_SIZE, reshape_size=[], Data=DogBreedData, epoch=3000, resize=[], image_norm_type=None):
        super(DogBreedTfrDS, self).__init__(batch_size, reshape_size, Data, epoch=epoch, resize=resize, image_norm_type=image_norm_type)

class DogCatTfrDS(TfrDS):
    def __init__(self, batch_size=BATCH_SIZE, reshape_size=[], Data=DogCatData, epoch=3000, resize=[], image_norm_type=None):
        super(DogCatTfrDS, self).__init__(batch_size, reshape_size, Data, epoch=epoch, resize=resize, image_norm_type=image_norm_type)

class LSUNDS(TfrDS):
    def __init__(self, batch_size=BATCH_SIZE, reshape_size=[], Data=LSUNData, epoch=3000, resize=[], image_norm_type=None):
        super(LSUNDS, self).__init__(batch_size, reshape_size, Data, epoch=epoch, resize=resize, image_norm_type=image_norm_type)

class CelebADS(TfrDS):
    def __init__(self, batch_size=BATCH_SIZE, reshape_size=[], Data=CelebAData, epoch=3000, resize=[], image_norm_type=None):
        super(CelebADS, self).__init__(batch_size, reshape_size, Data, epoch=epoch, resize=resize, image_norm_type=image_norm_type)

class ScrewDS(TfrDS):
    def __init__(self, batch_size=BATCH_SIZE, reshape_size=[], Data=ScrewData, epoch=3000, resize=[], image_norm_type=None):
        super(ScrewDS, self).__init__(batch_size, reshape_size, Data, epoch=epoch, resize=resize, image_norm_type=image_norm_type)


if __name__ == '__main__':
    dataset = None
    if not len(sys.argv) > 1:
        print('please select data type')
        print(locals().keys())
        exit()
    DS = locals().get(sys.argv[1], None )
    if not DS:
        print('no data type {} exist'.format(sys.argv[1]))
        print(locals().keys())
        exit() 
    dataset = DS(batch_size=10,resize=[128, 128], image_norm_type='0to1')
    print(dir(dataset))
    print(dataset.next_batch()[0].shape)

    for i in range(1):
        print('===== {}. round ===='.format(i))
        images, labels, codes = dataset.next_batch()
        print('batch iamge shape:{}'.format(images.shape))
        if sys.argv[1] == 'MnistLibDS' or sys.argv[1] == 'MnistTfrDS':
            images = np.squeeze(images, axis=3)
        #print(images.reshape(BATCH_SIZE, 28,28,3))
        print('final batch images shape: {}'.format(images.shape))
        print(np.min(images), ' | ',np.max(images))
        plotFigures(images, labels=codes, realname=labels) 
        
