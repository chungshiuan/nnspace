import tensorflow as tf
SIZE = 10
A1 = tf.get_variable(name = 'A1', 
                     shape = [SIZE, SIZE, SIZE, SIZE], 
                     initializer=tf.truncated_normal_initializer(stddev=0.1),
                     dtype = tf.float32)
A2 = tf.get_variable(name = 'A2', 
                     shape = [SIZE, SIZE, SIZE, SIZE], 
                     initializer=tf.truncated_normal_initializer(stddev=0.1),
                     dtype = tf.float32)

M = tf.matmul(A1, A2)

init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())

with tf.Session() as sess:
    sess.run(init)
    print(sess.run(M))
