import os
from dotenv import load_dotenv

def initEnv():
    current_path = os.path.dirname(__file__)
    dotenv_path = os.path.join(current_path, '..','env.conf')
    load_dotenv(dotenv_path)


