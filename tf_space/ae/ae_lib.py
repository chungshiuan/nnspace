import __init__
import sys, os, time
import tensorflow as tf
import numpy as np
from ae.ae_base_net import AEBaseNet
from ae.ae_util import encoder, decoder

LEARNING_RATE = 0.0002

class AELib(AEBaseNet):
    
    def __init__(self, dataset, image_shape):
        super(AELib, self).__init__(dataset, image_shape)
        self.net()
        self._post_jobs()

    def net(self):
        batch_images = tf.placeholder(tf.float32, shape=[None, *self.image_shape], name='ae_input')
        en = encoder(batch_images)
        de = decoder(en, self.image_shape[2])
        de = tf.identity(de, name="g-out")
        tf.summary.image('gen_image',tf.reshape(de, [-1, *self.image_shape]))
        loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=batch_images, logits=de)
        #loss = tf.squared_difference(batch_images, de)
        cost = tf.reduce_mean(loss)
        tf.summary.scalar('total loss', cost)

        opt = tf.train.AdamOptimizer(learning_rate = LEARNING_RATE)
        train_ae = opt.minimize(cost)
        self.run_train_tensors = {'_train_opt': train_ae, 
                                  'loss': cost}
        print('== init net ====')


