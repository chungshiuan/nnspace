import tensorflow as tf
from gan.gan_util import batch_normal_and_relu

def encoder(en):
    _, _, _, channel = en.get_shape()
    with tf.variable_scope('enc'):
        en = batch_normal_and_relu(en, False)
        en = tf.layers.conv2d(en, 16, [3, 3], activation=tf.nn.relu, name='conv0', padding='same', strides=2)
        en = batch_normal_and_relu(en, False)
        #en = tf.layers.max_pooling2d(en, [2, 2], strides=2, name='mp0', padding='same')
        en = tf.layers.conv2d(en, 32, [3, 3], activation=tf.nn.relu, name='conv1', padding='same', strides=2)
        en = batch_normal_and_relu(en, False)

        #en = tf.layers.max_pooling2d(en, [2, 2], strides=2, name='mp1', padding='same')
        en = tf.layers.conv2d(en, 64, [3, 3], activation=tf.nn.relu, name='conv2', padding='same', strides=2)
        en = batch_normal_and_relu(en, False)

        #en = tf.layers.max_pooling2d(en, [2, 2], strides=2, name='mp2', padding='same')
        en = tf.layers.conv2d(en, channel, [3, 3], activation=tf.nn.relu, name='conv3', padding='same', strides=2)
        en = batch_normal_and_relu(en, False)

        #en = tf.layers.max_pooling2d(en, [2, 2], strides=2, name='mp3', padding='same')

    return en

def decoder(de, channel=1):
    with tf.variable_scope('dec'):
        _, w, h, c = de.get_shape()
        print('decoder:', w,' ', h, ' ',c)

        de = tf.layers.conv2d_transpose(de, 64, kernel_size=[3, 3], padding='same', strides=[2, 2])
        de = tf.layers.conv2d_transpose(de, 32, kernel_size=[3, 3], padding='same', strides=[2, 2])
        de = tf.layers.conv2d_transpose(de, 16, kernel_size=[3, 3], padding='same', strides=[2, 2])
        de = tf.layers.conv2d_transpose(de, channel, kernel_size=[3, 3], padding='same', strides=[2, 2])
    return de 

def decoder_rc(de, channel=1):
     with tf.variable_scope('dec'):
        _, w, h, c = de.get_shape()
        print('decoder:', w,' ', h, ' ',c)
        de = tf.image.resize_images(de, size=(2*w, 2*h), method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        de = tf.layers.conv2d(inputs=de, filters=64, kernel_size=[3,3], padding='same', activation=tf.nn.relu)
        de = tf.image.resize_images(de, size=(4*w, 4*h), method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        de = tf.layers.conv2d(inputs=de, filters=32, kernel_size=[3,3], padding='same', activation=tf.nn.relu)
        de = tf.image.resize_images(de, size=(8*w, 8*h), method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        de = tf.layers.conv2d(inputs=de, filters=16, kernel_size=[3,3], padding='same', activation=tf.nn.relu)
        de = tf.image.resize_images(de, size=(16*w, 16*h), method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        de = tf.layers.conv2d(inputs=de, filters=channel, kernel_size=[3,3], padding='same', activation=tf.nn.relu)
     return de
  
