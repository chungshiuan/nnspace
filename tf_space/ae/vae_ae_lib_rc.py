import sys, os, time, __init__
import tensorflow as tf
import numpy as np
from ae.ae_base_net import AEBaseNet
from ae.ae_util import encoder, decoder_rc
from utils import check_path
from plot import plotFigures,genImage

LEARNING_RATE = 0.002
MODEL_FOLDER = os.path.join(check_path('./models'), 'mnist')

class AELib(AEBaseNet):
    
    def __init__(self, dataset, image_shape):
        super(AELib, self).__init__(dataset, image_shape)
        self.net()
        self._post_jobs()

    def net(self):
        batch_images = tf.placeholder(tf.float32, shape=[None, *self.image_shape], name='ae_input')
        en = encoder(batch_images)
        en = tf.identity(en ,name='ed')
        de = decoder_rc(en, self.image_shape[2])
        de = tf.identity(de, name="g-out")
#        tf.summary.image('bat_image',tf.reshape(batch_images, [-1, *self.image_shape]))
#        tf.summary.image('gen_image',tf.reshape(de, [-1, *self.image_shape]))
        tf.summary.image('bat_image',batch_images)
        tf.summary.image('gen_image',de)

#        loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=batch_images, logits=de)
        loss = tf.squared_difference(batch_images, de)
        cost = tf.reduce_mean(loss)
        tf.summary.scalar('total loss', cost)

        opt = tf.train.AdamOptimizer(learning_rate = LEARNING_RATE)
        train_ae = opt.minimize(cost)
        self.run_train_tensors = {'_train_opt': train_ae,
                                  '_encode': en, 
                                  'loss': cost}
        print('== init net ====')

    def predict(self, en_in):
        with tf.Session() as sess:
            saver = tf.train.import_meta_graph(MODEL_FOLDER+'.meta')
            saver.restore(sess, MODEL_FOLDER)
            en_input = tf.get_default_graph().get_tensor_by_name('ed:0')
            gen_out = tf.get_default_graph().get_tensor_by_name('g-out:0') 
            feed_dict = {en_input:en_in}
            images = sess.run(gen_out, feed_dict=feed_dict)
            #images = result.reshape((-1, *self.image_shape[0:2]))
            #images = -1*(images-1) 
            plotFigures(images)

