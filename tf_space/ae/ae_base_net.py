import tensorflow as tf
import traceback
import numpy as np
import __init__, os, time
from plot import plotFigures,genImage
from utils import check_path
from base_net import BaseNet

BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
MODEL_FOLDER = os.path.join(check_path('./models'), 'mnist')
LOGS_PATH = os.path.join('./tmp_2l_relu',str(int(time.time())))

class AEBaseNet(BaseNet):
    def __init__(self, dataset, image_shape):
        super(AEBaseNet, self).__init__(dataset)
        self.image_shape = image_shape

    def train(self):
        with tf.Session() as sess:
            sess.run(self.init)
            loop = 0

            while True:
                try:
                    batch_x, _, _ = self.dataset.next_batch(BATCH_SIZE)
                    ae_input = tf.get_default_graph().get_tensor_by_name('ae_input:0') 
                    feed_dict = {ae_input: batch_x}
                    result = sess.run(list(self.run_train_tensors.values()), feed_dict=feed_dict)
                    if not self.run_train_tensors.get('_summary_op')== None:
                        self.writer.add_summary(result[-1], loop)
                    if loop % 20 == 0 or loop == 0:
                        print('=========== {}'.format(loop))
                        for i, i_value in enumerate(result):
                            i_key = list(self.run_train_tensors.keys())[i]
                            if not i_key.startswith('_'):
                                print('{} : {}'.format(i_key, i_value))  
                        self.saver.save(sess, MODEL_FOLDER)
                    if loop % 200 == 0 :
                        # save output
                        print(list(result[1].reshape(-1,np.prod(result[1].shape[1:]))[0]))
                        pass
                    loop += 1
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print(e)
                    traceback.print_exc()
                    break

    def predict(self):
        with tf.Session() as sess:
            saver = tf.train.import_meta_graph(MODEL_FOLDER+'.meta')
            saver.restore(sess, MODEL_FOLDER)
            batch_x, _, _ = self.dataset.next_batch(BATCH_SIZE)
            ae_input = tf.get_default_graph().get_tensor_by_name('ae_input:0')
            gen_out = tf.get_default_graph().get_tensor_by_name('g-out:0') 
            feed_dict = {ae_input: batch_x}
            images = sess.run(gen_out, feed_dict=feed_dict)
            #images = result.reshape((-1, *self.image_shape[0:2]))
            #images = -1*(images-1) 
            plotFigures(images)


