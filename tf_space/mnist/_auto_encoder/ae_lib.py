import __init__
import sys, os, time
import tensorflow as tf
import numpy as np
from ae_base_net import AEBaseNet
from dataset_factory import MnistLibDS
from utils import check_path
from ae_util import encoder, decoder

BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
LEARNING_RATE = 0.0002

class AELib(AEBaseNet):
    
    def __init__(self, dataset):
        super(AELib, self).__init__(dataset)
        self.net()
        self._post_jobs()

    def net(self):
        batch_images = tf.placeholder(tf.float32, shape=[None, 28, 28, 1], name='ae_input')
        en = encoder(batch_images)
        de = decoder(en)
        #de = tf.identity(de, name="g-out")
        tf.summary.image('gen_image',tf.reshape(de, [-1, 28,28,1]))
        loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=batch_images, logits=de)
        #loss = tf.squared_difference(batch_images, de)
        cost = tf.reduce_mean(loss)
        tf.summary.scalar('total loss', cost)

        opt = tf.train.AdamOptimizer(learning_rate = LEARNING_RATE)
        train_ae = opt.minimize(cost)
        self.run_train_tensors = {'_train_opt': train_ae, 
                                  'loss': cost}
        print('== init net ====')

if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = AELib(MnistLibDS(reshape_size = [BATCH_SIZE, 28, 28, 1]))    
    getattr(net, exe_type)()
