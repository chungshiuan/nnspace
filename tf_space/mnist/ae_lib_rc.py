import sys, os, time, __init__
import tensorflow as tf
import numpy as np
from dataset_factory import MnistLibDS
from ae.ae_lib_rc import AELib

BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))

if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = AELib(MnistLibDS(reshape_size = [BATCH_SIZE, 28, 28, 1]), image_shape=[28, 28, 1])    
    getattr(net, exe_type)()
