import sys, os, time, __init__
from gan.dcgan import DCGAN
from dataset_factory import MnistTfrDS
from utils import check_path

MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')


if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = DCGAN(MnistTfrDS(reshape_size = [-1, 28, 28, 1]), disc_image_shape=[28,28,1])    
    getattr(net, exe_type)()
