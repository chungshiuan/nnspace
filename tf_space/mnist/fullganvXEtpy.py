import sys, os, time, __init__
from gan.fullgan import FULLGANvXEtpy as GANClass
from dataset_factory import MnistTfrDS

from utils import check_path

MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
size3 = [3, 3]
size5 = [5, 5]
size2 = [2, 2]

if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = GANClass(dataset=MnistTfrDS(epoch=3000, reshape_size=[-1, 28, 28, 1], image_norm_type='0to1'), 
                dis_k_size=None,
                dis_k_stride=None, 
                dis_p_size=None,
                dis_p_stride=None,
                dis_namespace='dis',
                dis_out_activate='sigmoid',
                dis_layers=1,
                gen_k_size=None,
                gen_k_stride=None,
                gen_namespace='gen',
                gen_out_activate='sigmoid',
                gen_layers=1,
                image_size=[28, 28],
                image_channel=1,
                filepath=__file__)    

    getattr(net, exe_type)()
