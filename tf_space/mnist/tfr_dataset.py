import os, sys
import __init__
import pandas as pd
from scipy.stats import itemfreq
from zipfile import ZipFile
import tensorflow as tf
from io import BytesIO
import PIL.Image
from base_dataset import DataSet
import numpy as np
from plot import plotFigures,genImage
import utils
from tf_space.mnist.const import *
sesst=tf.Session()

class Data(DataSet):
    def __init__(self, batch_size, folder='Mnist', resize=[], image_norm_type=None):
        super(Data, self).__init__(folder=folder, batch_size=batch_size, resize=resize, image_norm_type=image_norm_type)

    def parser(self, record):
        features = tf.parse_single_example( record,
                                            features = {
                                            'label': tf.FixedLenFeature([], tf.int64),
                                            'image_raw': tf.FixedLenFeature([], tf.string),
                                            'height': tf.FixedLenFeature([], tf.int64),
                                            'width': tf.FixedLenFeature([], tf.int64),
                                            'depth': tf.FixedLenFeature([], tf.int64)
                                            })
        record_image = tf.decode_raw(features['image_raw'], tf.uint8) #shape (128*128*3)
        label = features['label']
        height = tf.cast(features['height'], tf.int32)
        width = tf.cast(features['width'], tf.int32)
        channel = tf.cast(features['depth'], tf.int32)
        image = tf.reshape(record_image, tf.stack([height, width, channel]))
        image.set_shape([WH_SIZE,WH_SIZE, 1])
#        image = tf.div(tf.cast(image,tf.float32), 127.5) - 1 
#        image = tf.div(tf.cast(image, tf.float32), 257.)
        image = self.get_image_norm(image)
        if len(self.resize) == 2:
            image = tf.image.resize_images(image, self.resize)
        return image, label, height, width, channel


if __name__ == '__main__':
    dog = Data(batch_size=32,resize=[48,48], image_norm_type='-1to1')
    if sys.argv[1] == 'gen':
        pass
    elif sys.argv[1] == 'image':
        iterator =  dog.get_records(10)
        image_batch, label_batch, height, width, channel = iterator.get_next()
        with tf.Session() as sess:
            sess.run((tf.global_variables_initializer(),tf.local_variables_initializer()))
            sess.run(iterator.initializer)
            ep = 0
            while True:
                try:
                    labels, images, h,w,c = sess.run([label_batch, image_batch,  height, width, channel])
                    print('--------------------------------', ep)
                    #print(len(img))
                    print(h,' | ',w, '|',c)
                    images = np.squeeze(images, axis=3)
                    print(np.max(images), np.min(images))
                    plotFigures(images, realname=labels)

                except tf.errors.OutOfRangeError:
                    print('???? out of range', ) 
                    break
                except Exception as e:
                    print(e)
                    break
                ep += 1
    else:
        pass
