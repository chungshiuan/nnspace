import sys, os, time, __init__
from gan.wdcgan_gp import WDCGANGPv2
from dataset_factory import MnistTfrDS

from utils import check_path

MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
size3 = [3, 3]
size5 = [5, 5]
size2 = [2, 2]

if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = WDCGANGPv2(dataset=MnistTfrDS(epoch=3000, reshape_size=[-1, 28, 28, 1]), 
                dis_k_size=size5,
                dis_k_stride=[2, 2], 
                dis_p_size=size2,
                dis_p_stride=[2, 2],
                dis_namespace='dis',
                gen_k_size=size5,
                gen_k_stride=[2, 2],
                gen_namespace='gen',
                image_size=[28, 28],
                image_channel=1)    

    getattr(net, exe_type)()
