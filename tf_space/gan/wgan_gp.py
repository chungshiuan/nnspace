import sys, os, time
import tensorflow as tf
from functools import reduce
import numpy as np
from .gan_base_net import GANUBTrainNet
from .gan_util import generator_simple, discriminator_simple 
from utils import check_path

NOISE_DIM = 100
LEARNING_RATE = 0.0004
MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'dogbreed')

class WGANGP(GANUBTrainNet):
    
    def __init__(self, dataset, disc_image_shape):
        super(WGANGP, self).__init__(dataset)
        self.disc_image_shape = disc_image_shape
        self.disc_image_dim = reduce((lambda x, y: x*y),disc_image_shape)
        self.net()
        self._post_jobs()

    def net(self):
        scale=10.0
        gen_input_place = tf.placeholder(tf.float32, shape=[None, NOISE_DIM], name='gen_input')
        disc_input = tf.placeholder(tf.float32, shape=[None, self.disc_image_dim], name='disc_input')
        tf.summary.histogram(name = 'disc_input-{}'.format('org'), values = disc_input)
        #disc_input = tf.contrib.layers.batch_norm(disc_input, epsilon=1e-5)
        tf.summary.histogram(name = 'disc_input-{}'.format('btch'), values = disc_input)

        gen_input = gen_input_place[0:tf.shape(disc_input)[0],:]
        gen_fake = generator_simple(gen_input, *self.disc_image_shape) 
        tf.summary.image('gen_image',tf.reshape( gen_fake, [-1, *self.disc_image_shape]))
        disc_real = discriminator_simple(disc_input, need_sigm=False)
        disc_fake = discriminator_simple(gen_fake, reuse = True, need_sigm=False)
        #gen_loss = tf.reduce_mean(gen_fake) change to disc_fake
        gen_loss = tf.reduce_mean(disc_fake) 
        tf.summary.scalar('gen loss',gen_loss)
        disc_loss_o = tf.reduce_mean(disc_real) - tf.reduce_mean(disc_fake)
        tf.summary.scalar('disc loss', disc_loss_o)
        epsilon = tf.random_uniform([], 0.0, 1.0)
        #penalty = epsilon*disc_input + (1-epsilon)*gen_fake
        penalty = disc_input + epsilon*(gen_fake-disc_input)

        disc_penalty = discriminator_simple(penalty, reuse = True, need_sigm=False)
        disc_penalty_grad = tf.gradients(disc_penalty, penalty)[0]
        disc_penalty_grad = tf.sqrt(tf.reduce_sum(tf.square(disc_penalty_grad), axis=1))
        pena_loss = tf.reduce_mean(scale*tf.square(disc_penalty_grad -1.0))
        tf.summary.scalar('pena loss', pena_loss)
        disc_loss = disc_loss_o + pena_loss
        tf.summary.scalar('total disc loss',disc_loss)
        optimizer_gen = tf.train.RMSPropOptimizer(learning_rate=LEARNING_RATE)
        optimizer_disc = tf.train.RMSPropOptimizer(learning_rate=LEARNING_RATE) 

        #optimizer_gen = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE)
        #optimizer_disc = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE) 
        tvars = tf.trainable_variables()
        disc_vars = [var for var in tvars if 'd-' in var.name]
        print('disc vars: {}'.format(disc_vars))
        gen_vars = [var for var in tvars if 'g-' in var.name]
        print('gen vars: {}'.format(gen_vars))
        train_disc = optimizer_disc.minimize(disc_loss, var_list=disc_vars)
        train_gen = optimizer_gen.minimize(gen_loss, var_list=gen_vars)

        self.run_train_tensors = {'_train_gen': train_gen, 
                                  '_train_disc': train_disc, 
                                  'disc_loss_o': disc_loss_o,
                                  'pena_loss': pena_loss, 
                                  'gen_loss': gen_loss, 
                                  'disc_loss': disc_loss}
        self.run_predict_tensors = {'gen_output': gen_fake}



