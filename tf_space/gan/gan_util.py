import tensorflow as tf
import math, os
import __init__
import uuid
from tensorflow.python.training import moving_averages

LEARNING_RATE = 0.0002
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
def dense_composed(inputs,
                   units,
                   activation=None,
                   use_bias=True,
                   kernel_initializer=None,
                   bias_initializer=tf.zeros_initializer(),
                   kernel_regularizer=None,
                   bias_regularizer=None,
                   activity_regularizer=None,
                   kernel_constraint=None,
                   bias_constraint=None,
                   trainable=True,
                   name=None,
                   reuse=None,
                   use_layers=True):
    if use_layers:
        return tf.layers.dense(
                     inputs=inputs,
                     units=units,
                     activation=activation,
                     use_bias=use_bias,
                     kernel_initializer=kernel_initializer,
                     bias_initializer=bias_initializer,
                     kernel_regularizer=kernel_regularizer,
                     bias_regularizer=bias_regularizer,
                     activity_regularizer=activity_regularizer,
                     kernel_constraint=kernel_constraint,
                     bias_constraint=bias_constraint,
                     trainable=trainable,
                     name=name,
                     reuse=reuse)
    else:
        print('dense ================================= use_layers:{}'.format(use_layers))
        input_shape=inputs.get_shape().as_list()
        print(kernel_initializer)
        kernel = tf.get_variable(name = '{}-w'.format(name),
                                 shape=[input_shape[1], units], 
                                 initializer=kernel_initializer,
                                 dtype = tf.float32, 
                                 trainable = True)
        print('?????????????')
        d_out = tf.matmul(inputs, spectral_norm(kernel))
#        d_out = tf.matmul(inputs, kernel)

        if use_bias:
            bias = tf.get_variable(name = '{}-b'.format(name),
                                   shape=[units],
                                   initializer=bias_initializer,
                                   dtype = tf.float32,
                                   trainable = True)
            d_out = tf.nn.bias_add(d_out, bias)
        return d_out

def conv2d_composed(inputs,
                    filters,
                    kernel_size,
                    strides=(1, 1),
                    padding='valid',
                    data_format='channels_last',
                    dilation_rate=(1, 1),
                    activation=None,
                    use_bias=True,
                    kernel_initializer=None,
                    bias_initializer=tf.zeros_initializer(),
                    kernel_regularizer=None,
                    bias_regularizer=None,
                    activity_regularizer=None,
                    kernel_constraint=None,
                    bias_constraint=None,
                    trainable=True,
                    name=None,
                    reuse=None,
                    use_layers=True):
    if use_layers:
        return tf.layers.conv2d(
                    inputs=inputs,
                    filters=filters,
                    kernel_size=kernel_size,
                    strides=strides,
                    padding=padding,
                    data_format=data_format,
                    dilation_rate=dilation_rate,
                    activation=activation,
                    use_bias=use_bias,
                    kernel_initializer=kernel_initializer,
                    bias_initializer=bias_initializer,
                    kernel_regularizer=kernel_regularizer,
                    bias_regularizer=bias_regularizer,
                    activity_regularizer=activity_regularizer,
                    kernel_constraint=kernel_constraint,
                    bias_constraint=bias_constraint,
                    trainable=trainable,
                    name=name,
                    reuse=reuse)
    else:
        print('conv2d ================================= use_layers:{}'.format(use_layers))
        input_shape=inputs.get_shape().as_list()
        print([*kernel_size, input_shape[-1], int(filters)])
        kernel = tf.get_variable(name='{}-w'.format(name),
                                 shape=[*kernel_size, input_shape[-1], int(filters)],
                                 initializer=kernel_initializer,
                                 dtype=tf.float32, 
                                 trainable=True)
        conv = tf.nn.conv2d(input=inputs, 
                            filter=spectral_norm(kernel),
#                            filter=kernel, 
                            strides=[1,*strides,1], 
                            padding='SAME')
        print('!!!!!!!!!!!!!!!!!',conv)

        if use_bias:
            biases = tf.get_variable(name='{}-b'.format(name),
                                     shape=[int(filters)],
                                     initializer=bias_initializer,
                                     dtype=tf.float32, 
                                     trainable=True)
            conv = tf.nn.bias_add(conv, biases)
        return conv

def deconv2d_composed(inputs,
                      filters,
                      kernel_size,
                      strides=(1, 1),
                      padding='valid',
                      data_format='channels_last',
                      activation=None,
                      use_bias=True,
                      kernel_initializer=None,
                      bias_initializer=tf.zeros_initializer(),
                      kernel_regularizer=None,
                      bias_regularizer=None,
                      activity_regularizer=None,
                      kernel_constraint=None,
                      bias_constraint=None,
                      trainable=True,
                      name=None,
                      reuse=None,
                      use_layers=True):
    if use_layers:
        return tf.layers.conv2d_transpose(
                      inputs=inputs,
                      filters=filters,
                      kernel_size=kernel_size,
                      strides=strides,
                      padding=padding,
                      data_format=data_format,
                      activation=activation,
                      use_bias=use_bias,
                      kernel_initializer=kernel_initializer,
                      bias_initializer=bias_initializer,
                      kernel_regularizer=kernel_regularizer,
                      bias_regularizer=bias_regularizer,
                      activity_regularizer=activity_regularizer,
                      kernel_constraint=kernel_constraint,
                      bias_constraint=bias_constraint,
                      trainable=trainable,
                      name=name,
                      reuse=reuse) 
    else:
        print('deconv2d ================================= use_layers:{}'.format(use_layers))
        input_shape = inputs.get_shape().as_list()
        output_shape =  [input_shape[0], input_shape[1] * strides[0], input_shape[2] * strides[1], filters]
        output_shape =  [tf.shape(inputs)[0], input_shape[1] * strides[0], input_shape[2] * strides[1], filters]

        print(output_shape)
        kernels = tf.get_variable(name='{}-w'.format(name),
                                  shape=[*kernel_size,filters, input_shape[-1]],
                                  initializer=kernel_initializer,
                                  dtype=tf.float32, 
                                  trainable=True)
           
        conv = tf.nn.conv2d_transpose(value=inputs, 
                                      filter=spectral_norm(kernels), 
#                                      filter=kernels, 
                                      output_shape=output_shape,
                                      strides=[1,*strides,1], 
                                      padding=padding)
        if use_bias:
            biases = tf.get_variable(name='{}-b'.format(name),
                                     shape=[output_shape[-1]],
                                     initializer=bias_initializer,
                                     dtype=tf.float32, 
                                     trainable=True)
            conv = tf.nn.bias_add(conv, biases)
        return conv

def gen_full_layer(f_in, img_w, img_h, img_c, pre_name, stddev=0.02, need_relu=True):
    a = full_layer(f_in, img_w*img_h*img_c, pre_name, stddev=stddev, need_relu=need_relu)
    a = tf.reshape(a, [-1, img_w, img_h, img_c])
    #a = tf.contrib.layers.batch_norm(a, epsilon=1e-5)
    if need_relu:
        a = tf.nn.relu(a)
    return a

def full_layer(f_in, hidden_size, pre_name, stddev=0.02, need_relu=True):
    w = tf.get_variable( name = '{}-w'.format(pre_name),
                         shape=[int(f_in.shape[1]), hidden_size], 
                         initializer=tf.truncated_normal_initializer(stddev=stddev),
                         dtype = tf.float32, 
                         trainable = True)
#    tf.summary.histogram(name = '{}-w'.format(pre_name), values = w)

    b = tf.get_variable(name = '{}-b'.format(pre_name),
                    shape=[hidden_size],
                    initializer=tf.truncated_normal_initializer(stddev=stddev),
                    dtype = tf.float32,
                    trainable = True)
#    tf.summary.histogram(name = '{}-b'.format(pre_name), values = b)

    a = tf.nn.bias_add(tf.matmul(f_in, spectral_norm( w)),b)
    if need_relu:
        a = tf.nn.relu(a)
#    tf.summary.histogram(name = '{}-a'.format(pre_name), values = a)
    return a

def cnn_layer(c_in, 
              k_size,
              pre_name, 
              k_strides = [1,2,2,1], 
              p_size = [1,2,2,1], 
              p_strides = [1,2,2,1], 
              stddev = 0.02,
              need_relu = True,
              need_batch_normal=False):
     filter = tf.get_variable(name='{}-w'.format(pre_name),
                              shape=k_size,
                              initializer=tf.truncated_normal_initializer(stddev=stddev),
                              dtype=tf.float32, 
                              trainable=True)
#     tf.summary.histogram(name = '{}-w'.format(pre_name), values = filter)

     biases = tf.get_variable(name='{}-b'.format(pre_name),
                              shape=[k_size[3]],
                              initializer=tf.truncated_normal_initializer(stddev=stddev),
                              dtype=tf.float32, 
                              trainable=True)
#     tf.summary.histogram(name = '{}-b'.format(pre_name), values = biases)
     conv = tf.nn.conv2d(c_in, spectral_norm(filter), strides=k_strides, padding='SAME')
     conv_o = tf.nn.bias_add(conv, biases)
     if need_relu:
         conv_o= tf.nn.relu(conv_o)
#     tf.summary.histogram(name = '{}-a'.format(pre_name), values = conv_o)
     return  conv_o

def deconv2d(input_, 
             output_shape,
             k_h=3, k_w=3, d_h=2, d_w=2, 
             stddev=0.02,
             name="deconv2d"):
  with tf.variable_scope(name):
    # filter : [height, width, output_channels, in_channels]
    w = tf.get_variable('{}-deconv-w'.format(name), [k_h, k_w, output_shape[-1], input_.get_shape()[-1]],
              initializer=tf.random_normal_initializer(stddev=stddev))
    
    try:
      deconv = tf.nn.conv2d_transpose(input_, w, output_shape=output_shape,
                strides=[1, d_h, d_w, 1])

    # Support for verisons of TensorFlow before 0.7.0
    except AttributeError:
      deconv = tf.nn.deconv2d(input_, w, output_shape=output_shape,
                strides=[1, d_h, d_w, 1])

    biases = tf.get_variable('{}-deconv-b'.format(name), [output_shape[-1]], initializer=tf.constant_initializer(0.0))
    print('--------------1')
    print(deconv)
    print('--------------')
    print(biases)
    print('??????????',deconv.get_shape())
    deconv = tf.reshape(tf.nn.bias_add(deconv, biases), deconv.get_shape())
    print('!!!!!',deconv)
    return deconv

def decov(de,
          num_filters,
          k_size,
          pre_name,
          padding='same',
          strides=[2,2],
          need_relu=True):
    de = tf.layers.conv2d_transpose(de, 
                                    num_filters, 
                                    kernel_size=k_size, 
                                    name='{}-decov'.format(pre_name), 
                                    padding=padding, 
                                    strides=strides)    
    if need_relu:
        de = tf.nn.relu(de)
    return de

def batch_normal_and_relu(node, need_relu=True):
#    node = tf.contrib.layers.batch_norm(node, epsilon=1e-5)
    node = tf.contrib.layers.batch_norm(node,
                      decay=0.9, 
                      updates_collections=None,
                      epsilon=1e-5,
                      scale=True,
                      is_training=True)
    if need_relu:
       return tf.nn.relu(node)
    return node

def lrelu(x, leak=0.2, name="lrelu"):
    return tf.maximum(x, leak*x, name=name) 

def conv_out_size_same(size, stride):
    return int(math.ceil(float(size) / float(stride)))

def generator_dc2(g_in, s_w, s_h, img_c=3, depth = 128):
    with tf.variable_scope("generator") as scope:
        stride = 3
        s_h2, s_w2 = conv_out_size_same(s_h, stride), conv_out_size_same(s_w, stride)
        s_h4, s_w4 = conv_out_size_same(s_h2, stride), conv_out_size_same(s_w2, stride)
        s_h8, s_w8 = conv_out_size_same(s_h4, stride), conv_out_size_same(s_w4, stride)
        s_h16, s_w16 = conv_out_size_same(s_h8, stride), conv_out_size_same(s_w8, stride)
        print('2:', s_h2)
        print('4:', s_h4)
        print('8:', s_h8)
        print('16:', s_h16)

        g = gen_full_layer(g_in, s_w16, s_h16, depth*8,'g-f-1', need_relu=False)

        g = tf.reshape(g, [-1, s_h16, s_w16, depth*8])
        g = batch_normal_and_relu(g)

        g = deconv2d(g, [BATCH_SIZE, s_h8, s_w8, depth*4], name='g-d-1', d_w=stride, d_h=stride)
        g = batch_normal_and_relu(g)

        g = deconv2d(g, [BATCH_SIZE, s_h4, s_w4, depth*2], name='g-d-2', d_w=stride, d_h=stride)
        g = batch_normal_and_relu(g)
                     
        g = deconv2d(g, [BATCH_SIZE, s_h2, s_w2, img_c], name='g-d-3', d_w=stride, d_h=stride) # 3 is channel 3
        g = batch_normal_and_relu(g)
        g = tf.image.resize_images(g, [s_w, s_h])
        return tf.nn.tanh(g)
    

 
def generator_dc(g_in, img_w, img_h, img_c=3):
    depth = 1024
    f_w = int(math.ceil(float(img_w)/float(16)))
    f_h = int(math.ceil(float(img_h)/float(16)))
    g = gen_full_layer(g_in, f_w, f_h, depth,'g-f-1', need_relu=False)
    g = batch_normal_and_relu(g)
    g = tf.reshape(g, [-1, f_w, f_h, depth])
    g = decov(g, int(depth/2), [3, 3], 'g-c-1', need_relu=False)
    g = batch_normal_and_relu(g)
    g = decov(g, int(depth/4), [3, 3], 'g-c-2', need_relu=False)
    g = batch_normal_and_relu(g)
#    g = decov(g, int(depth/8), [3, 3], 'g-c-3', need_relu=False)
#    g = batch_normal_and_relu(g)
    g = decov(g, img_c, [3, 3], 'g-c-4', need_relu=False)
    g = tf.image.resize_images(g, [img_w, img_h])
    g = tf.nn.tanh(g)
    g = tf.identity(g, name="g-out")
    return g
  

def generator(g_in, img_w, img_h, img_c=3):
    gf = gen_full_layer(g_in, img_w, img_h, img_c,'g-f-1',need_relu=False)
    gf = batch_normal_and_relu(gf)
    print('g_in.shape',g_in.shape)
    c1_channel = math.ceil(int(g_in.shape[1])/2)
    c2_channel = math.ceil(int(g_in.shape[1])/4)
    gc = cnn_layer(gf, [3, 3, img_c, c1_channel], 'g-c-1', need_relu=False)
    gc = tf.image.resize_images(gc, [2*img_w, 2*img_h])
    gc = batch_normal_and_relu(gc)
    gc = cnn_layer(gc, [3, 3, c1_channel, c2_channel], 'g-c-2', need_relu=False)
    gc = tf.image.resize_images(gc, [2*img_w, 2*img_h])
    gc = batch_normal_and_relu(gc)
    gc = cnn_layer(gc, [3, 3, c2_channel, img_c], 'g-c-3', need_relu = False)
    gc = tf.sigmoid(gc)
    gc = tf.identity(gc, name="g-out")
    return gc

def discriminator(d_in, reuse=False, need_sigm=True):
    with tf.variable_scope(tf.get_variable_scope(), reuse=reuse) as scope:
        dc = cnn_layer(d_in, [3, 3, int(d_in.get_shape()[3]), 32], 'd-c-1')
        dc = tf.nn.avg_pool(dc, [1,2,2,1], strides = [1,2,2,1], padding='SAME')
        dc = batch_normal_and_relu(dc)

        dc = cnn_layer(dc, [3, 3, 32, 64], 'd-c-2')
        dc = tf.nn.avg_pool(dc, [1,2,2,1], strides = [1,2,2,1], padding='SAME')
        dc = batch_normal_and_relu(dc)

        dc = cnn_layer(dc, [3, 3, 64, 128], 'd-c-3')
        dc = tf.nn.avg_pool(dc, [1,2,2,1], strides = [1,2,2,1], padding='SAME')
        dc = batch_normal_and_relu(dc)

        df = tf.reshape(dc, [-1, dc.get_shape()[1:4].num_elements()])
#        df = full_layer(df, 1024, 'd-f-3')
        df = full_layer(df, 1, 'd-f-4', need_relu=False) # ???? why is unit 1
             
        if need_sigm:
            df = tf.sigmoid(df)
        return df

def generator_simple(g_in, img_w, img_h, img_c=3):
#    gf = full_layer(g_in, img_w*img_h*img_c, 'g-f-s-1' )    
    gf = full_layer(g_in, 1024, 'g-f-s-1' )    
#    gf = tf.contrib.layers.batch_norm(gf, epsilon=1e-5)
    gf = full_layer(gf, img_w*img_h*img_c, 'g-f-s-2', need_relu=False)
    gf = tf.nn.sigmoid(gf, name='g-out')
    return gf

def discriminator_simple(d_in, reuse=False, need_sigm=True):
    with tf.variable_scope(tf.get_variable_scope(), reuse=reuse) as scope:
#        df1 = full_layer(d_in, d_in.get_shape()[1], 'd-f-s-1')
        df1 = full_layer(d_in, 1024, 'd-f-s-1')
        df2 = full_layer(df1, 1, 'd-f-s-2', need_relu=False)
        if need_sigm:
            df2 = tf.nn.sigmoid(df2)
        return df2  

def gan_net(gen_in, real_in, img_w, img_h, img_c=1, gen_func = generator_dc2, disc_func = discriminator):
    gen_output = gen_func(gen_in, img_w, img_h, img_c)
    disc_real = disc_func(real_in)
    disc_real = tf.maximum(disc_real, 1e-9)
    disc_gen = disc_func(gen_output, reuse=True)
    disc_gen = tf.maximum(disc_gen, 1e-9)

    l_dis_gg = tf.log(disc_gen)
    gen_loss = -tf.reduce_mean(l_dis_gg)
    l_dis_real = tf.log(disc_real)
    l_dis_gen = tf.log(1.- disc_gen)
    disc_loss = -tf.reduce_mean(l_dis_real + l_dis_gen)
    #disc_loss = -tf.reduce_mean(tf.log(disc_real) + tf.log(1.- disc_gen))
    optimizer_gen = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE)
    optimizer_disc = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE) 
    tvars = tf.trainable_variables()
    disc_vars = [var for var in tvars if 'd-' in var.name]
    print('disc vars: {}'.format(disc_vars))
    gen_vars = [var for var in tvars if 'g-' in var.name]
    print('gen vars: {}'.format(gen_vars))
    #gen_gra = optimizer_gen.compute_gradients(gen_loss, var_list=gen_vars)
    #train_gen = optimizer_gen.apply_gradients(gen_gra)
    #disc_gra = optimizer_disc.compute_gradients(disc_loss, var_list=disc_vars)
    #train_disc = optimizer_gen.apply_gradients(disc_gra)
    train_disc = optimizer_disc.minimize(disc_loss, var_list=disc_vars)
    train_gen = optimizer_gen.minimize(gen_loss, var_list=gen_vars)

    return train_gen, train_disc, gen_loss, disc_loss, gen_output

def gan_net_cross_entropy(gen_in, real_in, img_w, img_h, img_c=1, gen_func = generator_dc, disc_func = discriminator):
    gen_output = gen_func(gen_in, img_w, img_h, img_c)
    disc_real = disc_func(real_in)
    disc_real = tf.maximum(disc_real, 1e-9)
    disc_gen = disc_func(gen_output, reuse=True)
    disc_r_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_real, labels = tf.ones_like(disc_real)))
    disc_g_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_gen, labels = tf.zeros_like(disc_gen)))
    # Loss function for generator
    gen_loss = -tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_gen, labels=tf.zeros_like(disc_gen)))

    optimizer_gen = tf.train.AdamOptimizer(learning_rate=0.0001)
    optimizer_disc_r = tf.train.AdamOptimizer(learning_rate=0.0003)
    optimizer_disc_g = tf.train.AdamOptimizer(learning_rate=0.0003) 

    tvars = tf.trainable_variables()
    disc_vars = [var for var in tvars if 'd-' in var.name]
    print('disc vars: {}'.format(disc_vars))
    gen_vars = [var for var in tvars if 'g-' in var.name]
    print('gen vars: {}'.format(gen_vars))
    train_gen = optimizer_gen.minimize(gen_loss, var_list=gen_vars)
    train_disc_r = optimizer_disc_r.minimize(disc_r_loss, var_list=disc_vars)
    train_disc_g = optimizer_disc_g.minimize(disc_g_loss, var_list=gen_vars)

    return train_gen, train_disc_g, train_disc_r, gen_loss, disc_g_loss, disc_r_loss, gen_output 


def spectral_norm(w, iteration=1):
   with tf.variable_scope(name_or_scope='sn', reuse=tf.AUTO_REUSE):
     print(w)
     w_shape = w.shape.as_list()
     w = tf.reshape(w, [-1, w_shape[-1]])

     u = tf.get_variable('u_{}'.format(uuid.uuid4()), [1, w_shape[-1]], initializer=tf.random_normal_initializer(), trainable=False)
     u_hat = u
     v_hat = None
     for i in range(iteration):
         """
         power iteration
         Usually iteration = 1 will be enough
         """
         v_ = tf.matmul(u_hat, tf.transpose(w))
         v_hat = tf.nn.l2_normalize(v_)
         u_ = tf.matmul(v_hat, w)
         u_hat = tf.nn.l2_normalize(u_)

     u_hat = tf.stop_gradient(u_hat)
     v_hat = tf.stop_gradient(v_hat)
     sigma = tf.matmul(tf.matmul(v_hat, w), tf.transpose(u_hat))

   with tf.control_dependencies([u.assign(u_hat)]):
       w_norm = w / sigma
       w_norm = tf.reshape(w_norm, w_shape)


   return w_norm

# https://www.twblogs.net/a/5c2a5680bd9eee01611460b7
def batch_norm(x, name, _ops, is_train=True):
    """Batch normalization."""
    with tf.variable_scope(name):
        params_shape = [x.get_shape()[-1]]

        beta = tf.get_variable('beta', params_shape, tf.float32,
                               initializer=tf.constant_initializer(0.0, tf.float32))
        gamma = tf.get_variable('gamma', params_shape, tf.float32,
                                initializer=tf.constant_initializer(1.0, tf.float32))

        if is_train is True:
            mean, variance = tf.nn.moments(x, list(range(len(x.get_shape()) - 1)), name='moments')
            #mean, variance = tf.nn.moments(x, [0,1,2], name='moments')

            moving_mean = tf.get_variable('moving_mean', params_shape, tf.float32,
                                          initializer=tf.constant_initializer(0.0, tf.float32),
                                          trainable=False)
            moving_variance = tf.get_variable('moving_variance', params_shape, tf.float32,
                                              initializer=tf.constant_initializer(1.0, tf.float32),
                                              trainable=False)

            _ops.append(moving_averages.assign_moving_average(moving_mean, mean, 0.9))
            _ops.append(moving_averages.assign_moving_average(moving_variance, variance, 0.9))
        else:
            mean = tf.get_variable('moving_mean', params_shape, tf.float32,
                                   initializer=tf.constant_initializer(0.0, tf.float32), trainable=False)
            variance = tf.get_variable('moving_variance', params_shape, tf.float32, trainable=False)

        # epsilon used to be 1e-5. Maybe 0.001 solves NaN problem in deeper net.
        y = tf.nn.batch_normalization(x, mean, variance, beta, gamma, 1e-5)
        y.set_shape(x.get_shape())

        return y

def avg_pool_conv(x, num_filters, kernel_size, k_stride, name='avg_pool_conv'):
    output = x
    with tf.variable_scope(name):
        output = tf.nn.avg_pool(output, 
                                ksize=[1, 2, 2, 1], 
                                strides=[1, 2, 2, 1], 
                                padding='SAME',
                                name='avgpool')
        output = tf.layers.conv2d(output, 
                                  filters=num_filters, 
                                  kernel_size=kernel_size, 
                                  name='conv2d', 
                                  padding='SAME',
                                  activation = None,
                                  kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                  bias_initializer=tf.constant_initializer(0.0), 
                                  strides=k_stride)
    return output

def conv_avg_pool(x, num_filters, kernel_size, k_stride, name='conv_avg_pool'):
    output = x
    with tf.variable_scope(name):
        output = tf.layers.conv2d(output, 
                                  filters=num_filters, 
                                  kernel_size=kernel_size, 
                                  name='conv2d', 
                                  padding='SAME',
                                  activation = None,
                                  kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                  bias_initializer=tf.constant_initializer(0.0), 
                                  strides=k_stride)
        output = tf.nn.avg_pool(output, 
                                ksize=[1, 2, 2, 1], 
                                strides=[1, 2, 2, 1], 
                                padding='SAME',
                                name='avgpool')
    return output

def conv4res(x, num_filters, kernel_size, _ops, name='conv4res'):
    output = x
    with tf.variable_scope(name):
        output = batch_norm(output, 'batch_norm_1', _ops, is_train=True)
        output = tf.nn.relu(output, name='relu_1')
        output = tf.layers.conv2d(output, 
                                  filters=num_filters, 
                                  kernel_size=kernel_size, 
                                  name='conv2d', 
                                  padding='SAME',
                                  activation = None,
                                  kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                  bias_initializer=tf.constant_initializer(0.0), 
                                  strides=[1, 1])
#        output = batch_norm(output, 'batch_norm_2', _ops, is_train=True)
        output = tf.nn.relu(output, name='relu_2')
        output = conv_avg_pool(output, 
                               num_filters=num_filters, 
                               kernel_size=[3, 3], 
                               k_stride=[1,1], 
                               name='conv_avg_pool')
    return output 

def deconv4res(x, num_filters, kernel_size, k_stride, _ops, name='deconv4res'):
    output = x
    with tf.variable_scope(name):
        output = batch_norm(output, 'batch_norm_1', _ops, is_train=True)
        output = tf.nn.relu(output, name='relu_1')
        output = tf.layers.conv2d_transpose(output, 
                                            filters=num_filters, 
                                            kernel_size=kernel_size, 
                                            name='deconv2d', 
                                            padding='SAME', 
                                            activation = None,
                                            kernel_initializer=tf.random_normal_initializer(stddev=0.02),
                                            bias_initializer=tf.constant_initializer(0.0), 
                                            strides=k_stride)
#        output = batch_norm(output, 'batch_norm_2', _ops, is_train=True)
        output = tf.nn.relu(output, name='relu_2')
        output = tf.layers.conv2d(output, 
                                  filters=num_filters, 
                                  kernel_size=kernel_size, 
                                  padding='SAME',
                                  activation = None,
                                  kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                  bias_initializer=tf.constant_initializer(0.0), 
                                  strides=[1,1],
                                  name='conv2d')
    return output 

def resconv(x, num_filters, kernel_size, _ops, name='resconv'):
    output = x
    with tf.variable_scope(name):
        shortcut = avg_pool_conv(x, num_filters=num_filters, kernel_size=[1, 1], k_stride=[1, 1])
        output = conv4res(x, num_filters=num_filters, kernel_size=kernel_size, _ops=_ops)
    return shortcut + output
 
def resdeconv(x, num_filters, kernel_size, k_stride, _ops, name='resdeconv'):
    output = x
    with tf.variable_scope(name):
        shortcut = tf.layers.conv2d_transpose(output, 
                                              filters=num_filters, 
                                              kernel_size=kernel_size, 
                                              padding='SAME', 
                                              activation = None,
                                              kernel_initializer=tf.random_normal_initializer(stddev=0.02),
                                              bias_initializer=tf.constant_initializer(0.0), 
                                              strides=k_stride,
                                              name='deconv2d')

        output = deconv4res(output,
                            num_filters=num_filters,
                            kernel_size=kernel_size,
                            k_stride=k_stride,
                            _ops=_ops)
    return shortcut + output
