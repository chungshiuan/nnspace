import tensorflow as tf
import numpy as np
import __init__, os, time, traceback
from plot import plotFigures,genImage
#from const import *
from utils import check_path
from base_net import BaseNet
from .gan_util import batch_norm, lrelu 
import math
NOISE_DIM = 100
IMAGE_DIM = 784
LEARNING_RATE = 0.0002
#LEARNING_RATE_GEN = 0.0001 0.00005
#LEARNING_RATE_DIS = 0.0004 0.00005
LEARNING_RATE_GEN = 0.00005
LEARNING_RATE_DIS = 0.00005

BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
LOGS_PATH = os.path.join('./tmp_2l_relu',str(int(time.time())))

class GANUBTrainNet(BaseNet):
    def __init__(self, dataset):
        super(GANUBTrainNet, self).__init__(dataset)

    def train(self):
        with tf.Session() as sess:
            sess.run(self.init)
            loop = 0
            ex = 0
            while True:
                try:
                    for i in range(4):
                        noise = self._get_noise('nor')
                        batch_x, _ ,_ = self.dataset.next_batch(BATCH_SIZE)
                        gen_input = tf.get_default_graph().get_tensor_by_name('gen_input:0') 
                        disc_input = tf.get_default_graph().get_tensor_by_name('disc_input:0') 
                        feed_dict = {gen_input: noise, 
                                     disc_input: batch_x}
                        summary, _, d_loss = sess.run([self.run_train_tensors['_summary_op'],
                                              self.run_train_tensors['_train_disc'],
                                              self.run_train_tensors['disc_loss']], feed_dict=feed_dict)
                        self.writer.add_summary(summary,ex)
                        ex += 1
                    noise = self._get_noise('nor')
                    batch_x, _, _ = self.dataset.next_batch(BATCH_SIZE)
                    gen_input = tf.get_default_graph().get_tensor_by_name('gen_input:0') 
                    disc_input = tf.get_default_graph().get_tensor_by_name('disc_input:0') 
                    feed_dict = {gen_input: noise, 
                                 disc_input: batch_x}
                    summary, _, g_loss = sess.run([self.run_train_tensors['_summary_op'],
                                                self.run_train_tensors['_train_gen'],
                                                self.run_train_tensors['gen_loss']], feed_dict=feed_dict)
                    self.writer.add_summary(summary, ex)
                    if loop % 20 == 0 or loop == 0:
                        print('=========== {}'.format(loop))
                        print('{}:{}'.format('disc_loss', d_loss))
                        print('{}:{}'.format('gen_loss', g_loss))
                        self.saver.save(sess, MODEL_FOLDER)
                    loop += 1
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    print(e)
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    traceback.print_exc()
                    break


class GANNet(BaseNet):

    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel, 
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):
        super(GANNet, self).__init__(dataset, filepath)
        self.dis_k_size = dis_k_size
        self.dis_k_stride = dis_k_stride
        self.dis_p_size = dis_p_size
        self.dis_p_stride = dis_p_stride
        self.dis_namespace = dis_namespace
        self.dis_out_activate = dis_out_activate
        self.dis_train_ops = []
        self.dis_layers = dis_layers 
        self.dis_use_layers=dis_use_layers
        self.gen_k_size = gen_k_size
        self.gen_k_stride = gen_k_stride
        self.gen_namespace = gen_namespace
        self.gen_out_activate = gen_out_activate
        self.gen_train_ops = []
        self.gen_layers = gen_layers
        self.gen_use_layers=gen_use_layers 
        self.image_channel = image_channel
        self.image_size = image_size
        self.init_status()
        self.sess = tf.Session()

    def get_activate(self, key):
        return {
            'relu': tf.nn.relu,
            'lrelu': lrelu,
            'sigmoid': tf.nn.sigmoid,
            'tanh': tf.nn.tanh
        }.get(key, lambda m, name=None : m)

    def init_status(self):
        self.input()
        self.net()
        self.get_loss()
        #self.get_real_accuracy()
        #self.get_fake_accuracy()
        self.optimize()
        self.add_batch_norm_ops_after_optimize()
        self._post_jobs()

    def input(self):
        with tf.variable_scope('input', reuse=False) as scope:
            self.gen_in = tf.placeholder(tf.float32, shape=[None, NOISE_DIM], name='gen_input')
            self.dis_in = tf.placeholder(tf.float32, shape=[None, *self.image_size, self.image_channel], name='dis_input')
#            self.gen_in = self.gen_in[0:tf.shape(self.dis_in)[0],:]
  
    def generator(self, in_data):
        pass 
 
    def discriminator(self, in_data, reuse=False):
        pass
 
    def net(self):
        self.gen_out = self.generator(self.gen_in)
        tf.summary.image('gen_image', self.gen_out)
        tf.summary.image('dis_image', self.dis_in)
        self.dis_out_gen = self.discriminator(self.gen_out, reuse=False)
        self.dis_out_real = self.discriminator(self.dis_in, reuse=True)

    def get_loss(self):
        log_comp_dis_gen = tf.log(1- self.dis_out_gen)
        log_comp_dis_gen = 1- self.dis_out_gen
        mean_log_comp_dis_gen = tf.reduce_mean(log_comp_dis_gen)
        log_dis_real = tf.log(self.dis_out_real)
        log_dis_real = self.dis_out_real
        log_dis_gen = tf.log(self.dis_out_gen)
        log_dis_gen = self.dis_out_gen
        mean_log_dis_real = tf.reduce_mean(log_dis_real)
        mean_log_dis_gen = tf.reduce_mean(log_dis_gen)
        tf.summary.scalar('mean_log_comp_dis_gen', mean_log_comp_dis_gen)
        tf.summary.scalar('mean_log_dis_real', mean_log_dis_real) 
        self.dis_loss = -(mean_log_dis_real + mean_log_comp_dis_gen)
        self.gen_loss = -mean_log_dis_gen
        tf.summary.scalar('dis loss', self.dis_loss)
        tf.summary.scalar('gen loss', self.gen_loss) 

    def get_loss_(self):
        log_dis_real = self.dis_out_real
        log_dis_gen = self.dis_out_gen
        mean_log_dis_real = tf.reduce_mean(log_dis_real)
        mean_log_dis_gen = tf.reduce_mean(log_dis_gen)
        self.dis_loss = mean_log_dis_gen - mean_log_dis_real
        tf.summary.scalar('mean_log_dis_gen', mean_log_dis_gen)
        tf.summary.scalar('mean_log_dis_real', mean_log_dis_real) 
        self.gen_loss = -mean_log_dis_gen
        tf.summary.scalar('dis loss', self.dis_loss)
        tf.summary.scalar('gen loss', self.gen_loss) 
    
    def init_optimize(self):
        self.optimizer_gen = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE_GEN)
        self.optimizer_dis = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE_DIS) 

    def set_optimize(self):
        self.init_optimize()
        tvars = tf.trainable_variables()
        self.dis_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.dis_namespace)
        #self.dis_vars = [var for var in tvars if self.dis_namespace in var.name]
        #self.dis_vars_clip = [var.assign(tf.clip_by_value(var, -0.01, 0.01)) for var in self.dis_vars]
        print('disc vars: {}'.format(self.dis_vars))
        self.gen_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.gen_namespace)
        #self.gen_vars = [var for var in tvars if self.gen_namespace in var.name]
        print('gen vars: {}'.format(self.gen_vars))

    def optimize(self):
        self.set_optimize() 
        self.train_dis = self.optimizer_dis.minimize(self.dis_loss, var_list=self.dis_vars)
        self.train_gen = self.optimizer_gen.minimize(self.gen_loss, var_list=self.gen_vars)

    def _set_grad_histogram(self, scope, grads):
        for index, i_grad in enumerate(grads):
            tf.summary.histogram(name='grad_{}_{}'.format(scope, index), values=i_grad)

    def optimize_(self):
        self.set_optimize()
        gen_gra = self.optimizer_gen.compute_gradients(self.gen_loss, var_list=self.gen_vars)
#        self._set_grad_histogram(scope='gen_gra', grads=gen_gra)
        self.train_gen = self.optimizer_gen.apply_gradients(gen_gra)
        dis_gra = self.optimizer_dis.compute_gradients(self.dis_loss, var_list=self.dis_vars)
#        self._set_grad_histogram(scope='dis_gra', grads=dis_gra)
        self.train_dis = self.optimizer_dis.apply_gradients(dis_gra)

    def add_batch_norm_ops_after_optimize(self):
        pass

    def train(self):
        pass

    def predict(self):
        pass 

    def sample(self, filename, sample_size=20):
        try:
            gen_input = tf.get_default_graph().get_tensor_by_name('input/gen_input:0') 
            noise = self._get_noise('uni', shape=[sample_size, NOISE_DIM])
            feed_dict = {gen_input: noise}
            sample_images = self.sess.run(self.gen_out, feed_dict=feed_dict)
            sample_images = np.squeeze(sample_images)
            plotFigures(data=sample_images, panel=False, save_path=os.path.join(self.filepath, '{}.jpg'.format(filename)))
        except tf.errors.OutOfRangeError:
            pass
        except Exception as e:
            print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
            print(e)
            print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
            traceback.print_exc()

    def get_real_accuracy(self):
        dis_out = self.dis_out_real
        print('????????????????????????????????????????',dis_out)
        dis_out = tf.Print(dis_out, ['dddddddddifffffQQQQQQQQQQQQQQ', dis_out], summarize=100)
        if not self.dis_out_activate == 'sigmoid':
            dis_out = tf.nn.sigmoid(self.dis_out_real)
        tf.summary.histogram(name='{}_real_accuracy'.format(self.dis_namespace), values=dis_out)
        corrects = tf.squeeze(tf.where(tf.greater(dis_out, 0.6), tf.ones(tf.shape(dis_out)), tf.zeros(tf.shape(dis_out))))
        
        acc = tf.reduce_mean(tf.cast(corrects, tf.float32))
        tf.summary.scalar('dis_real_accuracy', acc)     

    def get_fake_accuracy(self):
        dis_out = self.dis_out_gen
        if not self.dis_out_activate == 'sigmoid':
            dis_out = tf.nn.sigmoid(self.dis_out_gen)
        tf.summary.histogram(name='{}_gen_accuracy'.format(self.gen_namespace), values=dis_out)
        corrects = tf.squeeze(tf.where(tf.greater(dis_out, 0.6), tf.ones(tf.shape(dis_out)), tf.zeros(tf.shape(dis_out))))
        
        acc = tf.reduce_mean(tf.cast(corrects, tf.float32))
        tf.summary.scalar('dis_fake_accuracy', acc)      
