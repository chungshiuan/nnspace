import sys, os, time, traceback
import tensorflow as tf
import numpy as np
from utils import check_path
from .dcgan import DCGANv2, DCGANv2XEtpy
from gan.gan_util import batch_norm, lrelu, resconv, resdeconv,dense_composed, conv2d_composed, deconv2d_composed

NOISE_DIM = 100
LEARNING_RATE = 5e-5
MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'wganv2xetpy')
LEARNING_RATE_GEN = 0.0001
LEARNING_RATE_DIS = 0.0005
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
NOISE_SHAPE = [BATCH_SIZE, NOISE_DIM]

class WGANv2(DCGANv2XEtpy):
    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter=128,
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):

        super(WGANv2, self).__init__( 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter,
                 filepath,
                 dis_use_layers=dis_use_layers,
                 gen_use_layers=gen_use_layers)


    def get_loss(self):
        dis_real = self.dis_out_real
        dis_gen = self.dis_out_gen
        mean_dis_real = tf.reduce_mean(dis_real)
        mean_dis_gen = tf.reduce_mean(dis_gen)
        self.dis_loss = mean_dis_gen - mean_dis_real
        self.gen_loss = - mean_dis_gen
        tf.summary.scalar('dis loss', self.dis_loss)
        tf.summary.scalar('gen loss', self.gen_loss) 

    def init_optimize(self):
        self.optimizer_gen = tf.train.RMSPropOptimizer(learning_rate=LEARNING_RATE)
        self.optimizer_dis = tf.train.RMSPropOptimizer(learning_rate=LEARNING_RATE) 

    def set_optimize(self):
        super().set_optimize()
        self.dis_vars_clip = [var.assign(tf.clip_by_value(var, -0.01, 0.01)) for var in self.dis_vars]

    def train(self):
        with tf.Session() as sess:
            self.sess.run(self.init)
            loop = 0
            ex = 0
            while True:
                try:
                    gen_input = tf.get_default_graph().get_tensor_by_name('input/gen_input:0') 
                    disc_input = tf.get_default_graph().get_tensor_by_name('input/dis_input:0')
                    for i in range(5):
                        batch_x, _ ,_ = self.dataset.next_batch(BATCH_SIZE)
                        noise = self._get_noise(n_type='uni', shape=[batch_x.shape[0], NOISE_DIM])
                        if batch_x.shape[0] < BATCH_SIZE:
                            print('dis>> noise.shape: {}, images.shape:{}'.format(noise.shape, batch_x.shape))
                            continue
                        feed_dict = {gen_input: noise, 
                                     disc_input: batch_x}
                        _, d_loss = self.sess.run([self.train_dis,
                                              self.dis_loss], feed_dict=feed_dict)
                        self.sess.run(self.dis_vars_clip)
                    ex += 1
                    batch_x, _, _ = self.dataset.next_batch(BATCH_SIZE)
                    noise = self._get_noise(n_type='uni', shape=[batch_x.shape[0], NOISE_DIM])
                    if batch_x.shape[0] < BATCH_SIZE:
                        print('gen>> noise.shape: {}, images.shape:{}'.format(noise.shape, batch_x.shape))
                        continue
                    feed_dict = {gen_input: noise, 
                                 disc_input: batch_x}
                    summary, _, g_loss = self.sess.run([self.summary,
                                          self.train_gen,
                                          self.gen_loss], feed_dict=feed_dict)
                    self.writer.add_summary(summary, ex)
                    if loop % 20 == 0 or loop == 0:
                        print('=========== {}'.format(loop))
                        print('{}:{}'.format('disc_loss', d_loss))
                        print('{}:{}'.format('gen_loss', g_loss))
                        self.saver.save(self.sess, MODEL_FOLDER)
                    if loop% 1000 ==0:
                        self.sample(loop, sample_size=50)
                    loop += 1
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    print(e)
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    traceback.print_exc()
                    break

class WGANGPv2(WGANv2):
    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter=128,
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):
        super(WGANGPv2, self).__init__( 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter,
                 filepath,
                 dis_use_layers=dis_use_layers,
                 gen_use_layers=gen_use_layers)

    def generator(self, in_data):
        init_w = self.init_gen_w
        init_h = self.init_gen_h
        padding = 'SAME'
        num_filters = self.init_num_filters
        de = in_data
        with tf.variable_scope(self.gen_namespace, reuse=False) as scope:
            de = tf.layers.flatten(de)
            de =dense_composed(inputs=de,
                               units=init_w * init_h * self.init_num_filters, 
                               activation=None,
                               kernel_initializer=tf.contrib.layers.xavier_initializer(),
                               bias_initializer=tf.constant_initializer(0.0), 
                               name = 'pre-dense',
                               use_layers=self.gen_use_layers)
            de = tf.reshape(de, [-1,  init_w, init_h, self.init_num_filters])
            de = batch_norm(de, name='pre-dense', _ops=self.gen_train_ops)
            de = tf.nn.relu(de, name='pre-dense-relu')
            for index in range(1, self.gen_layers+1):
                num_filters = int(num_filters/2)
                activation = tf.nn.relu
                if index == self.gen_layers:
                    num_filters = self.image_channel
                    activation = self.get_activate(self.gen_out_activate)
                de = deconv2d_composed(inputs=de, 
                                       filters=num_filters, 
                                       kernel_size=self.gen_k_size, 
                                       name='{}'.format(index), 
                                       padding=padding, 
                                       activation = None,
                                       kernel_initializer=tf.random_normal_initializer(stddev=0.02),
                                       bias_initializer=tf.constant_initializer(0.0), 
                                       strides=self.gen_k_stride,
                                       use_layers=self.gen_use_layers)
                if not index == self.gen_layers: 
                    de = batch_norm(de, name='{}'.format(index), _ops=self.gen_train_ops)
                de = activation(de)
                print(de)
        return de
 
    def discriminator(self, in_data, reuse=False):
        
        padding = 'SAME'
        num_filters = self.init_num_filters/2/(2**self.dis_layers)
        dc = in_data
        with tf.variable_scope(self.dis_namespace, reuse=reuse) as scope:
            for index in range(1, self.dis_layers+1):
                activation = lrelu
                dc = conv2d_composed(inputs=dc, 
                                     filters=num_filters, 
                                     kernel_size=self.dis_k_size, 
                                     name='{}'.format(index), 
                                     padding=padding,
                                     activation = None,
                                     kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                     bias_initializer=tf.constant_initializer(0.0), 
                                     strides=self.dis_k_stride,
                                     use_layers=self.dis_use_layers) 
                if not index == 1: 
                    dc = batch_norm(dc, name='{}'.format(index), _ops=self.dis_train_ops)
                dc = activation(dc)
                num_filters = num_filters*2
                print(dc)
            print(self.dis_k_size)
            print(self.dis_k_stride)
            print(self.get_activate(self.dis_out_activate))
            dc = tf.layers.flatten(dc)
   
            dc = dense_composed(dc,
                                 units=1, 
                                 #activation=self.get_activate(self.dis_out_activate),
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                 bias_initializer=tf.constant_initializer(0.0), 
                                 name = '2-dense',
                                 use_layers=self.dis_use_layers)
        dc = activation=self.get_activate(self.dis_out_activate)(dc)

      
        return dc

    def net_backup(self):
        self.gen_out = self.generator(self.gen_in) 
        tf.summary.image('gen_image', self.gen_out)
        self.dis_out_real = self.discriminator(self.dis_in)
        self.dis_out_gen = self.discriminator(self.gen_out, reuse = True)
        alpha = tf.random_uniform(shape=[], minval=0., maxval=1.)  
        diff = self.gen_out - self.dis_in
        penalty = self.dis_in + alpha*diff
        self.dis_out_pena = self.discriminator(penalty, reuse = True)
        self.disc_penalty_grad = tf.gradients(self.dis_out_pena, penalty)[0]
        self.disc_penalty_grad = tf.sqrt(tf.reduce_sum(tf.square(self.disc_penalty_grad), axis=1))

    def gradient_penalty(self):
        alpha = tf.random_uniform(shape=[self.dataset.batch_size, 1, 1, 1], minval=0., maxval=1.)
        differences = self.gen_out - self.dis_in
        interpolates = self.dis_in + (alpha * differences)
        gradients = tf.gradients(self.discriminator(interpolates, reuse=True), [interpolates])[0]
#        slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1, 2, 3]))
        slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
        gradient_penalty = tf.reduce_mean((slopes-1.)**2)
        return gradient_penalty

    def init_optimize_(self):
        self.optimizer_gen = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE,  beta1=0.5, beta2=0.9)
        self.optimizer_dis = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE,  beta1=0.5, beta2=0.9) 

    def init_optimize(self):
        self.optimizer_gen = tf.train.RMSPropOptimizer(learning_rate=LEARNING_RATE)
        self.optimizer_dis = tf.train.RMSPropOptimizer(learning_rate=LEARNING_RATE) 

    def get_loss(self):
        discount = 1
        scale=10.0
        log_dis_real = self.dis_out_real
        log_dis_gen = self.dis_out_gen
        mean_log_dis_real = discount*tf.reduce_mean(log_dis_real)
        mean_log_dis_gen = tf.reduce_mean(log_dis_gen)
        self.gen_loss = - mean_log_dis_gen
        real_gen_diff = mean_log_dis_gen - mean_log_dis_real
        pena_loss = self.gradient_penalty()
        self.dis_loss = real_gen_diff + scale* pena_loss
        tf.summary.scalar('dis loss', self.dis_loss)
        tf.summary.scalar('gen loss', self.gen_loss) 

    def set_optimize_(self):
        self.init_optimize()
        self.dis_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.dis_namespace)
        self.gen_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.gen_namespace)


    def train(self):
        with tf.Session() as sess:
            self.sess.run(self.init)
            loop = 0
            ex = 0
            while True:
                try:
                    gen_input = tf.get_default_graph().get_tensor_by_name('input/gen_input:0') 
                    disc_input = tf.get_default_graph().get_tensor_by_name('input/dis_input:0')
                    for i in range(5):
                        batch_x, _ ,_ = self.dataset.next_batch(BATCH_SIZE)
                        noise = self._get_noise(n_type='uni', shape=[batch_x.shape[0], NOISE_DIM])
                        if batch_x.shape[0] < BATCH_SIZE:
                            print('dis>> noise.shape: {}, images.shape:{}'.format(noise.shape, batch_x.shape))
                            continue
                        feed_dict = {gen_input: noise, 
                                     disc_input: batch_x}
                        _, d_loss = self.sess.run([self.train_dis,
                                              self.dis_loss], feed_dict=feed_dict)
                    ex += 1
                    batch_x, _, _ = self.dataset.next_batch(BATCH_SIZE)
                    noise = self._get_noise(n_type='uni', shape=[batch_x.shape[0], NOISE_DIM])
                    if batch_x.shape[0] < BATCH_SIZE:
                        print('gen>> noise.shape: {}, images.shape:{}'.format(noise.shape, batch_x.shape))
                        continue
                    feed_dict = {gen_input: noise, 
                                 disc_input: batch_x}
                    summary, _, g_loss = self.sess.run([self.summary,
                                          self.train_gen,
                                          self.gen_loss], feed_dict=feed_dict)
                    self.writer.add_summary(summary, ex)
                    if loop % 20 == 0 or loop == 0:
                        print('=========== {}'.format(loop))
                        print('{}:{}'.format('disc_loss', d_loss))
                        print('{}:{}'.format('gen_loss', g_loss))
                        self.saver.save(self.sess, MODEL_FOLDER)
                    if loop% 1000 ==0 and loop > 60:
                        self.sample(loop, sample_size=50)
                    loop += 1
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    print(e)
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    traceback.print_exc()
                    break

class WGANResGPv2(WGANGPv2):
    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter=128,
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):
        super(WGANResGPv2, self).__init__( 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter,
                 filepath,
                 dis_use_layers=dis_use_layers,
                 gen_use_layers=gen_use_layers)

    def generator(self, in_data):
        init_w = self.init_gen_w*2
        init_h = self.init_gen_h*2
        padding = 'SAME'
        num_filters = self.init_num_filters
        de = in_data
        with tf.variable_scope(self.gen_namespace, reuse=False) as scope:
            de = tf.layers.flatten(de)
            de = tf.layers.dense(de,
                                 units=init_w * init_h * self.init_num_filters, 
                                 activation=None,
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                 bias_initializer=tf.constant_initializer(0.0), 
                                 name = 'pre-dense')
            de = tf.reshape(de, [-1,  init_w, init_h, self.init_num_filters])
            print(de)
            for index in range(1, self.gen_layers+1):
                num_filters = int(num_filters/2)
                if not index == self.gen_layers:
                    de = resdeconv(de, num_filters, self.gen_k_size, self.gen_k_stride, _ops=self.gen_train_ops, name='{}'.format(index))
                else:
                    num_filters = self.image_channel
#                    de = batch_norm(de, name='norm{}'.format(index), _ops=self.gen_train_ops)
                    de = tf.nn.relu(de)
                    de = tf.layers.conv2d(de, 
                                          num_filters, 
                                          kernel_size=self.gen_k_size, 
                                          name='{}'.format(index), 
                                          padding=padding, 
                                          activation = None,
                                          kernel_initializer=tf.random_normal_initializer(stddev=0.02),
                                          bias_initializer=tf.constant_initializer(0.0), 
                                          strides=[1,1])
                    de = self.get_activate(self.gen_out_activate)(de)
                print(de)
        return de
 
    def discriminator(self, in_data, reuse=False):
        
        padding = 'SAME'
        num_filters = int(self.init_num_filters/2/(2**self.dis_layers))
        dc = in_data
        with tf.variable_scope(self.dis_namespace, reuse=reuse) as scope:
            for index in range(1, self.dis_layers+1):
                dc = resconv(dc, 
                               num_filters=num_filters, 
                               kernel_size=self.dis_k_size, 
                               name='{}'.format(index), 
                               _ops=self.dis_train_ops)
                num_filters = num_filters*2
                print(dc)
            dc = tf.layers.flatten(dc)
            dc = tf.layers.dense(dc,
                                 units=1, 
                                 activation=self.get_activate(self.dis_out_activate),
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                 bias_initializer=tf.constant_initializer(0.0), 
                                 name = '2-dense')
        return dc

