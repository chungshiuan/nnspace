import sys, os, time, traceback
import tensorflow as tf
import numpy as np
from functools import reduce
from .gan_base_net import  GANNet
from .gan_util import gan_net, gan_net_cross_entropy, generator_dc, discriminator, lrelu
from utils import check_path
import math
from gan.gan_util import batch_norm 

NOISE_DIM = 100
LEARNING_RATE = 0.0002
MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))


class FULLGANv2(GANNet):
 
    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):
        super(FULLGANv2, self).__init__(dataset, 
                                      dis_k_size, 
                                      dis_k_stride,
                                      dis_p_size,
                                      dis_p_stride,
                                      dis_namespace,
                                      dis_out_activate,
                                      dis_layers,
                                      gen_k_size,
                                      gen_k_stride,
                                      gen_namespace,
                                      gen_out_activate,
                                      gen_layers,
                                      image_size,
                                      image_channel,
                                      filepath,
                                      dis_use_layers=dis_use_layers,
                                      gen_use_layers=gen_use_layers)

    def generator(self, in_data):
        de = in_data
        num_filters = 128
        with tf.variable_scope(self.gen_namespace, reuse=False) as scope:
            for index in range(1, self.gen_layers+1):
                de = tf.layers.flatten(de)
                de = tf.layers.dense(de,
                                     units= num_filters, 
                                     activation=None,
                                     kernel_initializer=tf.contrib.layers.xavier_initializer(), 
                                     name = '{}'.format(index))
                de = tf.nn.relu(de, name='relu-{}'.format(index))
                num_filters = num_filters*2
#                if not index == self.gen_layers: 
#                    de = batch_norm(de, name='{}'.format(index), _ops=self.gen_train_ops)

            de = tf.layers.dense(de,
                                 units=self.image_size[0]*self.image_size[1]*self.image_channel, 
                                 activation=None,
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(), 
                                 name = 'final-dense{}'.format(index))
            de = self.get_activate(self.gen_out_activate)(de, name='final-activate-{}'.format(index))
            de = tf.reshape(de, [-1,  *self.image_size, self.image_channel])
        return de
 
    def discriminator(self, in_data, reuse=False):
        de = in_data
        num_filters = 128
        with tf.variable_scope(self.dis_namespace, reuse=reuse) as scope:
            de = tf.layers.flatten(de)
            for index in range(1, self.dis_layers+1):
                de = tf.layers.dense(de,
                                     units=num_filters, 
                                     activation=None,
                                     kernel_initializer=tf.contrib.layers.xavier_initializer(), 
                                     name = '{}'.format(index))
#                de = batch_norm(de, name='{}'.format(index), _ops=self.dis_train_ops)
                de = tf.nn.relu(de, name='relu-{}'.format(index))
                num_filters = num_filters*2
            de = tf.layers.dense(de,
                                 units=1, 
                                 activation=None,
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(), 
                                 name = 'final-dense{}'.format(index))
            de = self.get_activate(self.gen_out_activate)(de, name='final-activate-{}'.format(index))
        return de

    def init_optimize(self):
        self.optimizer_gen = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE, beta1=0.5)
        self.optimizer_dis = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE, beta1=0.5) 

    def set_optimize_(self):
        self.init_optimize()
        #tvars = tf.trainable_variables()
        self.dis_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.dis_namespace)
        self.gen_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.gen_namespace)

    def train(self):
        self.sess.run(self.init)
        print(self.sess)
        loop = 1
        ex = 1
        while True:
            try:
                gen_input = tf.get_default_graph().get_tensor_by_name('input/gen_input:0') 
                disc_input = tf.get_default_graph().get_tensor_by_name('input/dis_input:0')
                noise = self._get_noise('uni')
                batch_x, _ ,_ = self.dataset.next_batch(BATCH_SIZE)
                feed_dict = {gen_input: noise, 
                             disc_input: batch_x}
                summary, _, d_loss = self.sess.run([self.summary,
                                                    self.train_dis,
                                                    self.dis_loss], feed_dict=feed_dict)
                ex += 1
                '''
                g_loss = self.sess.run([self.train_gen,
                                   self.gen_loss], feed_dict=feed_dict)
                '''
                summary, _, g_loss = self.sess.run([self.summary,
                                                    self.train_gen,
                                                    self.gen_loss], feed_dict=feed_dict)
                self.writer.add_summary(summary, ex)
                if loop % 20 == 0 or loop == 0:
                    print('=========== {}'.format(loop))
                    print('{}:{}'.format('disc_loss', d_loss))
                    print('{}:{}'.format('gen_loss', g_loss))
                    self.saver.save(self.sess, os.path.join(self.filepath, 'model'))
                if loop% 10000 ==0:
                    self.sample(loop, sample_size=50)
                loop += 1
                    
            except tf.errors.OutOfRangeError:
                break
            except Exception as e:
                print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                print(e)
                print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                traceback.print_exc()
                break

    def add_batch_norm_ops_after_optimize(self):
        gen_ops = [self.train_gen] + self.gen_train_ops
        self.train_gen = tf.group(*gen_ops)
        dis_ops = [self.train_dis] + self.dis_train_ops
        self.train_dis = tf.group(*dis_ops)


class FULLGANvXEtpy(FULLGANv2):

    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):
        super(FULLGANvXEtpy, self).__init__(dataset, 
                                      dis_k_size, 
                                      dis_k_stride,
                                      dis_p_size,
                                      dis_p_stride,
                                      dis_namespace,
                                      dis_out_activate,
                                      dis_layers,
                                      gen_k_size,
                                      gen_k_stride,
                                      gen_namespace,
                                      gen_out_activate,
                                      gen_layers,
                                      image_size,
                                      image_channel,
                                      filepath,
                                      dis_use_layers=dis_use_layers,
                                      gen_use_layers=gen_use_layers)

    def get_loss(self):
        mean_log_dis_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_out_real, labels = tf.ones_like(self.dis_out_real)))
        mean_log_dis_gen = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_out_gen, labels = tf.zeros_like(self.dis_out_gen)))
        tf.summary.scalar('mean_log_dis_gen', mean_log_dis_gen)
        tf.summary.scalar('mean_log_dis_real', mean_log_dis_real) 
        self.dis_loss = mean_log_dis_real + mean_log_dis_gen
        mean_log_dis_gen_plus = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_out_gen, labels = tf.ones_like(self.dis_out_gen)))

        self.gen_loss = mean_log_dis_gen_plus
        tf.summary.scalar('dis loss', self.dis_loss)
        tf.summary.scalar('gen loss', self.gen_loss) 


