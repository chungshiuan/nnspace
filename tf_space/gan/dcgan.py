import sys, os, time, traceback
import tensorflow as tf
import numpy as np
from functools import reduce
from .gan_base_net import GANUBTrainNet, GANNet
from .gan_util import gan_net, gan_net_cross_entropy, generator_dc, discriminator, lrelu
from utils import check_path
import math
from gan.gan_util import batch_norm 
from .fullgan import FULLGANvXEtpy, FULLGANv2 

NOISE_DIM = 100
LEARNING_RATE = 0.0002
MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'mnist')
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))

class DCGAN(GANUBTrainNet):
    
    def __init__(self, dataset, disc_image_shape):
        super(DCGAN, self).__init__(dataset)
        self.disc_image_shape = disc_image_shape
        self.disc_image_dim = reduce((lambda x, y: x*y),disc_image_shape)
        self.net()
        self._post_jobs()

    def net(self):
        gen_input_place = tf.placeholder(tf.float32, shape=[None, NOISE_DIM], name='gen_input')
        disc_input = tf.placeholder(tf.float32, shape=[None, *self.disc_image_shape], name='disc_input')
        gen_input = gen_input_place[0:tf.shape(disc_input)[0],:]
        train_gen, train_disc, gen_loss, disc_loss, gen_fake = gan_net(gen_input, disc_input, *self.disc_image_shape[0:3])
        tf.summary.image('gen_image',tf.reshape( gen_fake, [-1, *self.disc_image_shape]))
        tf.summary.scalar('gen loss',gen_loss)
        tf.summary.scalar('total disc loss',disc_loss)

        self.run_train_tensors = {'_train_gen': train_gen, 
                                  '_train_disc': train_disc, 
                                  'gen_loss': gen_loss, 
                                  'disc_loss': disc_loss}
        self.run_predict_tensors = {'gen_output': gen_fake}

class DCGANXEtpy_2Loss(DCGAN):
    def __init__(self, dataset, disc_image_shape):
        super(DCGANXEtpy_2Loss, self).__init__(dataset, disc_image_shape)

    def gan_net_cross_entropy(self, gen_in, real_in, img_w, img_h, img_c=1, gen_func=generator_dc, disc_func=discriminator):
        gen_output = gen_func(gen_in, img_w, img_h, img_c)
        disc_real = disc_func(real_in)
        disc_real = tf.maximum(disc_real, 1e-9)
        disc_gen = disc_func(gen_output, reuse=True)
        disc_r_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_real, labels = tf.ones_like(disc_real)))
        disc_g_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_gen, labels = tf.zeros_like(disc_gen)))
        disc_loss = disc_r_loss + disc_g_loss
        # Loss function for generator
        gen_loss = -tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_gen, labels=tf.zeros_like(disc_gen)))

        optimizer_gen = tf.train.AdamOptimizer(learning_rate=0.0001)
        optimizer_disc = tf.train.AdamOptimizer(learning_rate=0.0003)

        tvars = tf.trainable_variables()
        disc_vars = [var for var in tvars if 'd-' in var.name]
        print('disc vars: {}'.format(disc_vars))
        gen_vars = [var for var in tvars if 'g-' in var.name]
        print('gen vars: {}'.format(gen_vars))
        train_gen = optimizer_gen.minimize(gen_loss, var_list=gen_vars)
        train_disc = optimizer_disc.minimize(disc_loss, var_list=disc_vars)
        return train_gen, train_disc, gen_loss, disc_loss, gen_output 

    def net(self):
        gen_input_place = tf.placeholder(tf.float32, shape=[None, NOISE_DIM], name='gen_input')
        disc_input = tf.placeholder(tf.float32, shape=[None, *self.disc_image_shape], name='disc_input')
        gen_input = gen_input_place[0:tf.shape(disc_input)[0],:]
        train_gen, train_disc, gen_loss, disc_loss, gen_fake = self.gan_net_cross_entropy(gen_input, disc_input, *self.disc_image_shape[0:3])
        tf.summary.image('gen_image',tf.reshape( gen_fake, [-1, *self.disc_image_shape]))
        tf.summary.scalar('gen loss',gen_loss)
        tf.summary.scalar('total disc loss',disc_loss)

        self.run_train_tensors = {'_train_gen': train_gen, 
                                  '_train_disc': train_disc, 
                                  'gen_loss': gen_loss, 
                                  'disc_loss': disc_loss}
        self.run_predict_tensors = {'gen_output': gen_fake}
 

class DCGANXEtpy(DCGAN):
 
    def __init__(self, dataset, disc_image_shape):
        super(DCGANXEtpy, self).__init__(dataset, disc_image_shape)
   
    def net(self):
        gen_input_place = tf.placeholder(tf.float32, shape=[None, NOISE_DIM], name='gen_input')
        disc_input = tf.placeholder(tf.float32, shape=[None, *self.disc_image_shape], name='disc_input')
        gen_input = gen_input_place[0:tf.shape(disc_input)[0],:]
        train_gen, train_disc_g, train_disc_r, gen_loss, disc_g_loss, disc_r_loss, gen_fake = gan_net_cross_entropy(gen_input, disc_input, *self.disc_image_shape[0:3])
        tf.summary.image('gen_image',tf.reshape( gen_fake, [-1, *self.disc_image_shape]))
        tf.summary.scalar('gen loss',gen_loss)
        tf.summary.scalar('total disc g loss',disc_g_loss)
        tf.summary.scalar('total disc r loss',disc_r_loss)

        self.run_train_tensors = {'_train_gen': train_gen, 
                                  '_train_disc_r': train_disc_r,
                                  '_train_disc_g': train_disc_g, 
                                  'gen_loss': gen_loss, 
                                  'disc_r_loss': disc_r_loss,
                                  'disc_g_loss': disc_g_loss}
        self.run_predict_tensors = {'gen_output': gen_fake}


    def train(self):
        with tf.Session() as sess:
            sess.run(self.init)
            loop = 0
            ex = 0
            while True:
                try:
                    for i in range(4):
                        noise = self._get_noise('nor')
                        batch_x, _ ,_ = self.dataset.next_batch(BATCH_SIZE)
                        gen_input = tf.get_default_graph().get_tensor_by_name('gen_input:0') 
                        disc_input = tf.get_default_graph().get_tensor_by_name('disc_input:0') 
                        feed_dict = {gen_input: noise, 
                                     disc_input: batch_x}
                        summary, _, _, d_loss_r, d_loss_g = sess.run([self.run_train_tensors['_summary_op'],
                                              self.run_train_tensors['_train_disc_r'],
                                              self.run_train_tensors['_train_disc_g'],
                                              self.run_train_tensors['disc_r_loss'],
                                              self.run_train_tensors['disc_g_loss']], feed_dict=feed_dict)
                        self.writer.add_summary(summary,ex)
                        ex += 1
                    noise = self._get_noise('nor')
                    batch_x, _, _ = self.dataset.next_batch(BATCH_SIZE)
                    gen_input = tf.get_default_graph().get_tensor_by_name('gen_input:0') 
                    disc_input = tf.get_default_graph().get_tensor_by_name('disc_input:0') 
                    feed_dict = {gen_input: noise, 
                                 disc_input: batch_x}
                    summary, _, g_loss = sess.run([self.run_train_tensors['_summary_op'],
                                                self.run_train_tensors['_train_gen'],
                                                self.run_train_tensors['gen_loss']], feed_dict=feed_dict)
                    self.writer.add_summary(summary, ex)
                    if loop % 20 == 0 or loop == 0:
                        print('=========== {}'.format(loop))
                        print('{}:{}'.format('disc_loss_r', d_loss_r))
                        print('{}:{}'.format('disc_loss_g', d_loss_g))
                        print('{}:{}'.format('gen_loss', g_loss))
                        self.saver.save(sess, MODEL_FOLDER)
                    loop += 1
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    print(e)
                    print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                    traceback.print_exc()
                    break

class DCGANv2(FULLGANv2):
 
    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter=1024,
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):
        self.init_gen_w = math.floor(image_size[0]/(gen_k_stride[0]**gen_layers))
        self.init_gen_h = math.floor(image_size[1]/(gen_k_stride[1]**gen_layers))
        self.init_num_filters = gen_init_num_filter
        self.init_gen_shape = [BATCH_SIZE, self.init_gen_w * self.init_gen_h * self.init_num_filters]
        super(DCGANv2, self).__init__(dataset, 
                                      dis_k_size, 
                                      dis_k_stride,
                                      dis_p_size,
                                      dis_p_stride,
                                      dis_namespace,
                                      dis_out_activate,
                                      dis_layers,
                                      gen_k_size,
                                      gen_k_stride,
                                      gen_namespace,
                                      gen_out_activate,
                                      gen_layers,
                                      image_size,
                                      image_channel,
                                      filepath,
                                      dis_use_layers=dis_use_layers,
                                      gen_use_layers=gen_use_layers)

    def generator(self, in_data):
        init_w = self.init_gen_w
        init_h = self.init_gen_h
        padding = 'SAME'
        num_filters = self.init_num_filters
        de = in_data
        with tf.variable_scope(self.gen_namespace, reuse=False) as scope:
            de = tf.layers.flatten(de)
            de = tf.layers.dense(de,
                                 units=init_w * init_h * self.init_num_filters, 
                                 activation=None,
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                 bias_initializer=tf.constant_initializer(0.0), 
                                 name = 'pre-dense')
            de = tf.reshape(de, [-1,  init_w, init_h, self.init_num_filters])
            de = batch_norm(de, name='pre-dense', _ops=self.gen_train_ops)
            de = tf.nn.relu(de, name='pre-dense-relu')
            for index in range(1, self.gen_layers+1):
                num_filters = int(num_filters/2)
                activation = tf.nn.relu
                if index == self.gen_layers:
                    num_filters = self.image_channel
                    activation = self.get_activate(self.gen_out_activate)
                de = tf.layers.conv2d_transpose(de, 
                                                num_filters, 
                                                kernel_size=self.gen_k_size, 
                                                name='{}'.format(index), 
                                                padding=padding, 
                                                activation = None,
                                                kernel_initializer=tf.random_normal_initializer(stddev=0.02),
                                                bias_initializer=tf.constant_initializer(0.0), 
                                                strides=self.gen_k_stride)
                if not index == self.gen_layers: 
                    de = batch_norm(de, name='{}'.format(index), _ops=self.gen_train_ops)
                de = activation(de)
                print(de)
        return de
 
    def discriminator(self, in_data, reuse=False):
        
        padding = 'SAME'
        num_filters = self.init_num_filters/2/(2**self.dis_layers)
        dc = in_data
        with tf.variable_scope(self.dis_namespace, reuse=reuse) as scope:
            for index in range(1, self.dis_layers+1):
                activation = lrelu
                dc = tf.layers.conv2d(dc, 
                                      filters=num_filters, 
                                      kernel_size=self.dis_k_size, 
                                      name='{}'.format(index), 
                                      padding=padding,
                                      activation = None,
                                      kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                      bias_initializer=tf.constant_initializer(0.0), 
                                      strides=self.dis_k_stride) 
                if not index == 1: 
                    dc = batch_norm(dc, name='{}'.format(index), _ops=self.dis_train_ops)
                dc = activation(dc)
                num_filters = num_filters*2
                print(dc)
            print(self.dis_k_size)
            print(self.dis_k_stride)
            print(self.get_activate(self.dis_out_activate))
            dc = tf.layers.flatten(dc)
            dc = tf.layers.dense(dc,
                                 units=1, 
                                 activation=self.get_activate(self.dis_out_activate),
                                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                 bias_initializer=tf.constant_initializer(0.0), 
                                 name = '2-dense')
        return dc

    def add_batch_norm_ops_after_optimize(self):
        dis_ops = [self.train_dis] + self.dis_train_ops
        self.train_dis = tf.group(*dis_ops)
        gen_ops = [self.train_gen] + self.gen_train_ops
        self.train_gen = tf.group(*gen_ops)

    def train(self):
        self.sess.run(self.init)
        loop = 1
        ex = 1
        while True:
            try:
                gen_input = tf.get_default_graph().get_tensor_by_name('input/gen_input:0')
                disc_input = tf.get_default_graph().get_tensor_by_name('input/dis_input:0')
                noise = self._get_noise('uni')
                batch_x, _ ,_ = self.dataset.next_batch(BATCH_SIZE)
                feed_dict = {gen_input: noise,
                             disc_input: batch_x}
                _, d_loss = self.sess.run([self.train_dis,
                                           self.dis_loss], feed_dict=feed_dict)
                ex += 1
                g_loss = self.sess.run([self.train_gen,
                                        self.gen_loss], feed_dict=feed_dict)
                _, g_loss, summary = self.sess.run([self.train_gen,
                                                    self.gen_loss,
                                                    self.summary], feed_dict=feed_dict)
                self.writer.add_summary(summary, ex)
                if loop % 20 == 0 or loop == 0:
                    print('=========== {}'.format(loop))
                    print('{}:{}'.format('disc_loss', d_loss))
                    print('{}:{}'.format('gen_loss', g_loss))
                    self.saver.save(self.sess, MODEL_FOLDER)
                if loop% 1000 ==0:
                    self.sample(loop, sample_size=50)
                loop += 1
            except tf.errors.OutOfRangeError:
                break
            except Exception as e:
                print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                print(e)
                print('EEEEEEEEEEEEEEEEEEEEEEEEEE')
                traceback.print_exc()
                break

class DCGANv2XEtpy(DCGANv2):

    def __init__(self, 
                 dataset, 
                 dis_k_size, 
                 dis_k_stride,
                 dis_p_size,
                 dis_p_stride,
                 dis_namespace,
                 dis_out_activate,
                 dis_layers,
                 gen_k_size,
                 gen_k_stride,
                 gen_namespace,
                 gen_out_activate,
                 gen_layers,
                 image_size,
                 image_channel,
                 gen_init_num_filter=128,
                 filepath=None,
                 dis_use_layers=True,
                 gen_use_layers=True):
        super(DCGANv2XEtpy, self).__init__(dataset, 
                                      dis_k_size, 
                                      dis_k_stride,
                                      dis_p_size,
                                      dis_p_stride,
                                      dis_namespace,
                                      dis_out_activate,
                                      dis_layers,
                                      gen_k_size,
                                      gen_k_stride,
                                      gen_namespace,
                                      gen_out_activate,
                                      gen_layers,
                                      image_size,
                                      image_channel,
                                      gen_init_num_filter,
                                      filepath,
                                      dis_use_layers=dis_use_layers,
                                      gen_use_layers=gen_use_layers)


    def get_loss(self):
        mean_log_dis_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_out_real, labels = tf.ones_like(self.dis_out_real)))
        mean_log_dis_gen = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_out_gen, labels = tf.zeros_like(self.dis_out_gen)))
        tf.summary.scalar('mean_log_dis_gen', mean_log_dis_gen)
        tf.summary.scalar('mean_log_dis_real', mean_log_dis_real) 
        self.dis_loss = mean_log_dis_real + mean_log_dis_gen
        mean_log_dis_gen_plus = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.dis_out_gen, labels = tf.ones_like(self.dis_out_gen)))

        self.gen_loss = mean_log_dis_gen_plus
        tf.summary.scalar('dis loss', self.dis_loss)
        tf.summary.scalar('gen loss', self.gen_loss) 


