import os, shutil        
from config import initEnv
initEnv()
import tensorflow as tf

class DataSet(object):
    def __init__(self, folder, batch_size=int(os.environ.get('BATCH_SIZE')), resize=[], image_norm_type=None):
        self.batch_size = batch_size
        self.root = os.path.join(os.environ.get('DATASET_ROOT'), folder)
        self.records = os.path.join(self.root, 'Records')
        self.records_train = os.path.join(self.records, 'training')
        self.records_test = os.path.join(self.records, 'testing')
        self.resize = resize
        self.image_norm_type = image_norm_type 

    def get_image_norm(self, image):
        return {
            '0to1': norm_tf_zero_one,
            '-1to1': norm_tf_neg_pos,
            '0mean': norm_tf_zero_mean,
            'np0to1': norm_zero_one,
            'np-1to1': norm_neg_pos,
            'np0mean': norm_zero_mean

        }.get(self.image_norm_type, lambda m: m)(image)

    def _check_records(self):
        if os.path.exists(self.records):
            shutil.rmtree(self.records, ignore_errors=True)
        os.mkdir(self.records)
        return True

    def _check_root(self):
        if os.path.exists(self.root):
            return True
        os.mkdir(self.root)
        return True

    def parser(self, record):
        print('method _parser is not implemented.')
        pass

    def get_records(self, epoch=2):
        once_files = tf.train.match_filenames_once(os.path.join(self.records, '*.tfrecords'))
        dataset = tf.data.TFRecordDataset(once_files)
        dataset = dataset.map(self.parser)
        dataset = dataset.shuffle(buffer_size=30000)
        dataset = dataset.batch(self.batch_size)
        dataset = dataset.repeat(epoch)
        #iterator = dataset.make_one_shot_iterator()
        iterator = dataset.make_initializable_iterator()
        return iterator

def _parser(record):
    image_string = tf.read_file(record[0])
    image_decoded = tf.image.decode_jpeg(image_string)
    return image_decoded, record[0], code[0]

class FileData(DataSet):
    def __init__(self, folder, files=[], labels=[], codes=[], batch_size=int(os.environ.get('BATCH_SIZE')), resize=[], image_norm_type=None):
        super(FileData, self).__init__(folder=folder, batch_size=batch_size, resize=resize, image_norm_type=image_norm_type)
        if not len(files) == len(labels):
            print('[WARNING] length of files and labels are not equal')
        self.files = files
        self.labels = labels
        self.codes = codes

    def fetch(self):
        print('method fetch is not implemented.')
        pass

    def get_records(self, epoch=2):
        files = tf.constant(self.files, name='file')
        labels = tf.constant(self.labels, name='label')
        codes = tf.constant(self.codes, name='code')        
        dataset = tf.data.Dataset.from_tensor_slices((files, labels, codes))
        dataset = dataset.map(self.parser)
        dataset = dataset.shuffle(buffer_size=3800)
        dataset = dataset.batch(self.batch_size)
        dataset = dataset.repeat(epoch)
        #iterator = dataset.make_one_shot_iterator()
        iterator = dataset.make_initializable_iterator()
        return iterator

def norm_tf_zero_one(x):
    x = tf.cast(x, tf.float32)
    min = tf.reduce_min(x)
    max = tf.reduce_max(x)
    return (x-min)/(max-min)

def norm_tf_neg_pos(x):
    x = tf.cast(x, tf.float32)
    min = tf.reduce_min(x)
    max = tf.reduce_max(x)
    return 2*(x-min)/(max-min) - 1.

def norm_tf_zero_mean(x):
    return x # not implement

def norm_zero_one(x):
    x = x.astype(np.float32)
    min = np.min(x)
    max = np.max(x)
    return (x-min)/(max-min)

def norm_neg_pos(x):
    x = x.astype(np.float32)
    min = np.min(x)
    max = np.max(x)
    return 2*(x-min)/(max-min) - 1.

def norm_zero_mean(x):
    return x # not implement
