import __init__
from tf_space.rl.utils import get_summary 
import numpy as np

def rollout(env, agent=None, clear=False, render=False, MAXSTEP=5000):
    obs, rew, done = env.reset(), 0, False
    obses, actses, rewses = [], [], []
    step = 0
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            prob = agent.get_acts([obs])
#            act_space = np.arange(env.action_space.n)
#            act = np.random.choice(act_space, p=prob)
            obs_, rew, done, info = env.step(prob)
#            actses.append(act)
        obses.append(obs)
#        actses.append(act)
        rewses.append(rew)
        obs = obs_
        get_summary(env, False, rew, render, clear )
        step+=1
        if step > MAXSTEP:
           print('max step:{} is reached'.format(MAXSTEP))
           break
    print('------------------- len rews: {}'.format(sum(rewses)))

    return obses, actses, rewses

