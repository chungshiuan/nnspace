'''
1. maybe loss function should change to Cross entropy from square
2. single step update? batch step update? or batch sample update?
3. need discount reward?
'''

import __init__
import numpy as np
import pickle, os, time
from tf_space.utils import check_path
import tensorflow as tf
import argparse, gym
import numpy as np
from cp_common import rollout

LEARNING_RATE = 0.001
GAMMA = 0.79


def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np

class Agent(object):
    def __init__(self, env, model_path='model', cont=True, ep=0):
        self.model_path = model_path
        self.env = env
        self.obses = None
        self.obses2 = None
        self.acts = None
        self.rews = None
        self.out_prob = None
        self.n_out = None
        self.optimize = None
        self.sess = None
        self.saver = None
        self.loss = None
        self.ep = ep
        print('ep ======',ep)
        if cont:
           self.load()

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.n_out = tf.get_default_graph().get_tensor_by_name('ndense2/BiasAdd:0')
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def net(self):
        self.obses = tf.placeholder(tf.float32, shape=[None, 4], name='obses')
        self.target = tf.placeholder(tf.float32, shape=[None,self.env.action_space.n], name='targ')
        self.n_out = tf.layers.dense(self.obses, 
                                     kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.9),
                                     bias_initializer=tf.constant_initializer(0.1), 
                                     units=24, 
#                                     units=env.action_space.n,
                                     activation=tf.nn.relu, 
                                     name='ndense1')          
        self.n_out = tf.layers.dense(self.n_out, 
                                     kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.9),
                                     bias_initializer=tf.constant_initializer(0.1), 
                                     units=self.env.action_space.n, 
                                     activation=tf.nn.relu,
                                     name='ndense2')        
#        diff = tf.nn.softmax_cross_entropy_with_logits(labels= self.target, logits=self.n_out)
#        self.loss = tf.reduce_mean(diff)
#        self.prob = tf.argmax(self.n_out, 1)
        self.loss = tf.reduce_mean(tf.square(self.target - self.n_out))

        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)

    def get_acts(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np.array(obses)}
        if np.random.uniform(0,1) < self.ep:
            action = self.env.action_space.sample()
        else:
#            action = self.sess.run(tf.argmax(self.n_out, 1), feed_dict = feed_dict)[0]
            action = np.argmin(self.sess.run(self.n_out, feed_dict = feed_dict)[0])

        return action

    def get_q(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np.array(obses) }
        q = self.sess.run(self.n_out, feed_dict = feed_dict)
        return q

    def train(self, obses, target): 
        feed_dict = {
            self.obses : np.array(obses),
            self.target: np.array(target)
        }
        opt, loss, out = self.sess.run([self.optimize, self.loss, self.n_out], feed_dict=feed_dict)
#        print('out:{}'.format(out))
        print('loss:{}'.format(loss))



class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent
        self.agent.init_session()

    def play(self):
        self.agent.load()
        print('start to play')
        for i in range(100):
            rollout(self.env, None, True, True)
        self.env.close()

    def train(self, epoch=3000):
        for i in range(epoch):
            s = self.env.reset()
            done = False
            rews = []
            ss = []
            qq = []
            aa = []
            while not done:      
                a = self.agent.get_acts([s])
#                print('a:{}'.format(a))
                q = self.agent.get_q([s])[0]
#                print('q:{}'.format(q))
                s_, rew, done, info = self.env.step(a)
                self.env.render()
                if done:
                    rew = -rew
                a_ = self.agent.get_acts([s_])
#                print('a_:{}'.format(a_))
                q_ = self.agent.get_q([s_])[0]
#                print('q_:{}'.format(q_))

                q[a] = rew + GAMMA*np.max(q_)
                rews.append(rew)
                ss.append(s)
                qq.append(q)
                aa.append(a)
                s  = s_
#                self.agent.train([s], [q])

            print('------{}------'.format(i))

#            print(ss)
            self.agent.train(ss, qq)
            print('aa:{}'.format(aa))
            print('len rews: {}'.format(sum(rews)))
            if i % 100 == 0:
                print('------->',i)
                self.agent.save()
        self.env.close() 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')
 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    print(isContinue)
    ep = 0
    if args.td == 'train':
        ep = 0.0
    else:
        ep = 0.0
    env = gym.make('CartPole-v1')
    agent = Agent(env, 'cartpole/qnnv1', isContinue, ep)
    player = Player(env, agent)
    getattr(player, args.td)()
    env.close()
