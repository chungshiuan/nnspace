import __init__
import numpy as np
import pickle, os
from tf_space.utils import check_path
import os

EPSILON=0.1
GAMMA = 0.96
LEARN_RATE = 0.81
rew_time=20
class Agent(object):
    def __init__(self, env, model_path='model', cont=True, EPSILON=0.1):
        self.model_path = model_path
        self.Q = np.zeros((env.observation_space.n, env.action_space.n))
        self.G = np.zeros((env.observation_space.n, env.action_space.n))
        self.N = np.ones((env.observation_space.n, env.action_space.n))

        if cont:
           self.load()
        self.env = env
        self.EPSILON=EPSILON

    def save(self):
        folder = os.path.join(*self.model_path.split('/')[0: -1])
        check_path(folder)
        with open(self.model_path, 'wb') as f:
            pickle.dump(self.Q, f)
 
    def load(self):
        with open(self.model_path, 'rb') as f:
            self.Q = pickle.load(f)

    def get_acts(self, obses):
        action = None
        if np.random.uniform(0,1) < self.EPSILON:
            action = self.env.action_space.sample()
        else:
            action = np.argmax(self.Q[obses, :])
        return action

    def train(self, state_action_reward):
        for sar in state_action_reward:
            self.G[sar[0], sar[1]] += sar[2]
            self.N[sar[0], sar[1]] += 1
        self.Q = self.G / self.N
          
          
