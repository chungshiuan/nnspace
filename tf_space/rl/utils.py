import __init__
import numpy as np
import time, os, pickle
import random as pr
from tf_space.utils import check_path
hist = {'G':0, 'H':0}

def rargmax(vector):
    """ Argmax that chooses randomly among eligible maximum indices. """
    m = np.amax(vector)
    indices = np.nonzero(vector == m)[0]
    return pr.choice(indices)

def run_episode(env, policy, gamma = 1.0, render = False, run_slower=False):
    """ Runs an episode and return the total reward """
    obs = env.reset()
    total_reward = 0
    step_idx = 0
    while True:
        if render:
            env.render()
        obs, reward, done , _ = env.step(int(policy[obs]))
        total_reward += (gamma ** step_idx * reward)
        step_idx += 1
        if done:
            env.render()
            if reward <= 0:
               hist['H'] += 1
               print('T_______T')
            else:
               hist['G'] += 1
               print('^_______<')
            break
        if run_slower:
            time.sleep(1)
    print('success:{} , fail: {}'.format(hist['G'], hist['H']))
#        os.system('clear')
    return total_reward

def get_summary(env, done, rew, render=True, clear=True, sleep=0):
    if render:
        env.render()
    time.sleep(sleep)
    if clear:
        os.system('clear')
    if done:
        if rew <= 0:
            hist['H'] += 1
            print('T____________T')
        else:
            hist['G'] += 1
            print('>____________^')
    return hist       
 
def evaluate_policy(env, policy, gamma = 1.0,  n = 100, render= True):
    """ Evaluates a policy by running it n times.
    returns:
    average total reward
    """
    scores = [
            run_episode(env, policy, gamma = gamma, render = render)
            for _ in range(n)]
    return np.mean(scores)


def save(Q, path):
    folder = os.path.join(*path.split('/')[0: -1])
    check_path(folder)
    with open(path, 'wb') as f:
        pickle.dump(Q, f)
 
def load(path):
    with open(path, 'rb') as f:
        return pickle.load(f)

