import gym
import numpy as np

def rollout(env, agent=None):
    obs, rew, done = env.reset(), 0 , False
    obses, acts, rews = [], [], []
    idx = 0
    while not done:
        env.render()
        if agent is None:
            obs, rew, done, info = env.step(env.action_space.sample())
        else:
            
            prob = np.squeeze(agent.get_acts(obs), axis=0)
            print(prob)
            act_space = np.arange(env.action_space.n)
            act = np.random.choice(act_space, p=prob)
            obs, rew, done, info = env.step(act)
            if done:
               print('T___________________T')
            acts.append(act)
        obses.append(obs)
        rews.append(rew)
        print('{}--->'.format(idx))
        idx += 1
    return obses, acts, rews

class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load()
        for i in range(100):
            rollout(self.env, self.agent)
        self.env.close()

    def train(self, epoch=100):
        self.agent.init_session()
        for i in range(epoch):
            obses, acts, rews = rollout(self.env, self.agent)
            self.agent.train(obses, acts, rews)
            if i % 50 == 0:
                self.agent.save()
        self.agent.close_session()
          
