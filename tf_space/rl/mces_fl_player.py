import argparse, gym
from mces_fl import Agent
import time, os
class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load()
        for episode in range(5):
            state = self.env.reset()
            done = False
            print('---------',episode) 
            while not done:       
                action = self.agent.get_acts(state)
                next_state, reward, done, info = self.env.step(action)
                state = next_state
                os.system('clear')
                self.env.render()
                time.sleep(0.5)
        self.env.close()

    def train(self, epoch=10000):
        for i in range(epoch):
            state = self.env.reset()
            done = False
            state_action_reward = []
            while not done:      
                action = self.agent.get_acts(state)
                next_state, reward, done, info = self.env.step(action)
                #print(next_state)
                print('------------',i)
                self.env.render()
                if done:
                   if reward==0:
                      reward =-10
                   else:
                      reward = 20
                else:
                   reward = 3
                state_action_reward.append([next_state, action, reward])    
                if done: 
                    self.agent.train(state_action_reward)
                time.sleep(0.08)
                os.system('clear')
                state = next_state
            if i % 100 == 0:
                print('------->',i)
                self.agent.save()
        self.env.close() 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')

 
    args = parser.parse_args()
    print(args.td)
    print(args.co)

    isContinue = (args.co in ['t','true','True', '1'])
    print(isContinue)
    ep=0
    if args.td == 'train':
        ep = 0.4
    env = gym.make('FrozenLake-v0')
    agent = Agent(env, 'fronzelake/mcfl.pkl', isContinue, ep)
    player = Player(env, agent)
    getattr(player, args.td)()
    env.close()
