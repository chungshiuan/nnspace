import argparse, gym
from player import Player
import tensorflow as tf
LEARNING_RATE=1e-3

class Agent(object):
    def __init__(self, obses_shape=[], act_shape=4, model_path='model'):
        self.obses_shape = obses_shape
        self.act_shape = act_shape
        self.obses = None
        self.acts = None
        self.rews = None
        self.out_prob = []
        self.optimize = None
        self.sess = None
        self.saver = None
        self.model_path = model_path

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.out_prob = tf.get_default_graph().get_tensor_by_name('out_prob:0')
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def net(self):
        self.obses = tf.placeholder(tf.float32, shape=[None, *self.obses_shape],name='obses')
        self.acts = tf.placeholder(tf.int32, name='acts')
        self.rews = tf.placeholder(tf.float32, name='rews')
        n_out = tf.layers.dense(self.obses, units=36, activation=tf.nn.relu, name='dense1')         
        n_out = tf.layers.dense(n_out, units=self.act_shape, name='dense2')         
        self.out_prob = tf.nn.softmax(n_out, name='out_prob')
        n_out = tf.reduce_sum(-tf.log(self.out_prob)*tf.one_hot(self.acts, self.act_shape), axis=1) 
        loss = tf.reduce_mean(n_out * self.rews)
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(loss)

    def get_acts(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: obses.reshape(1,-1)}
        return self.sess.run(self.out_prob, feed_dict = feed_dict)

    def train(self, obses, acts, rews):
        print('>>>>>',len(acts))
        feed_dict = {
            self.obses : obses,
            self.acts: acts,
            self.rews: rews
        }
        self.sess.run(self.optimize, feed_dict=feed_dict)
       
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    args = parser.parse_args()
    print(args.td)

    env = gym.make('CartPole-v0')
    agent = Agent(env.observation_space.shape, env.action_space.n, 'cp01/pg')
    player = Player(env, agent)
    getattr(player, args.td)()
    env.close()
