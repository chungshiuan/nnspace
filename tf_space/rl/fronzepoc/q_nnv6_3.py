'''
remove softmax at output
add reduce_sum
'''
import __init__
import numpy as np
import pickle, os, time
from tf_space.utils import check_path
import tensorflow as tf
import argparse, gym
import numpy as np
from cp_common import rollout
import random
from gym import wrappers
from gym import register
from fl88env import FLEnv

LEARNING_RATE = 0.001
GAMMA = 0.9
INITIAL_EPSILON = 0.5 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon


def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np

class Agent(object):
    def __init__(self, env, model_path='model', cont=True, ep=INITIAL_EPSILON, namespace='ql'):
        self.model_path = model_path
        self.env = env
        self.obses = None
        self.acts = None
        self.rews = None
        self.out_prob = None
        self.n_out = None
        self.optimize = None
        self.sess = None
        self.saver = None
        self.loss = None
        self.diffQ = None
        self.ep = ep
        self.namespace = namespace
        print('ep ======',ep)
        if cont:
           self.load()

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        print('all trainable:', tf.trainable_variables())
#        print('all tensor:',  tf.get_default_graph().get_operations())

        self.saver.restore(self.sess, self.model_path)
        self.n_out = tf.get_default_graph().get_tensor_by_name('{}/ndense2/BiasAdd:0'.format(self.namespace))
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def setPlaceholder(self):
        self.obses = tf.placeholder(tf.float32, shape=[None, 16], name='obses')
        self.action = tf.placeholder(tf.int32, shape=[None, ], name='action')
        self.reward = tf.placeholder(tf.float32, shape=[None, ], name='reward')
        self.target = tf.placeholder(tf.float32, shape=[None,], name='targ')


    def net(self):
        self.setPlaceholder()
        with tf.variable_scope(self.namespace):
            self.n_out = tf.layers.dense(self.obses, 
#                                    kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                     kernel_initializer=tf.truncated_normal_initializer,
                                    
                                     bias_initializer=tf.constant_initializer(0.01), 
                                     units=20, 
                                     activation=tf.nn.relu, 
                                     name='ndense1')         
            self.n_out = tf.layers.dense(self.n_out, 
#                                     kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                     kernel_initializer=tf.truncated_normal_initializer,

                                     bias_initializer=tf.constant_initializer(0.01), 
                                     units=20, 
                                     name='ndense3')       

            self.n_out = tf.layers.dense(self.n_out, 
#                                     kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                     kernel_initializer=tf.truncated_normal_initializer,

                                     bias_initializer=tf.constant_initializer(0.01), 
                                     units=self.env.action_space.n, 
                                     name='ndense2')       
        
        with tf.name_scope("loss"):
            Q_action = tf.reduce_sum(tf.multiply(self.n_out, tf.one_hot(self.action,depth=self.env.action_space.n, dtype=tf.float32)), reduction_indices = 1)
            self.diffQ = self.target - Q_action
            self.loss = tf.reduce_mean(tf.square(self.diffQ))
           
        with tf.name_scope("train"):
            self.optimize = tf.train.AdamOptimizer(LEARNING_RATE).minimize(self.loss)

    def get_greedy_action(self, obses):
        if np.random.uniform(0,1) <= self.ep:
            action = self.env.action_space.sample()
        else:
            action = self.get_acts(obses)
        self.ep -= (INITIAL_EPSILON - FINAL_EPSILON)/10000 
        return action

    def get_acts(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np_one_hot(obses, 16) }
        action = np.argmax(self.sess.run(self.n_out, feed_dict = feed_dict)[0])
        return action

    def get_q(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np_one_hot(obses, 16) }
        q = self.sess.run(self.n_out, feed_dict = feed_dict)
        return q

    def train(self, obses, actions, obses_, rewards, dones): 
        feed_dict = {
            self.obses : np_one_hot(obses, 16),
            self.target: np.array(self.get_target(obses_, rewards, dones)),
            self.reward: np.array(rewards),
            self.action: np.array(actions)
        }
        opt, loss, out = self.sess.run([self.optimize, self.loss, self.n_out], feed_dict=feed_dict)

    def get_target(self, obses_, rews,dones):
        target = []
        q_ = self.get_q(obses_)

        for i, d in enumerate(dones):
            if d:
                target.append(rews[i])           
            else:
                target.append(rews[i]+GAMMA*np.max(q_[i]))
        return target


class PureAgent(Agent):
    def __init__(self, *argv, **kargv):
        super(PureAgent, self).__init__(*argv,**kargv)

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.n_out = tf.get_default_graph().get_tensor_by_name('ndense2:0')
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def net(self):
        self.setPlaceholder()
        W1 = self.weight_variable([16,64])
        b1 = self.bias_variable([64])
        h_layer = tf.nn.relu(tf.matmul(self.obses,W1) + b1)
        '''
        W3 = self.weight_variable([64,128])
        b3 = self.bias_variable([128])
        h_layer = tf.nn.relu(tf.matmul(h_layer,W3) + b3)
        '''
        W2 = self.weight_variable([64, self.env.action_space.n])
        b2 = self.bias_variable([self.env.action_space.n])
        self.n_out = tf.identity(tf.matmul(h_layer,W2) + b2, name='ndense2')
        self.doOptimize()

    def doOptimize(self):

        with tf.name_scope("loss"):
            Q_action = tf.reduce_sum(tf.multiply(self.n_out, 
                                                 tf.one_hot(self.action,depth=self.env.action_space.n, 
                                                            dtype=tf.float32)), 
                                                 reduction_indices = 1)
            self.loss = tf.reduce_mean(tf.square(self.target - Q_action))
           
        with tf.name_scope("train"):
            self.optimize = tf.train.AdamOptimizer(LEARNING_RATE).minimize(self.loss)
   
    def weight_variable(self,shape):
        initial = tf.truncated_normal(shape)
        return tf.Variable(initial)

    def bias_variable(self,shape):
        initial = tf.constant(0.01, shape = shape)
        return tf.Variable(initial)

DT_LEN = 1000
BATCH_SIZE = 32
class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent
        self.agent.init_session()
        self.dataset = []

    def play(self, need_load = True):
        if need_load:
            self.agent.load()
        print('start to play')
        for i in range(100):
            rollout(self.env, self.agent, False, True, 500, True)
        self.env.close()

    def renew_dataset(self, dt_tmp):
        self.dataset.extend(dt_tmp)
        if len(self.dataset) > DT_LEN:
            self.dataset = self.dataset[len(self.dataset)-DT_LEN:]
#        print('dataset length: {}'.format(len(self.dataset)))

    def train(self, epoch=20000):
        for i in range(epoch):
            s = self.env.reset()
            done = False
            step = 0
            round_dataset = []
            while not done:      
                dataset_tmp = []
                
                a = self.agent.get_greedy_action([s])
                s_, rew, done, info = self.env.step(a)
#                self.env.render()
                '''

                if done:
                    rew = -1
                else:
                    rew = 1
                step += 1
                '''

                if done :
                  if rew == 1:
                      rew = rew+50

                  elif rew==0 and s_== 15:
                      rew = rew+50
                  else:
                      rew = rew-50
                else:
                   rew = rew+2
                   '''

                   if s == s_:
                      rew = rew-5

                   else:
                      if step > 1000:
                          rew = rew -2
                      else:
                          rew = rew+2
                      '''
#                if step > 200:
#                    break

                dataset_tmp.append((s, a, s_, rew, done))
                round_dataset.append((s, a, s_, rew, done))

                s  = s_
                self.renew_dataset(dataset_tmp)         
                if len(self.dataset) > BATCH_SIZE: 
                    ds = random.sample(self.dataset, BATCH_SIZE) 
                else:
                    ds = self.dataset
#                t = time.time() 
                self.agent.train([dt[0] for dt in ds],
                             [dt[1] for dt in ds],
                             [dt[2] for dt in ds],
                             [dt[3] for dt in ds],
                             [dt[4] for dt in ds])
#                print(time.time()-t)
                ''' 
                self.agent.train([dt[0] for dt in self.dataset],
                                 [dt[1] for dt in self.dataset],
                                 [dt[2] for dt in self.dataset],
                                 [dt[3] for dt in self.dataset],
                                 [dt[4] for dt in self.dataset])
                '''
#            print('------{}-----step: {}----{}'.format(i,step, self.agent.ep))
            print([dt[1] for dt in round_dataset])
            
#            print('aa:{}'.format([dt[1] for dt in self.dataset]))
            if i>300 and i % 100 == 0:
                print('------{}-----step: {}----{}'.format(i,step, self.agent.ep))
                self.play(False)
                print('------->',i)
                self.agent.save()
        self.env.close() 


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')
 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    print(isContinue)
    ep = 0
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
#    env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'

    env = gym.make(env_name)   
    fe = FLEnv()
    env = fe.get_env()

    agent = Agent(env, 'frozenlake_pg/pg', isContinue)
    player = Player(env, agent)
    getattr(player, args.td)()
    if args.td == 'train':
        player.play()
    print(env.P)
    env.close()         
