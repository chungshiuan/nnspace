import __init__
from tf_space.rl.utils import get_summary 
import numpy as np
import time

def rollout(env, agent=None, clear=False, render=False, MAXSTEP=500, need_history=False):
    obs, rew, done = env.reset(), 0, False
    obses, actses, rewses = [], [], []
    step=0
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            t1 = time.time() 
            prob = agent.get_acts([obs])
            t2 = time.time()
#            act_space = np.arange(env.action_space.n)
#            act = np.random.choice(act_space, p=prob)
            obs_, rew, done, info = env.step(prob)
            t3=time.time()
            print(t2-t1)
            print(t3-t2)
#            actses.append(act)
        obses.append(obs)
#        actses.append(act)
        rewses.append(rew)
        obs = obs_
        print('??????', rew)
        if done and need_history:
            print(get_summary(env, done, rew, render, clear ))
        else:
            get_summary(env, done, rew, render, clear )
        step+=1
        if step > MAXSTEP:
           print('max step:{} is reached'.format(MAXSTEP))
           break

    print('------------------- len rews: {}'.format(sum(rewses)))

    return obses, actses, rewses

