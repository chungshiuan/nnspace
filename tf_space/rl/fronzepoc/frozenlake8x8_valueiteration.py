import __init__
import numpy as np
import gym, time, os, argparse
from gym import wrappers
from gym import register
import random as pr
from tf_space.rl.utils import save, load, evaluate_policy
from fl88env import FLEnv
 

def rargmax(vector):
    """ Argmax that chooses randomly among eligible maximum indices. """
    m = np.amax(vector)
    indices = np.nonzero(vector == m)[0]
    return pr.choice(indices)

def extract_policy(v, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        q_sa = np.zeros(env.action_space.n)
        for a in range(env.action_space.n):
            for next_sr in env.P[s][a]:
                # next_sr is a tuple of (probability, next state, reward, done)
                p, s_, r, _ = next_sr
                '''
                if _ :
                   if r == 1 or s_==15:

                      r = r+10
                   else:
                      r = r-10
                else:
                   if s == s_:
                      r = r-2
                   else:
                      r = r+10
                '''
                q_sa[a] += (p * (r + gamma * v[s_]))
                print('!!!!!!', v[s_])
        print('{} position: with q_sa: {}'.format(s,q_sa))
        policy[s] = rargmax(q_sa)
        print('choose: {}'.format(policy[s]))
    print('ppppppp', policy)
    return policy


def value_iteration(env, gamma = 1.0):
    """ Value-iteration algorithm """
    v = np.zeros(env.nS)  # initialize value-function
    max_iterations = 200000
    eps = 1e-50
    for i in range(max_iterations):
        prev_v = np.copy(v)
        for s in range(env.nS):
            #q_sa = [sum([p*(r + prev_v[s_]) for p, s_, r, _ in env.P[s][a]]) for a in range(env.nA)]
            #print('>>>>>>',q_sa)
            q_sa = []
            for a in range(env.nA):
                print('====================')
                print(env.P[s][a])
                q_sum = 0
                for p, s_, r, _ in env.P[s][a]:
                    print('-------')
                    print('p:{}, s_:{}, r:{}, _: {}'.format(p,s_,r,_))
                    '''
                    if _ :
                      if r == 1 or s_==15:
                         r = r+10
                      else:
                         r = r-10
                    else:
                      if s == s_:
                         r = r-2
                      else:
                         r = r+10
                    '''
                    print('P[{}][{}] :{} to {} with r:{}'.format(s,a,p,s_,r))
                    print('s:',s, 'a:',a, 'q_sum:', q_sum,'>>',p*(r+prev_v[s_])) 
                    q_sum = q_sum + p*(r+prev_v[s_])
                q_sa.append(q_sum)      
                print('q_sa: ', q_sa)
            v[s] = max(q_sa)
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>',np.sum(np.fabs(prev_v - v)))
        if (np.sum(np.fabs(prev_v - v)) <= eps):
            print ('Value-iteration converged at iteration# %d.' %(i+1))
            break
    return v


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t') 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    PATH = 'frozenlake/valueiteration'
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
    #env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'
    gamma = 1
    env = gym.make(env_name)
    env = env.unwrapped
    fe = FLEnv()
    env = fe.get_env()
    if args.td == 'train': 
        optimal_v = value_iteration(env, gamma)
        print('##########################################################')
        policy = extract_policy(optimal_v, gamma)
        save(policy, PATH)
    policy = load(PATH)
    policy_score = evaluate_policy(env, policy, gamma, n=1000, render=True)
    print('Policy average score = ', policy_score)
