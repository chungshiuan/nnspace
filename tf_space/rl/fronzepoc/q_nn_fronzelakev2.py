import __init__
import numpy as np
import pickle, os, time
from tf_space.utils import check_path
import tensorflow as tf
import argparse, gym
import numpy as np

LEARNING_RATE = 0.81
EPSILON=0.0
EPSILON=0.4
GAMMA = 0.96
rew_time=20

hist = {'G':0, 'H':0}

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    print(init_np.shape)
    return init_np

class QModel(object):
    def __init__(self, scope_name, env):
        with tf.variable_scope(scope_name):
            self.obses = tf.placeholder(tf.int32, shape=[None, ], name='obses')
            obses_onehot = tf.one_hot(self.obses, depth=env.observation_space.n)
            self.n_out = tf.layers.dense(obses_onehot, units=36, activation=tf.nn.relu, name='ndense1')         
            self.Q = tf.layers.dense(self.n_out, units=env.action_space.n, name='ndense2')        
            print(self.Q)
 
class Agent(object):
    def __init__(self, env, model_path='model', cont=True, ep=0):
        self.model_path = model_path
        self.env = env
        self.ep = ep
        self.QP_obses = None
        self.QP_Q = None
        self.QT_obses = None
        self.QT_Q = None
        self.rew = None
        self.sess = None
        self.loss =None
        self.cont = cont
        self.optimize=None
#        self.init_session()

    def init_session(self):
        self.rew = tf.placeholder(tf.int32, shape=[None, ], name='rew')
        self.a = tf.placeholder(tf.int32, shape=[None, ], name='a')
        self.load_or_new_model()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
#        print('all trainable:', tf.trainable_variables())
#        print('all tensor:',  tf.get_default_graph().get_operations())

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load_or_new_model(self):
        if self.cont:
            self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
            self.sess = tf.Session()
            self.saver.restore(self.sess, self.model_path)
            print('all trainable:', tf.trainable_variables())
            print('all tensor:',  tf.get_default_graph().get_operations())
            for n in tf.get_default_graph().as_graph_def().node:
                if 'Optimization' in n.name:
                    print(n.name)
            self.QP_obses  = tf.get_default_graph().get_tensor_by_name('QP/obses:0')
            self.QP_Q = tf.get_default_graph().get_tensor_by_name('QP/ndense2/BiasAdd:0')
            self.QT_obses = tf.get_default_graph().get_tensor_by_name('QT/obses:0')
            self.QT_Q = tf.get_default_graph().get_tensor_by_name('QT/ndense2/BiasAdd:0')
            self.loss = tf.get_default_graph().get_tensor_by_name('Optimization/loss:0')
            self.optimize = tf.get_default_graph().get_operation_by_name('Optimization/foptimize')
        else:
            qp = QModel('QP', self.env)
            qt = QModel('QT', self.env)
            self.QP_obses = qp.obses
            self.QP_Q = qp.Q
            self.QT_obses = qp.obses
            self.QT_Q = qp.Q
            self.optimization()

    def copyq(self):
        t_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='QT')
        e_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='QP')
        self.target_replace_op = [tf.assign(t, e) for t, e in zip(t_params, e_params)]

    def optimization(self):
        with tf.variable_scope('Optimization'):
            target = tf.cast( self.rew, tf.float32) + GAMMA* tf.reduce_max(self.QT_Q, 1) 
            target = tf.stop_gradient(target)
            a_idx = tf.stack([tf.range(tf.shape(self.a)[0], dtype=tf.int32), self.a], axis=1)
            srcq = tf.gather_nd(params=self.QP_Q, indices=a_idx)
            self.loss = tf.reduce_sum(tf.square(target - srcq), name='loss')
            optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
            self.optimize = optimizer.minimize(self.loss, name='foptimize')

    def get_acts_from_qp(self, s):
        feed_dict = {
            self.QP_obses : [s]
        }
        action = self.sess.run(tf.argmax(self.QP_Q), feed_dict=feed_dict) 
        return action 

    def get_acts_from_qt(self, s):
        feed_dict = {
            self.QT_obses : [s]
        }
        action = self.sess.run(tf.argmax(self.QT_Q, axis=1), feed_dict=feed_dict)
        return action 

    def train(self, pholder): 
        feed_dict = {
            self.QP_obses: pholder['s'],
            self.QT_obses: pholder['s_'],
            self.a : pholder['a'],
            self.rew : pholder['r']
        }
        opt, loss = self.sess.run([self.optimize, self.loss], feed_dict=feed_dict)
        print('loss:{}'.format(loss))
        self.copyq()


def rollout(env, agent=None, clear=False, render = False):
    obs, rew, done, info = env.reset(), 0 , False, {}
    obses, acts, rews = [], [], []
    idx = 0
    if render:
        env.render()
        os.system('clear') 
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
#            print('!!?!?!!!!!!!!!!!!!!!!!!!!!------------',obs)
            act = agent.get_acts_from_qt(obs)[0]

#            act = np.squeeze(agent.get_acts_from_qt(obs), axis=0)
#            print('!!?!?!!!!!!!!!!!!!!!!!!!!!')
#            act_space = np.arange(env.action_space.n)
#            act = np.random.choice(act_space, p=prob)
            obs_, rew, done, info = env.step(act)
            acts.append(act)
        if done :
            if rew == 1 or obs_==15:
                rew = rew+20
            else:
                rew = rew-10
        else:
            if obs == obs_:
               rew = rew-5
            else:
               rew = rew+10
        obses.append(obs)
        rews.append(rew)
        print('{}--->'.format(idx))
        if render:
            env.render()
            if clear:
                if done:
                   if rew <= 0:
                      hist['H'] += 1
                      print('T____________T')
                   else:
                      hist['G'] += 1
                      print('>____________^')
                time.sleep(0.8)
                os.system('clear') 
        idx += 1
        obs = obs_
    return obses, acts, rews

   
        


class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load_or_new_model()
        print('start to play')
        for i in range(100): 
            rollout(self.env, self.agent, True, True)
            print('{} round to play'.format(i))
        print('success:{} , fail: {}'.format(hist['G'], hist['H']))

        self.env.close()

    def train(self, epoch=100):
        self.agent.init_session()
        for i in range(epoch):
            pholder = {
                's' : [],
                'a' : [],
                's_' : [],
                'a_' : [],
                'r': []
            }

            s = self.env.reset()
            done = False
            while not done:    
                a = self.agent.get_acts_from_qt(s)[0]
                s_, rew, done, info = self.env.step(a)
                if done :
                   if rew == 1 or s_==15:
                       rew = rew+20
                   else:
                       rew = rew-10
                else:
                   if s == s_:
                      rew = rew-5
                   else:
                      rew = rew+10
                a_ = self.agent.get_acts_from_qt(s_)[0]
                pholder['s'].append(s)
                pholder['a'].append(a)
                pholder['s_'].append(s_)
                pholder['a_'].append(a_)
                pholder['r'].append(rew)
                s = s_
            self.agent.train(pholder)
            if i % 50 == 0:
                print('------->',i)
                self.agent.save()
        self.env.close() 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')

 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    print(isContinue)
    ep = 0
    if args.td == 'train':
        ep = 0.4
    env = gym.make('FrozenLake-v0')
    agent = Agent(env, 'frozenlakeqnn/qnn', isContinue, ep)
    player = Player(env, agent)
    getattr(player, args.td)()
    env.close()
