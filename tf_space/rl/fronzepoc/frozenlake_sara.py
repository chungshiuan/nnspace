import __init__
import numpy as np
import gym, time, os, argparse
from gym import wrappers
from gym import register
import random as pr
from frozenlake_play import evaluate_policy, save, load

hist = {'G':0, 'H':0}

def extract_policy(q, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        policy[s] = np.argmax(q[s])
    return policy

def get_action(qtable,s, act_type=1):
    if act_type == 0:
        return np.random.choice([0,1,2,3])
    else:
        if np.random.uniform(0,1) <= 0:
            return np.random.choice([0,1,2,3])

        return np.argmax(qtable[s])

EPISODES = 10000
ACT_TYPE = 1
DISCOUNT = 0.8
ALPHA = 0.9
def get_qtable(env): 
    qtable = np.ones((env.observation_space.n, env.action_space.n))
    for i in range(EPISODES):
        s = env.reset()
        complete = False
        history = []
        G = 0
        a = get_action(qtable, s, ACT_TYPE)

        while complete == False:

            s_, reward, complete, _ = env.step(a)
            a_ = get_action(qtable, s_, ACT_TYPE)
            if complete:
                td_target = reward
            else:
                td_target = reward + DISCOUNT * qtable[s_, a_] 
            td_delta = td_target - qtable[s, a]
            qtable[s,a] += ALPHA*td_delta
            print('qtable:{}'.format(qtable))
            print('{} ==> action:{}, reward:{} ,s_:{}'.format(i,a, reward, s_))
            s = s_
            a = a_
    return qtable
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t') 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    PATH = 'frozenlake/sara'
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
    #env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'
    gamma = 1.0
    env = gym.make(env_name)
    env = env.unwrapped
    if args.td == 'train': 
        q = get_qtable(env)
        print('q:{}'.format(q))
        policy = extract_policy(q)
        save(policy, PATH)
    policy = load(PATH)
    policy_score = evaluate_policy(env, policy, gamma, n=1000)
    print('Policy average score = ', policy_score)
