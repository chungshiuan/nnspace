import __init__
import numpy as np
import pickle, os, time
from tf_space.utils import check_path
import tensorflow as tf
import argparse, gym
import numpy as np
from cp_common import rollout
import random
from gym import wrappers
from gym import register
from fl88env import FLEnv

LEARNING_RATE = 0.001
GAMMA = 0.9
INITIAL_EPSILON = 0.5 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon

logs_path = os.path.join('./tmp_2l_relu',str(int(time.time())))


def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    #print(init_np.shape)
    return init_np

class QModel(object):
    def __init__(self, scope_name, env):
        self.env =env
        with tf.variable_scope(scope_name):
            self.obses = tf.placeholder(tf.float32, shape=[None, 16], name='obses')
            self.n_out = tf.layers.dense(self.obses, 
#                                    kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                     kernel_initializer=tf.truncated_normal_initializer,
                                    
                                     bias_initializer=tf.constant_initializer(0.01), 
                                     units=64, 
                                     activation=tf.nn.relu, 
                                     name='ndense1')         
            self.Q = tf.layers.dense(self.n_out, 
#                                     kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                     kernel_initializer=tf.truncated_normal_initializer,

                                     bias_initializer=tf.constant_initializer(0.01), 
                                     units=self.env.action_space.n,
                                     activation=tf.nn.relu, 

                                     name='ndense2')       

            print('==== QQQ ===',self.Q)
 
class Agent(object):
    def __init__(self, env, model_path='model', cont=True, ep=0):
        self.model_path = model_path
        self.env = env
        self.ep = ep
        self.QP_obses = None
        self.QP_Q = None
        self.QT_obses = None
        self.QT_Q = None
        self.rew = None
        self.sess = None
        self.loss =None
        self.cont = cont
        self.optimize=None
        self.loop=0
#        self.init_session()

    def init_session(self):
        self.reward = tf.placeholder(tf.float32, shape=[None, ], name='rew')
        self.action = tf.placeholder(tf.int32, shape=[None, ], name='a')
        self.load()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
#        print('all trainable:', tf.trainable_variables())
#        print('all tensor:',  tf.get_default_graph().get_operations())

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        if self.cont:
            self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
            self.sess = tf.Session()
            self.saver.restore(self.sess, self.model_path)
            print('all trainable:', tf.trainable_variables())
            print('all tensor:',  tf.get_default_graph().get_operations())
            for n in tf.get_default_graph().as_graph_def().node:
                if 'Optimization' in n.name:
                    print(n.name)
            self.QP_obses  = tf.get_default_graph().get_tensor_by_name('QP/obses:0')
            self.QP_Q = tf.get_default_graph().get_tensor_by_name('QP/ndense2/BiasAdd:0')
            self.QT_obses = tf.get_default_graph().get_tensor_by_name('QT/obses:0')
            self.QT_Q = tf.get_default_graph().get_tensor_by_name('QT/ndense2/BiasAdd:0')
            self.loss = tf.get_default_graph().get_tensor_by_name('Optimization/loss:0')
            self.optimize = tf.get_default_graph().get_operation_by_name('Optimization/foptimize')
        else:
            qp = QModel('QP', self.env)
            qt = QModel('QT', self.env)
            self.QP_obses = qp.obses
            self.QP_Q = qp.Q
            self.QT_obses = qt.obses
            self.QT_Q = qt.Q
            self.optimization()
        self.copyq()
#        self.get_gradient('QP')

    def get_trainable(self, scope):
        vars = tf.get_collection(key=tf.GraphKeys.GLOBAL_VARIABLES, scope=scope)
        print(vars)
        return vars

    def get_gradient(self, scope):
        grads = tf.gradients(self.loss, self.get_trainable(scope))
        tf.summary.scalar("loss", self.loss)
        for index, i_grad in enumerate(grads):
            tf.summary.histogram('histogram_{}_{}'.format(scope, index), i_grad)
        self.sm = tf.summary.merge_all() 
        self.writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

    def copyq(self):
        t_params = self.get_trainable('QT')
        e_params = self.get_trainable('QP')
        self.target_replace_op = [tf.assign(t, e) for t, e in zip(t_params, e_params)]

    def optimization(self):
        with tf.variable_scope('Optimization'):
            print('>>>>>>', tf.reduce_max(self.QT_Q, 1))
            print('!!!!!!', self.reward)
            target =  self.reward + GAMMA* tf.reduce_max(self.QT_Q, 1) 
            target = tf.stop_gradient(target)
            a_idx = tf.stack([tf.range(tf.shape(self.action)[0], dtype=tf.int32), self.action], axis=1)
#            a_idx = tf.Print(a_idx,['>>>>>',a_idx], summarize=10)
            srcq = tf.gather_nd(params=self.QP_Q, indices=a_idx)
            print('###############',srcq)
            print('###############',target)

            self.loss = tf.reduce_mean(tf.square(target - srcq), name='loss')
            optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
            self.optimize = optimizer.minimize(self.loss, name='foptimize')

    def get_acts(self, obses):
        return self.get_acts_from_qp(obses)

    def get_greedy_action(self, obses):
        if np.random.uniform(0,1) <= self.ep:
            action = self.env.action_space.sample()
        else:
            action = self.get_acts_from_qp(obses)
        self.ep -= (INITIAL_EPSILON - FINAL_EPSILON)/2000
        return action


    def get_acts_from_qp(self, s):
        feed_dict = {
            self.QP_obses : np_one_hot(s, 16)
        }
        action = self.sess.run(tf.argmax(self.QP_Q, axis=1), feed_dict=feed_dict)[0] 
        return action 

    def get_acts_from_qt(self, s):
        feed_dict = {
            self.QT_obses : np_one_hot(s, 16)
        }
        action = self.sess.run(tf.argmax(self.QT_Q, axis=1), feed_dict=feed_dict)[0]
        return action 
 
    def train(self, obses, actions, obses_, rewards, dones): 
        feed_dict = {
            self.QP_obses : np_one_hot(obses, 16),
            self.QT_obses : np_one_hot(obses_, 16),
            self.reward: np.array(rewards),
            self.action: np.array(actions)
        }
        opt, loss = self.sess.run([self.optimize, self.loss], feed_dict=feed_dict)

#        opt, loss, summary = self.sess.run([self.optimize, self.loss, self.sm], feed_dict=feed_dict)
#        self.writer.add_summary(summary, self.loop)
        self.sess.run(self.target_replace_op)      
        self.loop+=1
 
DT_LEN = 1000
BATCH_SIZE = 32
class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent
        self.agent.init_session()
        self.dataset = []

    def play(self, need_load = True, rg=100):
        if need_load:
            self.agent.load()
        print('start to play')
        for i in range(rg):
            rollout(self.env, self.agent, False, True, 500, True)
        self.env.close()

    def renew_dataset(self, dt_tmp):
        self.dataset.extend(dt_tmp)
        if len(self.dataset) > DT_LEN:
            self.dataset = self.dataset[len(self.dataset)-DT_LEN:]
#        print('dataset length: {}'.format(len(self.dataset)))

    def train(self, epoch=20000):
        for i in range(epoch):
            s = self.env.reset()
            done = False
            step = 0
            round_dataset = []
            while not done:      
                dataset_tmp = []
                
                a = self.agent.get_greedy_action([s])
                s_, rew, done, info = self.env.step(a)
#                self.env.render()
                '''

                if done:
                    rew = -1
                else:
                    rew = 1
                step += 1
                '''

                if done :
                  if rew == 1:
                      rew = rew+50
                      print('^__________________________^')
                  elif rew==0 and s_== 15:
                      rew = rew+50
                  else:
                      rew = rew-50
                else:
                   rew = rew+2
                   '''

                   if s == s_:
                      rew = rew-5

                   else:
                      if step > 1000:
                          rew = rew -2
                      else:
                          rew = rew+2
                      '''
#                if step > 200:
#                    break

                dataset_tmp.append((s, a, s_, rew, done))
                round_dataset.append((s, a, s_, rew, done))

                s  = s_
                self.renew_dataset(dataset_tmp)         
                if len(self.dataset) > BATCH_SIZE: 
                    ds = random.sample(self.dataset, BATCH_SIZE) 
                else:
                    ds = self.dataset
#                t = time.time() 
                self.agent.train([dt[0] for dt in ds],
                             [dt[1] for dt in ds],
                             [dt[2] for dt in ds],
                             [dt[3] for dt in ds],
                             [dt[4] for dt in ds])
#                print(time.time()-t)
                ''' 
                self.agent.train([dt[0] for dt in self.dataset],
                                 [dt[1] for dt in self.dataset],
                                 [dt[2] for dt in self.dataset],
                                 [dt[3] for dt in self.dataset],
                                 [dt[4] for dt in self.dataset])
                '''
            print('------{}-----step: {}----> {}'.format(i,step, self.agent.ep))
            print([dt[1] for dt in round_dataset])
            
#            print('aa:{}'.format([dt[1] for dt in self.dataset]))
            if i>=300 and i % 200 == 0:
                print('------{}-----step: {}----> {}'.format(i,step, self.agent.ep))
                self.play(False,3)
                print('------->',i)
                self.agent.save()
        self.env.close() 


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')
 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    print(isContinue)
    ep = 0
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
#    env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'

    env = gym.make(env_name)   
    fe = FLEnv()
    env = fe.get_env()

    agent = Agent(env, 'frozenlake_pg/pg', isContinue, ep=5)
    player = Player(env, agent)
    getattr(player, args.td)()
    if args.td == 'train':
        player.play()
    env.close()         
