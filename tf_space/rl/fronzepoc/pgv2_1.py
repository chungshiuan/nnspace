import __init__
import gym, argparse
import os, time
import tensorflow as tf
import numpy as np
from tf_space.rl.utils import get_summary 

LEARNING_RATE=0.001

INITIAL_EPSILON = 0.5 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon

hist = {'G':0, 'H':0}
logs_path = os.path.join('./pglog',str(int(time.time())))

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np

class Agent(object):
    def __init__(self, env, model_path='model', ep=INITIAL_EPSILON):
        self.env = env
        self.obses = None
        self.acts = None
        self.rews = None
        self.out_prob = []
        self.optimize = None
        self.sess = None
        self.saver = None
        self.model_path = model_path
        self.loss = None
        self.ep = ep

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.out_prob = tf.get_default_graph().get_tensor_by_name('out_prob:0')
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def net(self):
        self.obses = tf.placeholder(tf.float32, shape=[None, self.env.observation_space.n],name='obses')
        self.acts = tf.placeholder(tf.int32, name='acts')
        self.rews = tf.placeholder(tf.float32, name='rews')
        self.n_out = tf.layers.dense(self.obses, 
                                units=64, 
#                                activation=tf.nn.relu, 
                                activation=tf.nn.tanh,  # tanh activation
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')         
        self.n_out = tf.layers.dense(self.n_out, 
                                activation=tf.nn.tanh,  # tanh activation
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=self.env.action_space.n, 
                                name='dense2') 

        self.out_prob = tf.nn.softmax(self.n_out, name='out_prob')
        self.optimize_section()
        self.getSummary()

    def optimize_section(self):
        self.out = tf.reduce_sum(-tf.log(self.out_prob)*tf.one_hot(self.acts, self.env.action_space.n), axis=1) 
        self.loss = tf.reduce_mean(self.out * self.rews)
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)

    def getSummary(self):
        tf.summary.scalar('loss', self.loss)
        self.summary = tf.summary.merge_all()
        self.writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

    def optimize_section_CEL(self):
        # to maximize total reward (log_p * R) is to minimize -(log_p * R), and the tf only have minimize(loss)
        print(tf.cast(tf.one_hot(self.acts, self.env.action_space.n),tf.int64))
        self.out = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.n_out, 
                                                                  labels=self.acts)   # this is negative log of chosen action
        # or in this way:
        # neg_log_prob = tf.reduce_sum(-tf.log(self.all_act_prob)*tf.one_hot(self.tf_acts, self.n_actions), axis=1)
        self.loss = tf.reduce_mean(self.out * self.rews)  # reward guided loss
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)


    def get_greedy_action(self, obses):
        if np.random.uniform(0,1) <= self.ep:
            action = self.env.action_space.sample()
        else:
            action = self.get_acts(obses)
        self.ep -= (INITIAL_EPSILON - FINAL_EPSILON)/10000 
        return action

    def get_acts(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np_one_hot(obses, self.env.observation_space.n)}
        return np.argmax(self.sess.run(self.out_prob, feed_dict = feed_dict)[0])

    def train(self, obses, acts, rews, times):
        feed_dict = {
            self.obses : np_one_hot(obses, self.env.observation_space.n),
            self.acts: acts,
            self.rews: rews
        }
        opt, loss,sm, out = self.sess.run([self.optimize, self.loss, self.summary, self.out], feed_dict=feed_dict)
        print('out:{}'.format(out))
        print('loss:{}'.format(loss))
        self.writer.add_summary(sm, times)
        self.ep -= (INITIAL_EPSILON - FINAL_EPSILON)/10000 



def rollout(env, agent=None, clear=False, render = False, sleep = 0):
    obs, rew, done, info = env.reset(), 0 , False, {}
    obses, acts, rews = [], [], []
    idx = 0
    can_train=False
    if render:
        env.render()
#        os.system('clear') 
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            act = agent.get_acts([obs])
            obs_, rew, done, info = env.step(act)
            acts.append(act)
        if done :
            if rew == 1:
                rew = rew+200
                can_train=True
#            elif rew==0 and obs_== 15:
#                rew = rew+200
#                can_train=True
            else:
                rew = rew -200
        else:
            rew = rew+2
            '''
            rew = 0
            if obs == obs_:
               rew = rew
            else:
               rew = rew+5
            '''
        obses.append(obs_)
        rews.append(rew)

        print(get_summary(env, done, rew, render, clear, sleep))
        idx += 1
        obs = obs_
    return obses, acts, rews, can_train

class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load()
        print('start to play')
        for i in range(100):
            print('############################', i)
            rollout(self.env, self.agent, True, True,sleep=0.1)
        self.env.close()

    def get_discount_sum_of_rewards(self, rewards, gamma=0.7):
        print(rewards)
        adv = np.zeros_like(rewards)
        print(adv)
        c = 0
        for i, r in enumerate(rewards[::-1]):
            c = r + gamma *c
            adv[i] = c
#        adv -= np.mean(adv)
#        adv /= (np.std(adv) + 1e-10)
        print('###################')
        print('####',adv[::-1])
        print('###################')

        return adv[::-1]

    def get_discount_sum_of_rewards1(self, rewards, gamma=0.99):
        print(rewards)
        adv = np.zeros_like(rewards)
        print(adv)
        c = 0
        for r in rewards:
            c = c + r
        for i, r in enumerate(rewards[::-1]):
            adv[i] = c
        return adv[::-1]

    def train(self, epoch=10000):
        print('start to train')
        self.agent.init_session()
        i=0
        while i < epoch:
            obses, acts, rews, can_train = rollout(self.env, self.agent)

            if can_train or np.random.uniform(0,1) <= 0.05 or True:
                print('=================== train round:',i)

                print('obses: {}'.format(obses))
                print('acts: {}'.format(acts))
                dis_rews = self.get_discount_sum_of_rewards(rews)
                if rews[-1] > 50:
                    print('------>>> rews: {}'.format(rews))
                    print('------>>> dis_rew: {}'.format(dis_rews))
                self.agent.train(obses, acts, dis_rews, i)
                i+=1
                if i % 1000 == 0:
                    self.agent.save()
        self.agent.close_session()
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    args = parser.parse_args()
    print(args.td)

    env = gym.make('FrozenLake-v0')
    agent = Agent(env, 'frozenlake_pg/pg')
    player = Player(env, agent)
    getattr(player, args.td)()
    if args.td == 'train':
        player.play()
    env.close()         
