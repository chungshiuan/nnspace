"""
Solving FrozenLake8x8 environment using Value-Itertion.


Author : Moustafa Alzantot (malzantot@ucla.edu)
"""
import numpy as np
import gym, time, os
from gym import wrappers
from gym import register
import random as pr
hist = {'G':0, 'H':0}

def rargmax(vector):
    """ Argmax that chooses randomly among eligible maximum indices. """
    m = np.amax(vector)
    indices = np.nonzero(vector == m)[0]
    return pr.choice(indices)

def run_episode(env, policy, gamma = 1.0, render = False):
    """ Runs an episode and return the total reward """
    obs = env.reset()
    total_reward = 0
    step_idx = 0
    while True:
        if render:
            env.render()
        obs, reward, done , _ = env.step(int(policy[obs]))
        total_reward += (gamma ** step_idx * reward)
        step_idx += 1
        if done:
            env.render()
            if reward == 0:
               hist['H'] += 1
               print('T_______T')
            else:
               hist['G'] += 1
               print('^_______<')
            break
        time.sleep(1)
    print('success:{} , fail: {}'.format(hist['G'], hist['H']))
#        os.system('clear')
    return total_reward


def evaluate_policy(env, policy, gamma = 1.0,  n = 100):
    """ Evaluates a policy by running it n times.
    returns:
    average total reward
    """
    scores = [
            run_episode(env, policy, gamma = gamma, render = True)
            for _ in range(n)]
    return np.mean(scores)

def extract_policy(q, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        policy[s] = np.argmax(q[s])
    return policy

def get_action(qtable,s, act_type=1):
    if act_type == 0:
        return np.random.choice([0,1,2,3])
    else:
        if np.random.uniform(0,1) <= 0.2:
            return np.random.choice([0,1,2,3])

        return np.argmax(qtable[s])

EPISODES = 50000
ACT_TYPE=1

def get_qtable(env): 
    qtable = np.zeros((env.observation_space.n, env.action_space.n))
    sa_values = np.zeros(qtable.shape)
    sa_count = np.ones(qtable.shape)

    for i in range(EPISODES):
        s = env.reset()
        complete = False
        history = []
        while complete == False:
            action = get_action(qtable, s, ACT_TYPE)
            s_, reward, complete, _ = env.step(action)
            print('>>>>>>',complete)
            if complete :
                if reward == 1 or s_==15:
                    reward = reward+20
                else:
                    reward = reward-10
            else:
                if s == s_:
                    reward = reward-5
                else:
                    reward = reward+10
            print('?????', history)
            history.append((action, s_, reward))
        print('history:{}'.format(history))
        for ha, hs, sr in history:
            sa_values[hs, ha] += sr
            sa_count[hs, ha] +=1
        print('------------------')
        print('sa_values:{}'.format(sa_values))
        print('sa_count:{}'.format(sa_count))
        print('------------------')

        qtable = sa_values / sa_count
    print('qtable:{}'.format(qtable))
    return qtable
            

if __name__ == '__main__':
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
    #env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
    #env_name = 'FrozenLakeNotSlippery-v0'
    gamma = 1.0
    env = gym.make(env_name)
    env = env.unwrapped
    q = get_qtable(env)
    policy = extract_policy(q)
    policy_score = evaluate_policy(env, policy, gamma, n=1000)
    print('Policy average score = ', policy_score)
