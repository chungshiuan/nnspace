import __init__
import sys
from dataset_factory import LSUNDS
from gan.wgan_gp import WGANGP 


if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = WGANGP(dataset=LSUNDS(epoch=3000, reshape_size=[-1, 128*128*3]), disc_image_shape=[128, 128, 3])    
    getattr(net, exe_type)()
