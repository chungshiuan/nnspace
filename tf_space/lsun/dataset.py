import os, sys, argparse, lmdb, cv2
import __init__
import pandas as pd
from scipy.stats import itemfreq
import tensorflow as tf
from io import BytesIO
import PIL.Image
from base_dataset import FileData
import numpy as np
from plot import plotFigures
import utils



class Data(FileData):
    def __init__(self, folder='Lsun', select_category='church_outdoor_train_lmdb', resize=[]):
        super(Data, self).__init__(folder, resize=resize)
        self._select_category = select_category.split(',')
        self._check_root() 

    def fetch(self):
        pass

    def get_records(self, epoch=2):
        def all_record(categorys):
            keys = []
            labels = []
            codes = []
            for ct in categorys:
                env = lmdb.open(os.path.join(self.root, ct))
                with env.begin() as txn:
                    for key, _ in txn.cursor():
                        keys.append(key)
                        codes.append(key)
                        labels.append(ct)
            return keys, labels, codes
        self.files, self.labels, self.codes = all_record(self._select_category)
        return super(Data, self).get_records(epoch)  

    def parser(self, *record):
        print('== start to parse record ==')
        def get_image_from_imdb(key, dbname):
            #print('???? 1')
            image = ''
            env = lmdb.open(os.path.join(self.root, dbname.decode('utf-8')))
            with env.begin() as txn:
                #print('!!!!!!!!!',key)
                #print('!!!!!!!!!',txn.get(key))
                image = cv2.imdecode(np.fromstring(txn.get(key),dtype=np.uint8),1)
            image = cv2.resize(image,self.resize)
            #print('??????????????????????!!!!!!!',image.shape)
            return image
        image = tf.py_func(
            get_image_from_imdb,
            [record[0], record[1]],
            tf.uint8
        )
        #image = tf.decode_raw(image, tf.uint8)
        #image = tf.image.resize_images(image, [128, 128])

        return image, record[0], record[1]

    def get_labels(self, all = False):
        return self._select_category
   
    def list_category(self):
        dirs = os.listdir(self.root)
        ret = ','.join(dirs)
        return ret 



if __name__ == '__main__':
    dog = Data(resize=[128,128])
    if sys.argv[1] == 'fetch':
        dog.fetch()  
    elif sys.argv[1] == 'image':
        iterator =  dog.get_records(1)
        image_batch, label_batch, codes = iterator.get_next()
        with tf.Session() as sess:
            sess.run((tf.global_variables_initializer(),tf.local_variables_initializer()))
            sess.run(iterator.initializer)
            ep = 0
            while True:
                try:
                    labels, images, t_labels = sess.run([label_batch, image_batch, codes])
                    print('--------------------------------', ep)
                    #print(len(img))
                    print(len(labels))
                    #if b'dog' in labels:
                    print(images.shape)
                    plotFigures(images, labels=t_labels, realname=labels)
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print(e)
                    break
                ep += 1
    elif sys.argv[1] == 'ls':
        print('category: {}'.format(dog.list_category()))
    else:
        print(len(dog.get_labels(True)))
