import sys, time, os
import tensorflow as tf
import numpy as np
import __init__
from gan.wdcgan_gp import WDCGANGP
from dataset_factory import LSUNDS



if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    net = WDCGANGP(LSUNDS(reshape_size=[-1, 128, 128, 3]), disc_image_shape=[128,128,3])    
    getattr(net, exe_type)()
