import os, sys
import __init__
import pandas as pd
from scipy.stats import itemfreq
from zipfile import ZipFile
import tensorflow as tf
from io import BytesIO
import PIL.Image
from base_dataset import DataSet
import numpy as np
from plot import plotFigures
import utils
from tf_space.dogbreed.const import *


class Data(DataSet):
    def __init__(self, folder='KaggleDog', resize=[]):
        super(Data, self).__init__(folder=folder, resize=resize)
        self.train_path = os.path.join(self.root, 'train.zip')
        self.label_csv = os.path.join(self.root, 'labels.csv.zip')

    def gen_records(self):
        self._check_records()
        archive_train = ZipFile(self.train_path, 'r')            
        print(archive_train.namelist()[1:10])
        labels_raw = pd.read_csv(self.label_csv, compression='zip', header=0, sep=',', quotechar='"')
        select_breed = self.get_labels()
        current_index = 0
        writer = None
        count = {}
        for image_name in archive_train.namelist()[1:]:
            image_code = image_name.split('/')[1].split('.')[0]
            image_label = labels_raw.loc[labels_raw.id == image_code, 'breed'].tolist()[0].encode('utf-8')
            if image_label not in select_breed:
                continue 
            if current_index % 100 == 0:
                if writer:
                   writer.close()
                record_filename = '{rl}-{ci}.tfrecords'.format(rl=self.records_train, ci=current_index)
                writer = tf.python_io.TFRecordWriter(record_filename)
            filename = BytesIO(archive_train.read(image_name))
            image = PIL.Image.open(filename) # original image size (500,300), (500, 299)
            image = np.array(image.resize((WH_SIZE,WH_SIZE))) # shape (128,128,3)
            image_byte = image.tobytes()
            image_h, image_w, image_c = image.shape
            example = tf.train.Example(features=tf.train.Features(feature={
                      'label': tf.train.Feature(bytes_list=tf.train.BytesList(value=[image_label])),
                      'image': tf.train.Feature(bytes_list=tf.train.BytesList(value=[image_byte])),
                      'hight': tf.train.Feature(int64_list=tf.train.Int64List(value=[image_h])),
                      'weight': tf.train.Feature(int64_list=tf.train.Int64List(value=[image_w])),
                      'channel': tf.train.Feature(int64_list=tf.train.Int64List(value=[image_c])),
                      'code': tf.train.Feature(bytes_list=tf.train.BytesList(value=[image_code.encode('utf-8')]))
            }))
            writer.write(example.SerializeToString())
            if image_label in count.keys():
               count[image_label] += 1
            else:
               count[image_label] = 1
            current_index += 1
            print(image_name)
        print( count)
        writer.close()

    def get_labels(self, all = False):
        labels_raw = pd.read_csv(self.label_csv, compression='zip', header=0, sep=',', quotechar='"')
        labels_freq = itemfreq(labels_raw['breed'])
        labels_freq = labels_freq[labels_freq[:,1].argsort()[::-1]]
        if not all:
           return ['{}'.format(breed).encode('utf-8') for breed in np.sort(labels_freq[0:BREED_SIZE,0])]
        else:
           return ['{}'.format(breed).encode('utf-8') for breed in np.sort(labels_freq[:,0])]

    def parser(self, record):
        features = tf.parse_single_example( record,
                                        features = {
                                        'label': tf.FixedLenFeature([], tf.string),
                                        'image': tf.FixedLenFeature([], tf.string),
                                        'hight': tf.FixedLenFeature([], tf.int64),
                                        'weight': tf.FixedLenFeature([], tf.int64),
                                        'channel': tf.FixedLenFeature([], tf.int64),
                                        'code': tf.FixedLenFeature([], tf.string),
                                        })
        record_image = tf.decode_raw(features['image'], tf.uint8) #shape (128*128*3)
        label = tf.cast(features['label'], tf.string)
        hight = tf.cast(features['hight'], tf.int32)
        weight = tf.cast(features['weight'], tf.int32)
        channel = tf.cast(features['channel'], tf.int32)
        image = tf.reshape(record_image, tf.stack([hight, weight, channel]))
        image.set_shape([WH_SIZE,WH_SIZE,3])
        if len(self.resize) == 2:
            image = tf.image.resize_images(image, self.resize)
        image = tf.clip_by_value(image/256.0, 0.0, 1.0)

        code = tf.cast(features['code'], tf.string)
        return image, label, code

if __name__ == '__main__':
    dog = Data(resize=[64,64])
    if sys.argv[1] == 'gen':
        dog.gen_records()  
    elif sys.argv[1] == 'image':
        iterator =  dog.get_records(1)
        image_batch, label_batch, code = iterator.get_next()
        labels = dog.get_labels()
        train_labels = tf.map_fn(lambda l: tf.where(tf.equal(labels, l))[0,0:1][0], label_batch, dtype=tf.int64)
        one_hot_label_batch = tf.one_hot(train_labels, depth=len(labels), dtype=tf.float32,name="label_batch")
 
        with tf.Session() as sess:
            sess.run((tf.global_variables_initializer(),tf.local_variables_initializer()))
            sess.run(iterator.initializer)
            ep = 0
            while True:
                try:
                    labels, images, t_labels = sess.run([label_batch, image_batch, train_labels])
                    print('--------------------------------', ep)
                    #print(len(img))
                    print(len(labels))
                    #if b'dog' in labels:
                    plotFigures(images, labels=t_labels, realname=labels)
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print(e)
                    break
                ep += 1
    else:
        print(len(dog.get_labels(True)))
