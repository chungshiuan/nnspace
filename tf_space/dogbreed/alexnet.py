import glob,time,os
import tensorflow as tf
import numpy as np
from dataset import Data
from cnn import *
from plot import  plotFigures, genCnnImg, genImage, genFilterImg
from const import *

EPOCH_NUM = 3000
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
dataset = Data()

logs_path = os.path.join('./tmp_2l_relu',str(int(time.time())))
labels =dataset.get_labels()

iterator = dataset.get_records(EPOCH_NUM)
image_batch, label_batch, codes = iterator.get_next()
train_labels = tf.map_fn(lambda l: tf.where(tf.equal(labels, l))[0,0:1][0], label_batch, dtype=tf.int64)
image_batch = tf.cast(image_batch, tf.float32)
keep_prob_cnn = tf.placeholder(tf.float32)
keep_prob = tf.placeholder(tf.float32)
alex = alex_conv(image_batch, [11,11,3,96], [1,4,4,1])
alex = alex_maxpool(alex, [1,3,3,1], [1,2,2,1])
alex = alex_conv(alex, [5,5,96,256], group=2)
alex = alex_maxpool(alex,[1,3,3,1],[1,2,2,1])
alex = alex_conv(alex, [3,3,256,384], group=2)
alex = alex_conv(alex, [3,3,256,384], group=2)
alex = alex_conv(alex, [3,3,384,256], group=2)
alex = alex_maxpool(alex, [1,3,3,1],[1,2,2,1])
flattened = tf.reshape(alex, [-1, pool5.get_shape()[1:4].num_elements()])
fc = alex_fc(flattened, [pool5.get_shape()[1:4].num_elements(),4096]) 
fc = tf.nn.dropout(fc, keep_prob_cnn)
fc = alex_fc(fc, [4096, 4096])
fc6 = tf.nn.dropout(fc6, keep_prob_cnn)
fc = alex_fc(fc, [4096, len(labels)], need_relu=False)

LEARNING_RATE=1e-4
final_labels = tf.one_hot(train_labels, depth=o_size, dtype=tf.float32,name="label_batch")
diff = tf.nn.softmax_cross_entropy_with_logits(labels= final_labels, logits=fc)
loss = tf.reduce_mean(diff)
global_step = tf.Variable(0)
Y_SOF = tf.nn.softmax(Y)
train = tf.train.AdamOptimizer(LEARNING_RATE).minimize(loss)
pred = tf.argmax(Y_SOF, 1)
ans = tf.argmax(final_labels, 1)

correct_prediction = tf.equal(pred, ans)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

tf.summary.scalar("cost", loss)
tf.summary.scalar("accuracy", accuracy)
summary_op = tf.summary.merge_all()
writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())


if __name__ == '__main__':
    loop = 0
    with tf.Session() as sess:
        sess.run(init_op)
        sess.run(iterator.initializer)
        while True:
            try:
                print( '=============',loop)
                _, acc = sess.run([ train, accuracy ], {keep_prob_cnn:0.3, keep_prob:0.4})
                writer.add_summary(summary, loop)
                print('accuracy:{}'.format(acc))
            except tf.errors.OutOfRangeError:
                break
            except Exception as e:
                print(e)
                break
            loop += 1 
 

        

