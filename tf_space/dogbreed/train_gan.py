import sys, os, datetime
import tensorflow as tf
import numpy as np
from dataset import Data
from const import *
from utils import check_path
from gan import generator, discriminator
 
EPOCH_NUM = 100000
LEARNING_RATE = 0.0002
NOISE_DIM = 100
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
MODEL_FOLDER = os.path.join(check_path('./gan_model'), 'dogbreed')

print('MODEL PATH: {}'.format(MODEL_FOLDER))
dataset = Data()
iterator = dataset.get_records(EPOCH_NUM)
disc_input, _, _ = iterator.get_next()
disc_input = tf.cast(disc_input, tf.float32)
gen_input_place = tf.placeholder(tf.float32, shape=[None, NOISE_DIM])
gen_input = gen_input_place[0:tf.shape(disc_input)[0],:]
gen_output = generator(gen_input, WH_SIZE, WH_SIZE)
disc_real = discriminator(disc_input)
disc_gen = discriminator(gen_output, reuse=True)

gen_loss = -tf.reduce_mean(tf.log(disc_gen))
l_dis_real = tf.log(disc_real)
diff_disc_gen = 1.-disc_gen
l_dis_gen = tf.log(diff_disc_gen)
disc_loss = -tf.reduce_mean(l_dis_real + l_dis_gen)
#disc_loss = -tf.reduce_mean(tf.log(disc_real) + tf.log(1.- disc_gen))

optimizer_gen = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE)
optimizer_disc = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE) 
tvars = tf.trainable_variables()
disc_vars = [var for var in tvars if 'd-' in var.name]
print('disc vars: {}'.format(disc_vars))
gen_vars = [var for var in tvars if 'g-' in var.name]
print('gen vars: {}'.format(gen_vars))
train_gen = optimizer_gen.minimize(gen_loss, var_list=gen_vars)
train_disc = optimizer_disc.minimize(disc_loss, var_list=disc_vars)

init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
saver = tf.train.Saver()


def train(sess):
    print('=== select train ===')

    loop = 0
    while True:
        try:
            noise = np.random.uniform(0., 1., size=[BATCH_SIZE, NOISE_DIM]).astype('f')
            feed_dict = {gen_input_place:noise}
            _, _, gl, dl, dg, dr,ldr, ldg, ddg = sess.run([train_gen, train_disc, gen_loss, disc_loss, disc_gen, disc_real, l_dis_real, l_dis_gen, diff_disc_gen],
                                          feed_dict=feed_dict)
            print('??? dis_gen:', dg)
            print('<<< dis_real:', dr)

            print('----ldr:',loop, ldr)
            print('====ldg:',loop, ldg)
            print('====dff:',loop, ddg)

            if loop % 1 == 0 or loop == 0:
               print('Step %i: Generator Loss: %f, Discriminator Loss: %f' % (loop, gl, dl))
               saver.save(sess, MODEL_FOLDER)
        except tf.errors.OutOfRangeError:
            break
        except Exception as e:
            print(e)
            break
        loop += 1

def pred(sess):
    print('=== select pred ===')
    saver.restore(sess, MODEL_FOLDER)
    noise = np.random.uniform(-1., 1., size=[BATCH_SIZE, NOISE_DIM]).astype('f')
    feed_dict = {gen_input_place:noise}
    result = sess.run([gen_output],feed_dict=feed_dict)
    print(result)


if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'pred' or sys.argv[1] == 'train':
            exe_type = sys.argv[1]
    
    with tf.Session() as sess:
        sess.run(init)
        sess.run(iterator.initializer)
        globals()[exe_type](sess)
