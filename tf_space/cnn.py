import tensorflow as tf

def lrelu(x, alpha=0.1):
    return tf.maximum(alpha*x,x)

def cnn_layer(c_in, 
              k_size, 
              k_strides = [1,2,2,1], 
              p_size = [1,2,2,1], 
              p_strides = [1,2,2,1], 
              stddev = 0.1):
    filter = tf.Variable(tf.truncated_normal(k_size,stddev=stddev), dtype = tf.float32, trainable = True)
    biases = tf.Variable(tf.constant(0.5, shape=[k_size[3]], dtype=tf.float32), trainable = True)
    conv = tf.nn.conv2d(c_in, filter, strides=k_strides, padding='SAME')
    conv_o = tf.nn.bias_add(conv, biases)
    pool = tf.nn.max_pool(conv_o, p_size, strides = p_strides, padding='SAME')
    out = tf.nn.relu( pool)
    return out, pool, filter,biases

def full_layer(f_in, w_size, stddev=0.01, need_relu=True):
    w = tf.Variable(tf.truncated_normal(w_size,stddev=stddev), dtype = tf.float32, trainable = True)
    b = tf.Variable(tf.constant(0.05, shape=[w_size[1]], dtype=tf.float32),trainable = True)
    a = tf.nn.bias_add(tf.matmul(f_in, w),b)
    #if not need_relu:
    #   return a, w, b
    z = tf.nn.relu(a)
    return a,z,w,b
 

def alex_conv(c_in, k_size, k_strides=[1,1,1,1], stddev=0.1, padding='SAME', group=1, need_relu=True):
    assert c_in.get_shape()[-1] % group == 0
    assert k_size[-1] % group == 0
    filter = tf.Variable(tf.truncated_normal(k_size, stddev=stddev), dtype=tf.float32)
    biases = tf.Variable(tf.constant(0.5, shape=[k_size[3]], dtype=tf.float32))
    c_in_grp = tf.split(c_in, group, 3)
    filter_grp = tf.split(filter, group, 3)
    pair = [tf.nn.conv2d(ci, ki, k_strides, padding = padding) for ci, ki in zip(c_in_grp, filter_grp)]
    conv = tf.concat(pair, 3)
    output = tf.nn.bias_add(conv, biases)
    if need_relu:
        output = tf.nn.relu(output)
    return output

def alex_fc(f_in, w_size, need_relu=True):
    w = tf.Variable(tf.truncated_normal(w_size,stddev=stddev), dtype = tf.float32, trainable = True)
    b = tf.Variable(tf.constant(0.05, shape=[w_size[1]], dtype=tf.float32),trainable = True)
    output = tf.nn.bias_add(tf.matmul(f_in, w),b)
    if need_relu:
        output = tf.nn.relu(output)
    return output

def alex_maxpool(c_in, p_size, p_strides, padding='SAME', need_relu=False):
    output = tf.nn.max_pool(c_in, p_size, strides=p_strides, padding=padding)
    if need_relu:
       output = tf.nn.relu(output)
    return output   
