import os, sys
import __init__
from zipfile import ZipFile
from tf_space.base_dataset import DataSet
from tf_space.dogcat.const import *
import tensorflow as tf
from io import BytesIO
import PIL.Image
import numpy as np
from plot import plotFigures
import utils


class Data(DataSet):
    def __init__(self, folder='KaggleDogCat', batch_size=int(os.environ.get('BATCH_SIZE')), resize=[], image_norm_type=None):
        super(Data, self).__init__(folder, batch_size=batch_size, resize=resize, image_norm_type=image_norm_type)
        self.train_path = os.path.join(self.root, 'train.zip')

    def gen_records(self):
        self._check_records()
        archive_train = ZipFile(self.train_path, 'r')            
        print(archive_train.namelist()[1:10])
        current_index = 0
        writer = None
        count = {}
        for image_name in archive_train.namelist()[1:]:
            image_label = image_name.split('/')[1].split('.')[0].encode('utf-8')
            image_code = image_name.split('/')[1].split('.')[-2].encode('utf-8')
            if current_index % 100 == 0:
                if writer:
                   writer.close()
                record_filename = '{rl}-{ci}.tfrecords'.format(rl=self.records_train, ci=current_index)
                writer = tf.python_io.TFRecordWriter(record_filename)
            filename = BytesIO(archive_train.read(image_name))
            image = PIL.Image.open(filename)
            image = np.array(image.resize((WH_SIZE,WH_SIZE)))
            image_byte = image.tobytes()
            image_h, image_w, image_c = image.shape
            example = tf.train.Example(features=tf.train.Features(feature={
                      'label': tf.train.Feature(bytes_list=tf.train.BytesList(value=[image_label])),
                      'image': tf.train.Feature(bytes_list=tf.train.BytesList(value=[image_byte])),
                      'hight': tf.train.Feature(int64_list=tf.train.Int64List(value=[image_h])),
                      'weight': tf.train.Feature(int64_list=tf.train.Int64List(value=[image_w])),
                      'channel': tf.train.Feature(int64_list=tf.train.Int64List(value=[image_c])),
                      'code': tf.train.Feature(bytes_list=tf.train.BytesList(value=[image_code]))
            }))
            writer.write(example.SerializeToString())
            if image_label in count.keys():
               count[image_label] += 1
            else:
               count[image_label] = 1
            current_index += 1
            print(image_name)
        print( count)
        writer.close()

    def get_labels(self):
        return [b'cat', b'dog']     

    def parser(self, record):
        features = tf.parse_single_example( record,
                                        features = {
                                        'label': tf.FixedLenFeature([], tf.string),
                                        'image': tf.FixedLenFeature([], tf.string),
                                        'hight': tf.FixedLenFeature([], tf.int64),
                                        'weight': tf.FixedLenFeature([], tf.int64),
                                        'channel': tf.FixedLenFeature([], tf.int64),
                                        'code': tf.FixedLenFeature([], tf.string),
                                        })
        record_image = tf.decode_raw(features['image'], tf.uint8)
        #print(record_image.shape)
        label = tf.cast(features['label'], tf.string)
        hight = tf.cast(features['hight'], tf.int32)
        weight = tf.cast(features['weight'], tf.int32)
        channel = tf.cast(features['channel'], tf.int32)
        image = tf.reshape(record_image, tf.stack([hight, weight, channel]))
        image.set_shape([WH_SIZE,WH_SIZE,3])
        image = self.get_image_norm(image)
        code = tf.cast(features['code'], tf.string)
        if len(self.resize) == 2:
            image = tf.image.resize_images(image, self.resize)
        return image, label, code   

if __name__ == '__main__':
    dog_cat = Data(resize=[])
    if sys.argv[1] == 'gen':
        dog_cat.gen_records()  
    elif sys.argv[1] == 'image':
        iterator =  dog_cat.get_records(1)
        labels =dog_cat.get_labels()
        image_batch, label_batch, code = iterator.get_next()
        print('label_batch: {}'.format(label_batch))
        train_labels = tf.map_fn(lambda l: tf.where(tf.equal(labels, l))[0,0:1][0], label_batch, dtype=tf.int64)
        one_hot_label_batch = tf.one_hot(train_labels, depth=len(labels), dtype=tf.float32,name="label_batch")
        final_labels= tf.reshape(one_hot_label_batch, [5, -1])
        with tf.Session() as sess:
            sess.run((tf.global_variables_initializer(),tf.local_variables_initializer()))
            sess.run(iterator.initializer)
            ep = 0
            while ep <= 2:
                try:
                    labels, images, t_labels, f_labels = sess.run([label_batch, image_batch, one_hot_label_batch, final_labels])
                    print('--------------------------------', ep)
                    print(labels)
                    print(t_labels)
                    print(f_labels)
                    #print(img)
                    #print(len(img))
                    print(images.shape)
                    plotFigures(images, realname=labels)
                    #if b'dog' in labels:
                    #    plotFigures(images,labels=t_labels, realname=labels)
                except tf.errors.OutOfRangeError:
                    break
                except Exception as e:
                    print(e)
                    break
                ep += 1

