import glob,time,os
import tensorflow as tf
import numpy as np
from dataset import Data
from cnn import cnn_layer, full_layer
from plot import  plotFigures, genCnnImg, genImage, genFilterImg
from const import *

EPOCH_NUM = 3000
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
dataset = Data()

logs_path = os.path.join('./tmp_2l_relu',str(int(time.time())))
model_path = './models/dogcat.ckpt'

#labels =dataset.get_labels()
iterator = dataset.get_records(EPOCH_NUM)
image_batch, label_batch, codes = iterator.get_next()
image_batch = tf.cast(image_batch, tf.float32)

init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())

saver = tf.train.import_meta_graph(model_path+'.meta')
if __name__ == '__main__':
    loop = 0
    batch_image = None
    with tf.Session() as sess1:
        sess1.run(init_op)
        sess1.run(iterator.initializer)
        batch_image = sess1.run(image_batch)

    with tf.Session() as sess:
        saver = saver.restore(sess, model_path )
        graph = tf.get_default_graph()
        X = graph.get_tensor_by_name('X:0')
        output = graph.get_tensor_by_name('output:0')
        feed_dict = {
            X: batch_image
        }
        print(sess.run([output], feed_dict=feed_dict))
 

        

