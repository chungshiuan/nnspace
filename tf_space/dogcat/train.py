import __init__
import glob,time,os
import tensorflow as tf
import numpy as np
from dataset import Data
from cnn import cnn_layer, full_layer
from plot import  plotFigures, genCnnImg, genImage, genFilterImg
from const import *

EPOCH_NUM = 40
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))
dataset = Data(batch_size=BATCH_SIZE)

logs_path = os.path.join('./tmp_2l_relu',str(int(time.time())))
labels =dataset.get_labels()

iterator = dataset.get_records(EPOCH_NUM)
image_batch, label_batch, codes = iterator.get_next()

train_labels = tf.map_fn(lambda l: tf.where(tf.equal(labels, l))[0,0:1][0], label_batch, dtype=tf.int64)
image_batch = tf.cast(image_batch, tf.float32)
X = tf.placeholder(tf.float32, [None, WH_SIZE, WH_SIZE, 3])
keep_prob_cnn = tf.placeholder(tf.float32)
keep_prob = tf.placeholder(tf.float32)
cnn_1, po1, f1, b1 = cnn_layer(image_batch, [3,3,3,32], k_strides=[1,1,1,1])
cnn_2, po2, f2, b2 = cnn_layer(cnn_1, [3,3,32,32], k_strides=[1,1,1,1])
cnn_3, po3, f3, b3 = cnn_layer(cnn_2, [3,3,32,64], k_strides=[1,1,1,1])
#cout = tf.nn.dropout(cnn_3, keep_prob_cnn)
cout = cnn_3
dense_in = tf.reshape(cout, [-1, cout.get_shape()[1:4].num_elements()])
d_size = WH_SIZE
o_size = len(labels)
fa_1, full_1, full_w1, full_b1 = full_layer(dense_in, [int(dense_in.shape[1]), d_size], need_relu=True)
#y1 = tf.nn.dropout(full_1, keep_prob)
y1 = full_1
fa_2, full_2, full_w2, full_b2 = full_layer(y1, [d_size, o_size], need_relu=True)
Y = fa_2
LEARNING_RATE=1e-4

final_labels = tf.one_hot(train_labels, depth=o_size, dtype=tf.float32,name="label_batch")

one_hot_label_batch = tf.one_hot(train_labels, depth=o_size, dtype=tf.float32,name="label_batch")
#final_labels= tf.reshape(one_hot_label_batch, [BATCH_SIZE, -1])
diff = tf.nn.softmax_cross_entropy_with_logits(labels= final_labels, logits=fa_2)
loss = tf.reduce_mean(diff)
global_step = tf.Variable(0)
Y_SOF = tf.nn.softmax(Y)
train = tf.train.AdamOptimizer(LEARNING_RATE).minimize(loss)
pred = tf.argmax(Y_SOF, 1)
#ans = tf.argmax(one_hot_label_batch, 1)
ans = tf.argmax(final_labels, 1)

correct_prediction = tf.equal(pred, ans)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

tf.summary.scalar("cost", loss)
tf.summary.scalar("accuracy", accuracy)
summary_op = tf.summary.merge_all()
writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())


if __name__ == '__main__':
    loop = 0
    with tf.Session() as sess:
        sess.run(init_op)
        sess.run(iterator.initializer)
        while True:
            try:
                print( '=============',loop)
                iimg, _,c1,c2,c3,ys,yh,yy,din,fu1,fa1, fa2, acc, los, summary, filter1, filter2, fw1, tlbs, ohlb, lbh, cd, pd, an= sess.run([
                                                         X,
                                                         train, 
                                                         cnn_1,
                                                         cnn_2,
                                                         cnn_3,
                                                         Y_SOF, 
                                                         final_labels,
                                                         Y,
                                                         dense_in,
                                                         full_1,
                                                         fa_1,
                                                         fa_2,
                                                         accuracy, 
                                                         loss, 
                                                         summary_op,
                                                         f1,
                                                         f2,
                                                         full_w1,
                                                         train_labels,
                                                         one_hot_label_batch,
                                                         label_batch,
                                                         codes,pred,ans], {keep_prob_cnn:0.3,
                                                                    keep_prob:0.4, 
                                                                    X:sess.run(image_batch)})
        
                #print( tlbs)
                #print( ohlb)
                #plotFigures(iimg, labels=tlbs,realname=cd)
                #print( 'input image:', iimg)
                #print( genFilterImg(filter1))
                #print( yy)
                #print('====an====')
                #print( yh)
                #print('====pd====')
                #print( ys)
                #print( filter1.shape)
                #print( 'filter1:', filter1)
                #print( 'filter2:', filter2)
                #print( 'pred ', pd)
                #print( 'ans ', an)
                print( 'loss:', los)
                print( 'accu:', acc)
                '''
    
                if loop < 300:
                    continue
                for img in c2:
                    plotFigures(genCnnImg(img))


                for img in fa2:
                    plotFigures(genCnnImg(img))

                gy = []
                for ind, val in enumerate(din):
                    gy.append(genImage(val, (1,7)))
                    gy.append(genImage(yh[ind],(1,7)))
                plotFigures(gy, 4)
                plotFigures(genFilterImg(filter1))

                #for img in c2:
                #    plotFigures(genCnnImg(img))
                '''
                writer.add_summary(summary, loop)

            except tf.errors.OutOfRangeError:
                break
            except Exception as e:
                print(e)
                break
            loop += 1 
 

        

