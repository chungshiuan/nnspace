import __init__
import sys
from dataset_factory import DogCatTfrDS
from cnn_module.cnn_base_net import CNNCAM
from cnn_module.cam import CAM
from const import *
import tensorflow as tf
from plot import plotFigures,genImage

model_path = './3c_2d_cnncam/dogcat'
def train():
    net = CNNCAM(dataset=DogCatTfrDS(reshape_size=[-1, WH_SIZE, WH_SIZE, 3], epoch=3000, image_norm_type='0to1'), image_shape=[WH_SIZE, WH_SIZE, 3], model_path=model_path)    
    activations = net.net()
    print(activations)
    for activation in activations:
        tf.add_to_collection('CAM', activation)    
    net.optimization()
    net.train()

def predict():
    saver = tf.train.import_meta_graph(model_path+'.meta')
    dataset = DogCatTfrDS(batch_size=20, image_norm_type='0to1')
    images, labels, codes = dataset.next_batch()
    with tf.Session() as sess:
        saver.restore(sess, model_path)
        input_data = tf.get_default_graph().get_tensor_by_name('CNN/input:0')
        predict = tf.argmax(tf.nn.softmax(tf.get_default_graph().get_tensor_by_name('CNN/dense2/MatMul:0')),1)
        result = sess.run(predict,  feed_dict={input_data: images})
    return [images, labels, result ]

 
def predictex():
    #tf.reset_default_graph()
    #with tf.Session() as sess1:
    RS = []
    saver = tf.train.import_meta_graph(model_path+'.meta')
    images = []
    labels = []
    codes = []
    #with tf.Session() as sess1:
    #    sess1.run( tf.group(tf.global_variables_initializer(), tf.local_variables_initializer()))
    dataset = DogCatTfrDS(batch_size=25, image_norm_type='0to1')

    images, labels, codes = dataset.next_batch()

    with tf.Session() as sess:
        saver.restore(sess, model_path)

        activations = tf.get_collection('CAM')
        weights = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='.*kernel.*')
        biases = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='.*bias.*')
        print('all activations: {}'.format(activations))
        print('all weights: {}'.format(weights))
        input_data = tf.get_default_graph().get_tensor_by_name('CNN/input:0')
        predict = sess.run(activations[-1], feed_dict={input_data: images})
        ex =  CAM(activations[5], weights[-1])
        for i in range(2):
            RS.append(sess.run(tf.squeeze(ex.get_ex(i, images.shape[1:3])), feed_dict={input_data: images}))
    print('--------------------',RS[0].shape)
    return {'rs':RS, 
            'images': images, 
            'labels': labels,
            'predict': predict}

def show_lrp(result, panel=True):
    for idx, image in enumerate(result['images']):
        plotFigures(data = [image, result['rs'][0][idx],  result['rs'][1][idx]], 
                    culNum = 3,
                    labels = [result['labels'][idx], 'cat', 'dog'],
                    realname = ['cat:{} dog:{}'.format(result['predict'][idx][0], result['predict'][idx][1]), '', ''],
                    panel = panel)

 
if __name__ == '__main__':
    exe_type = 'train'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'predict' or sys.argv[1] == 'train' or sys.argv[1] == 'predictex':
            exe_type = sys.argv[1]
        else:
            print('predict or train')
            exit()
    result = eval(exe_type)()
    if sys.argv[1] == 'predictex':
        print( result)
        show_lrp(result)
    elif sys.argv[1] == 'predict':
        print(result[0].shape)
        plotFigures(result[0], culNum=2, labels = result[1], realname=result[2])
  
