import __init__
import glob,time,os
import tensorflow as tf
import numpy as np
from dataset import Data
from cnn import cnn_layer, full_layer
from plot import  plotFigures, genCnnImg, genImage, genFilterImg
from const import *
from utils import get_available_gpus
import time

LEARNING_RATE=1e-4
EPOCH_NUM = 50
BATCH_SIZE = int(os.environ.get('BATCH_SIZE'))

logs_path = os.path.join('./tmp_2l_relu',str(int(time.time())))
model_path = './models/dogcat.ckpt'

cpu_devices = get_available_gpus('CPU')
gpu_device = get_available_gpus()
ps = cpu_devices[0]
worker = []
if len(gpu_device)==0:
    worker= cpu_devices
else:
    worker = gpu_device
dataset = Data(batch_size=BATCH_SIZE*len(worker))
labels =dataset.get_labels()
iterator = dataset.get_records(EPOCH_NUM)
image_batch, label_batch, codes = iterator.get_next()

train_labels = tf.map_fn(lambda l: tf.where(tf.equal(labels, l))[0,0:1][0], label_batch, dtype=tf.int64)
image_batch = tf.cast(image_batch, tf.float32)


PS_OPS = ['Variable', 'VariableV2', 'AutoReloadVariable']

def assign_to_device(device, ps_device='/cpu:0'):
    def _assign(op):
        node_def = op if isinstance(op, tf.NodeDef) else op.node_def
        if node_def.op in PS_OPS: 
            print('>>>>>>>>>>', "/" + ps_device)
            return "/" + ps_device
        else:
            print('>>>>>>>>',device)
            return device

    return _assign

def average_gradients(tower_grads):
    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            print('??!?!?!?!?!??!',g)
            # Add 0 dimension to the gradients to represent the tower.
            if not g == None:
                expanded_g = tf.expand_dims(g, 0)

                # Append on a 'tower' dimension which we will average over below.
                grads.append(expanded_g)

        # Average over the 'tower' dimension.
        grad = tf.concat(grads, 0)
        grad = tf.reduce_mean(grad, 0)

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append(grad_and_var)
    return average_grads


def net(in_put, kp, kpc, reuse=False):
    with tf.variable_scope('ConvNet', reuse=reuse):
        cnn_1, po1, f1, b1 = cnn_layer(in_put, [3,3,3,32], k_strides=[1,1,1,1])
        cnn_2, po2, f2, b2 = cnn_layer(cnn_1, [3,3,32,32], k_strides=[1,1,1,1])
        cnn_3, po3, f3, b3 = cnn_layer(cnn_2, [3,3,32,64], k_strides=[1,1,1,1])
        dense_in = tf.reshape(cnn_3, [-1, cnn_3.get_shape()[1:4].num_elements()])
        o_size = len(labels)
        fa_1, full_1, full_w1, full_b1 = full_layer(dense_in, [int(dense_in.shape[1]), WH_SIZE], need_relu=True)
        fa_2, full_2, full_w2, full_b2 = full_layer(full_1, [WH_SIZE, o_size], need_relu=True)
        return fa_2


with tf.device('/cpu:0'):
    tower_grads = []
    reuse = False 
    X = tf.placeholder(tf.float32, [None, WH_SIZE, WH_SIZE, 3], name='X')
    Y_HAT = tf.placeholder(tf.float32, [None, 2], name='Y_HAT')
    keep_prob_cnn = tf.placeholder(tf.float32)
    keep_prob = tf.placeholder(tf.float32)

    for i, device in enumerate(worker):
        if len(gpu_device)==0:
            dev = '/cpu:0'
        else:
            dev = assign_to_device('/gpu:{}'.format(i), ps_device='/cpu:0')
        with tf.device(dev):
            start = i*BATCH_SIZE
            end = (i+1)*BATCH_SIZE
            _x = X[start:end] 
            _y_hat = Y_HAT[start:end]
            Y = net(_x, keep_prob, keep_prob_cnn, reuse)
            diff = tf.nn.softmax_cross_entropy_with_logits(labels= _y_hat, logits=Y)
            loss = tf.reduce_mean(diff)
            optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
            grads = optimizer.compute_gradients(loss)
            print('complete assign {} device: {}'.format(i, device))
            if i == 0:
                Y_SOF = tf.nn.softmax(Y, name='output')
                pred = tf.argmax(Y_SOF, 1)
                #ans = tf.argmax(one_hot_label_batch, 1)
                ans = tf.argmax(_y_hat, 1)
                correct_prediction = tf.equal(pred, ans)
                accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
            reuse = True
            tower_grads.append(grads)
print('>>>>>>>>', tower_grads)
tower_grads = average_gradients(tower_grads)
print('--------', tower_grads)
train_op = optimizer.apply_gradients(tower_grads)

final_labels = tf.one_hot(train_labels, depth=len(labels), dtype=tf.float32,name="label_batch")
tf.summary.scalar("cost", loss)
tf.summary.scalar("accuracy", accuracy)
summary_op = tf.summary.merge_all()
writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())

saver = tf.train.Saver()


if __name__ == '__main__':
    loop = 0
    with tf.Session() as sess:
        sess.run(init_op)
        sess.run(iterator.initializer)
        start = time.time()
        while True:
            try:
                print( '=============',loop)
                batch_label, batch_image = sess.run([final_labels, image_batch])
                print('>>>>>>>>>>')
                print(batch_label.shape)
                print(batch_image.shape)
                print('<<<<<<<<<<<')
                feed_dict =  {keep_prob_cnn:0.3,
                              keep_prob:0.4, 
                              X: batch_image,
                              Y_HAT: batch_label}  

                _, so, los, acc= sess.run([train_op, summary_op, loss, accuracy ], feed_dict=feed_dict)
        
                print( 'loss:', los)
                print( 'accu:', acc)
                writer.add_summary(so, loop)
                if loop % 50 == 0:
                   saver.save(sess, model_path)
            except tf.errors.OutOfRangeError:
                break
            except Exception as e:
                print(e)
                break
            loop += 1 
        print('final use: {}'.format(time.time()-start))
 

        

