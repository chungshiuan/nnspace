import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
fig = None
def genImage(img, shape):
    return np.array(img).reshape(*shape)

def genCnnImg(images):
    width, height, channels = images.shape
    data = []
    for ch in  range(channels):
        data.append(images[:,:,ch])
    return data
    
def genFilterImg(images):
    width, height, channels, size = images.shape
    print( width, height, channels, size )
    data = []
    for s in range(size):
        for ch in  range(channels):
            data.append(images[:,:,ch, s])
    return data

def plotFigures(data, culNum = 5, labels=[], realname=[]):
    global fig
    if fig == None:
       fig = plt.figure()
    data_length = len(data)
    rowNum = int(data_length/culNum) + 1
    print('----',rowNum)
    gs = gridspec.GridSpec(rowNum,culNum)
    row = -1
    for index, dt in enumerate(data):
        col = index % culNum
        if col == 0:
           row = row + 1
        print(row,' | ' ,col)
        ax = plt.subplot(gs[row, col])
        if(len(realname) > 0 and len(realname) == data_length):
            plt.xlabel(realname[index])
        if (len(labels) > 0 and  len(labels) == data_length):
            plt.title(labels[index])
        plt.imshow(dt)
    plt.show(block = False)    
    _ = input("Press [enter] to continue.")
    #print( _)
    plt.clf()

if __name__ == "__main__":
   data = [[[1,2],[2,1]] for i in range(32)]
   print( data)
   plotFigures(data)
