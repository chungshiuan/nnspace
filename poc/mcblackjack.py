import numpy as np
import gym

env = gym.make('Blackjack-v0')
state_val = np.zeros((22, 12, 2))
state_count = np.zeros(state_val.shape)
# Hold at values greater than 20
policy = 20

episodes = 100000
for episode in range(episodes):
    complete = False
    s_0 = env.reset()
    print('s_0: {}'.format(s_0))
    G = []
    states = [s_0]
    while complete==False:
        # Implement policy
        if s_0[0] >= policy:
            print('1111111111')
            s_1, reward, complete, _ = env.step(0)
        else:
            print('2222222222222')
            s_1, reward, complete, _ = env.step(1)
        
        G.append(reward)
        states.append(s_1)
        print('***************************')
        print(complete,' ',reward,' ', s_1, '  ', states) 
        print('----------------------------')
        if complete==True:
            print('>>>>>>>>>>>>>>>')
            for s_i, s in enumerate(states[:-1]):
                print(s_i, ' :  ', s)
                if s[2] == True:
                    s_ace = 1
                else:
                    s_ace = 0
                    
                returns = np.mean(G[s_i:])
                print('??????????returns :', returns) 
                # Update values
                #print('s[0]: {}'.format(s[0]))
                state_count[s[0], s[1], s_ace] += 1
                state_val[s[0], s[1], s_ace] = state_val[s[0], s[1], s_ace] + (
                    returns - state_val[s[0], s[1], s_ace]) / state_count[s[0], s[1], s_ace]
            #print(state_val)
            
        s_0 = s_1
