import tensorflow as tf
model_path = './assign/assign.ckpt'
def func(g):
    g = tf.identity(g, name='in')
    bias = tf.Variable(1.0)
    y_pred = g ** 2 + bias     
    y_pred = tf.identity(y_pred, name='out')
    return y_pred

x = tf.placeholder(tf.float32)
z=func(x)

saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    print(sess.run(z, {x:0.3}))
    saver.save(sess, model_path)
