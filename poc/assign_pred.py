import tensorflow as tf
model_path = './assign/assign.ckpt'
saver = tf.train.import_meta_graph(model_path+'.meta')

with tf.Session() as sess:
    saver = saver.restore(sess, model_path)
    graph = tf.get_default_graph()
    in_  = graph.get_tensor_by_name('in:0')
    out = graph.get_tensor_by_name('out:0')
    print('??????',sess.run([out], {in_:2.0}))
