import numpy as np
import tensorflow as tf
from tensorflow.python.ops import nn_ops, gen_nn_ops

x = tf.constant(np.arange(9).reshape((1, 3, 3, 1)))
sess = tf.Session()
y = tf.nn.pool(x,[2,2],padding='VALID', strides=[1,1], pooling_type='MAX')
g=tf.constant(np.array([2,2,2,2]).reshape(1,2,2,1))
sess.run(gen_nn_ops.max_pool_grad(x,y,g,[1,2,2,1],[1,1,1,1],'VALID'))


