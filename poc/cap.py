import tensorflow as tf
import numpy as np

sess = tf.Session()
x = tf.cast(tf.constant(np.arange(12).reshape([1,2,2,3])), tf.float32)
print('x:{}'.format(sess.run(x)))
y = tf.cast(tf.constant(np.arange(3)),tf.float32)
print('y:{}'.format(sess.run(y)))
print('x*y:{}'.format(sess.run(x*y)))

print('x GAP:{}'.format(sess.run(tf.layers.average_pooling2d(x, pool_size=[2,2], strides=1))))
print('reduce x*y{}'.format(sess.run(tf.reduce_sum(x*y, axis=3, keep_dims=True))))
