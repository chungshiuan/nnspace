import gym
env = gym.make('CartPole-v0')
for i_episode in range(20):
    observation = env.reset()
    print('being reset')
    for t in range(100):
        print('t:{}======================'.format(t))
        env.render()
        print(observation)
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        print('obs:{}'.format(observation))
        print('rew:{}'.format(reward))
        print('done:{}'.format(done))
        print('info:{}'.format(info))
        if done:
            print("Episode finished after {} timesteps".format(t+1))
            break
