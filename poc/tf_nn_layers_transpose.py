import tensorflow as tf
import numpy as np

# the format of value: [NHWC]
value = tf.reshape(tf.constant([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]]), [1, 3, 3, 1]) 
# the format of filter: [height, width, output_channels, input_channels]
filter = tf.reshape(tf.constant([[1., 0.], [0., 1.]]), [2, 2, 1, 1])
# the format of output_shape: [NHWC]
output_shape = [1, 5, 5, 1]
# the format of strides: [1, stride, stride, 1]
strides = [1, 2, 2, 1]
padding = 'SAME'
# define the transpose conv op
transpose_nn = tf.nn.conv2d_transpose(value=value, filter=filter, output_shape=output_shape, strides=strides, padding=padding)
transpose_layer = tf.layers.conv2d_transpose(value, 1, 5, strides=strides[1], padding=padding, kernel_initializer=tf.constant_initializer([[0., 1.], [1., 0.]]))  

init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
with tf.Session() as sess:
    sess.run(init)
    print(sess.run(transpose_nn))
    print(sess.run(transpose_layer))
