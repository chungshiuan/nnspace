import numpy as np
import tensorflow as tf
import __init__
from plot import plotFigures
from tensorflow.examples.tutorials.mnist import input_data

# ref: https://stackoverflow.com/questions/16798888/2-d-convolution-as-a-matrix-matrix-multiplication
from numpy import zeros

from scipy.sparse import lil_matrix
from numpy import zeros
import scipy 


def unroll_kernel(kernel, n, sparse=True):
    m = kernel.shape[0]
    if sparse:
        unrolled_K = lil_matrix(((n - m +1)**2, n**2))
    else:
        unrolled_K = zeros(((n - m+1)**2, n**2))
    skipped = 0
    for i in range(n ** 2):
        if (i % n) < n - m +1 and((i / n) % n) < n - m +1:
            for j in range(m):
                for l in range(m):
                    unrolled_K[i - skipped, i + j * n + l] = kernel[j, l]
        else:
            skipped += 1
    return unrolled_K

def unroll_matrix(X, m):
    flat_X = X.flatten()
    n = X.shape[0]
    unrolled_X = zeros(((n - m) ** 2, m**2))
    skipped = 0
    for i in range(n ** 2):
        if (i % n) < n - m and ((i / n) % n) < n - m:
            for j in range(m):
                for l in range(m):
                    unrolled_X[i - skipped, j * m + l] = flat_X[i + j * n + l]
                    print(unrolled_X[i - skipped, j * m + l])
        else:
            skipped += 1
    return unrolled_X

def cov_decov_np(image):
    image = image.reshape([28,28])
    filter = np.array([[0,100,0],[0,0,0],[0,100,0]])
#    filter = np.array([[100,0,0],[0,100,0],[0,0,100]])

#    unroll_filter = unroll_kernel(filter, image.shape[0], False)
    unroll_filter = unroll_kernel(filter, image.shape[0], False)
    image = image.flatten()
    print('unroll_filter.shape:{}'.format(unroll_filter.shape))
    image_f_t = image.reshape((1,-1)).transpose()
    print('image_f_t.shape:{}'.format(image_f_t.shape))
    cov_result = np.dot(unroll_filter, image_f_t)
    print('cov_result.shape:{}'.format(cov_result.shape))
    decov_result = np.dot(unroll_filter.transpose(), cov_result)
    print('decov_result.shape:{}'.format(decov_result.shape))
    image = decov_result.transpose().reshape([1,28,28])
    conv_image = np.pad(cov_result.reshape([26,26]),((1,1),(1,1)),'constant',constant_values=256)
    print(conv_image.shape)
    conv_image = conv_image.reshape([1,28,28])
    
    return image, conv_image

# get a image and show image
mnist = input_data.read_data_sets("/tmp/data/")
image = mnist.train.images[1].reshape([1,28,28])
#plotFigures(image, 1)
# cov_decov
new_image, cov_image = cov_decov_np(image)
# show result image
final = np.vstack((image, cov_image, new_image))
plotFigures(final, 3)

