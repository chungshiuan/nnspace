import tensorflow as tf
x = tf.placeholder(tf.float32)
y = tf.placeholder(tf.float32)
bias = tf.Variable(1.0)
y_pred = x ** 2 + bias     # x -> x^2 + bias
loss = (y - y_pred)**2     # l2 loss?
with tf.Session() as session: 
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    session.run(init_op)
    # Error: to compute loss, y is required as a dependency
    #print('Loss(x,y) = %.3f' % session.run(loss, {x: 3.0}))
    # OK, print 1.000 = (3**2 + 1 - 9)**2
    print('Loss(x,y) = %.3f' % session.run(loss, {x: 3.0, y: 9.0}))
    # OK, print 10.000; for evaluating y_pred only, input to y is not required
    print('pred_y(x) = %.3f' % session.run(y_pred, {x: 3.0}))
    # OK, print 1.000 bias evaluates to 1.0
    print('bias      = %.3f' % session.run(bias))
