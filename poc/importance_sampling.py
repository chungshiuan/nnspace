import numpy as np
from scipy.stats import norm

correct_prob=1-norm.cdf(3.0, loc=0.0, scale=1.0)
print('correct prob > 3 is {}'.format(correct_prob))
size_list = [100, 1000, 10000, 100000, 100000, 1000000]
for sz in size_list:
  x = np.random.normal(size=sz)
  x = x > 3
  print('size={}, prob={}'.format(sz, sum(x)/sz*1.0))

print('--------')
for sz in size_list:
  is_value = 0
  x=np.random.normal(4,1,size=sz)
  p = norm.pdf(x)
  q = norm.pdf(x, loc=4)
  ratio = p/q
  for i in range(sz):
    if x[i] >= 3:
      is_value += ratio[i]
  print('size={}, prob={}'.format(sz, is_value/sz*1.0))


print('--------')
for sz in size_list:
  is_value = 0
  x=np.random.normal(4,1,size=sz)
  p = norm.pdf(x)
  q = norm.pdf(x, loc=4)
  ratio = p/q
  ratio_sum = np.sum(ratio)
  for i in range(sz):
    if x[i] >= 3:
      is_value += ratio[i]
  print('size={}, prob={}'.format(sz, is_value/ratio_sum))

