# -*- coding:utf-8 -*-
#引入numpy庫和matplotlib庫
import numpy as np
import matplotlib.pyplot as plt

# 定義等高線圖的橫縱座標x，y
#從左邊取值爲從 -3 到 3 ，各取5個點，一共取 5*5 = 25 個點
x = np.linspace(-3, 3, 5)
y = np.linspace(-3, 3, 5)
# 將原始數據變成網格數據
X, Y = np.meshgrid(x, y)
print(X)
print(Y)
# 各地點對應的高度數據
#Height是個 5*5 的數組，記錄地圖上 25 個點的高度彙總
#Height = [[0,0,1,2,2],[0,-2,-2,1,5],[4,2,6,8,1],[3,-3,-3,0,5],[1,-5,-2,0,3]]
Height = [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14],[15,16,17,18,19],[20,21,22,23,24]]

print(Height)
# 填充顏色
plt.contourf(X, Y, Height, 10, alpha = 0.6, cmap = plt.cm.hot)
# 繪製等高線
C = plt.contour(X, Y, Height, 10, colors = 'black', linewidth = 0.5)
# 顯示各等高線的數據標籤
plt.clabel(C, inline = True, fontsize = 10)
plt.show()
