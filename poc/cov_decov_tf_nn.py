import numpy as np
import tensorflow as tf
import __init__
from plot import plotFigures
from tensorflow.examples.tutorials.mnist import input_data

# ref: https://stackoverflow.com/questions/16798888/2-d-convolution-as-a-matrix-matrix-multiplication
from numpy import zeros

from scipy.sparse import lil_matrix
from numpy import zeros
import scipy 



def cov_decov_np():
    image = tf.placeholder(tf.float32, shape=[None, 28, 28, 1], name='ae_input')

    #image = image.reshape([1,28,28,1])
    filter = tf.constant(np.array([[0.,0.,100.],[0.,100.,0.],[100.,0.,0.]]), dtype=tf.float32)
    filter = tf.reshape(filter, [3,3,1,1])
    en = tf.nn.conv2d(image, filter, strides=[1,2,2,1], name='conv1', padding='SAME')
    de = tf.nn.conv2d_transpose(en, filter, output_shape=[1, 28,28,1],  padding='SAME', strides=[1, 2, 2, 1])
    image = tf.reshape(de, [1,28,28]) 
    return image

# get a image and show image
mnist = input_data.read_data_sets("/tmp/data/")
image = mnist.train.images[1].reshape([1,28,28])
print(image)
#plotFigures(image, 1)
# cov_decov
_image = cov_decov_np()
new_image = None
init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())

with tf.Session() as sess:
    sess.run(init)
    ae_input = tf.get_default_graph().get_tensor_by_name('ae_input:0') 
    feed_dict = {ae_input: image.reshape([1,28,28,1]).astype(np.float32)}
    new_image = sess.run(_image, feed_dict=feed_dict)

# show result image
final = np.vstack((image, new_image))
plotFigures(final, 2)

