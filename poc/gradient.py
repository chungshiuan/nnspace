import tensorflow as tf

w1 = tf.get_variable('w1', shape=[1])
w2 = tf.get_variable('w2', shape=[1])

w3 = tf.get_variable('w3', shape=[1])
w4 = tf.get_variable('w4', shape=[1])

z1 = 3 * w1 + 2 * w2+ w3
z2 = -1 * w3 + w4
grads1 = tf.gradients([z1, z2], [w1, w2, w3, w4])

grads = tf.gradients([z1, z2], [w1, w2, w3, w4], grad_ys=[[-2.0], [-3.0]])

with tf.Session() as sess:
    tf.global_variables_initializer().run()
    print(sess.run(grads1))
    print(sess.run(grads))

w1 = tf.get_variable('w5', shape=[3])
w2 = tf.get_variable('w6', shape=[3])

w3 = tf.get_variable('w7', shape=[3])
w4 = tf.get_variable('w8', shape=[3])

z1 = 3 * w1 + 2 * w2+ w3
z2 = -1 * w3 + w4
grads1 = tf.gradients([z1, z2], [w1, w2, w3, w4])

grads = tf.gradients([z1, z2], [w1, w2, w3, w4], grad_ys=[[-2.0, -3.0, -4.0], [-2.0, -3.0, -4.0]])

with tf.Session() as sess:
    tf.global_variables_initializer().run()
    print(sess.run(grads1))
    print(sess.run(grads))

'''
例如ys=[y1,y2,y3], xs=[x1,x2,x3,x4]，那么tf.gradients(ys,xs)=[d(y1+y2+y3)/dx1,d(y1+y2+y3)/dx2,d(y1+y2+y3)/dx3,d(y1+y2+y3)/dx4].具体例子见下面代码第16-17行。

2. grad_ys 是ys的加权向量列表，和ys长度相同，当grad_ys=[q1,q2,g3]时，tf.gradients(ys,xs，grad_ys)=[d(g1*y1+g2*y2+g3*y3)/dx1,d(g1*y1+g2*y2+g3*y3)/dx2,d(g1*y1+g2*y2+g3*y3)/dx3,d(g1*y1+g2*y2+g3*y3)/dx4].
'''
