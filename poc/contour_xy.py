# -*- coding:utf-8 -*-
#引入numpy庫和matplotlib庫
import numpy as np
import matplotlib.pyplot as plt

# 定義等高線圖的橫縱座標x，y
#從左邊取值爲從 -3 到 3 ，各取5個點，一共取 5*5 = 25 個點
x = np.linspace(-3, 3, 5)
y = np.linspace(-3, 3, 5)
# 將原始數據變成網格數據
X, Y = np.meshgrid(x, y)

# 定義等高線高度函數
def f(x, y):
    print('x: {}'.format(x))
    print('y: {}'.format(y))
    print('xy: {}'.format(x*y))

    return x * y

# 填充顏色
plt.contourf(X, Y, f(X,Y), 10, alpha = 0.5, cmap = plt.cm.hot)
# 繪製等高線
C = plt.contour(X, Y, f(X,Y), 10, colors = 'black', linewidth = 0.5)
# 顯示各等高線的數據標籤
plt.clabel(C, inline = True, fontsize = 10)
plt.show()
